﻿using System.IO;
using System.Windows;
using DiffDisplay.Util;
using DiffTestBench;
using DiffTestBench.Diff;
using SSTDiff;
using SSTDiff.Util;

namespace DiffDisplay.UI
{
    /// <summary>
    /// Interaction logic for BaseCasesViewer.xaml
    /// </summary>
    public partial class BaseCasesViewer
    {
        private readonly BaseCaseViewerModel _baseCaseViewerModel;
        private readonly Settings _settings;

        public BaseCasesViewer()
        {
            InitializeComponent();
            _baseCaseViewerModel = new BaseCaseViewerModel();
            FolderPath.Text = Program.RootDir;

            _settings = new Settings();
            SettingGrid.SetDataContext(_settings);
        }

        private void BrowseFolder(object sender, RoutedEventArgs e)
        {
            FolderPath.Text = FileDialogUtil.GetFolderPath();
        }

        private void UpdateCasesButtonOnClick(object sender, RoutedEventArgs e)
        {
            UpdateCases();
        }

        private void UpdateCases()
        {
            if (FolderPath.Text != "" && Directory.Exists(FolderPath.Text))
            {
                Cases.ItemsSource = _baseCaseViewerModel.GetBaseCases(FolderPath.Text);
            }
            else
            {
                MessageBox.Show("The folder path does not exist");
            }
        }

        private void DisplayCase(object sender, RoutedEventArgs e)
        {
            var sstTuple = _baseCaseViewerModel.CaseDictionary[(string) Cases.SelectionBoxItem];

            DiffDisplay.Xaml = XamlPrinter.PrintDiff(sstTuple.Item1.DeepCloneNode(), sstTuple.Item2.DeepCloneNode(), _settings);
        }
    }
}

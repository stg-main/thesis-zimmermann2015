﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DiffTestBench;
using KaVE.Commons.Model.SSTs.Impl;

namespace DiffDisplay.UI
{
    public class BaseCaseViewerModel
    {
        public Dictionary<string, Tuple<SST, SST>> CaseDictionary;

        public BaseCaseViewerModel()
        {
            CaseDictionary = new Dictionary<string, Tuple<SST, SST>>();
        }

        public IEnumerable<string> GetBaseCases(string directoryPath)
        {
            UpdateCaseDictionary(directoryPath);
            return CaseDictionary.Keys;
        }

        public void UpdateCaseDictionary(string directoryPath)
        {
            CaseDictionary.Clear();

            var jsonFilesA = SSTReader.GetJsonFileList(Path.Combine(directoryPath, "A"));
            var jsonFilesB = SSTReader.GetJsonFileList(Path.Combine(directoryPath, "B"));

            var sstFilePaths = new List<Tuple<string, string>>();

            foreach (var jsonFileFromA in jsonFilesA)
            {
                var jsonFileFromB = jsonFilesB.FirstOrDefault(sstPath =>
                {
                    var fileName = Path.GetFileName(sstPath);
                    return fileName != null && fileName.Equals(Path.GetFileName(jsonFileFromA));
                });
                if (!string.Empty.Equals(jsonFileFromB))
                    sstFilePaths.Add(Tuple.Create(jsonFileFromA, jsonFileFromB));
            }

            foreach (var filePair in sstFilePaths)
            {
                var sstA = SSTReader.ReadAndParseSST(filePair.Item1);
                var sstB = SSTReader.ReadAndParseSST(filePair.Item2);

                var caseName = Path.GetFileNameWithoutExtension(filePair.Item1);
                if (caseName != null) CaseDictionary.Add(caseName,Tuple.Create(sstA,sstB));
            }
        }

    }
}
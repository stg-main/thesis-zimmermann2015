﻿using System.Windows.Controls;
using SSTDiff;

namespace DiffDisplay.UI
{
    /// <summary>
    /// Interaction logic for SettingsGrid.xaml
    /// </summary>
    public partial class SettingsGrid : Grid
    {
        public SettingsGrid()
        {
            InitializeComponent();
        }

        public void SetDataContext(Settings settings)
        {
            DataContext = settings;
        }
    }
}

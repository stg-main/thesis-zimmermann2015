﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Threading;
using KaVE.Commons.Utils;
using KaVE.Commons.Utils.Exceptions;
using KaVE.RS.Commons.Utils;

namespace DiffDisplay.UI
{
    public class XamlBindableRichTextBox : RichTextBox
    {
        public XamlBindableRichTextBox()
        {
            IsReadOnly = true;
        }

        public static readonly DependencyProperty XamlProperty =
            DependencyProperty.Register(
                "Xaml",
                typeof(string),
                typeof(XamlBindableRichTextBox),
                new PropertyMetadata(OnXamlChanged));

        public string Xaml
        {
            set { SetValue(XamlProperty, value); }
        }

        private static void OnXamlChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var richTextBox = (XamlBindableRichTextBox)d;
            richTextBox.Document = new FlowDocument(new Paragraph(new Run("Loading ...")));
            // defer parsing Xaml till after loading message is displayed
            richTextBox.Dispatcher.BeginInvoke(
                (Action)(() =>
                {
                    var xaml = (string)e.NewValue;
                    Paragraph par;
                    if (xaml != null)
                    {
                        par = CreateParagraphFromXaml(xaml);
                        
                    }
                    else
                    {
                        par = new Paragraph();
                    }
                    richTextBox.Document = new FlowDocument(par);
                }),
                DispatcherPriority.Loaded);
        }

        private static Paragraph CreateParagraphFromXaml(string xaml)
        {
            try
            {
                var template = XamlUtils.CreateDataTemplateFromXaml(xaml);
                var textBlock = (TextBlock)template.LoadContent();

                var par = new Paragraph();
                par.Inlines.AddRange(textBlock.Inlines.ToList());
                return par;
            }
            catch (Exception)
            {
                return new Paragraph(new Run(xaml));
            }
        }
    }
}
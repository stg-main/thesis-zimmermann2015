﻿using System.Windows;
using DiffTestBench.Diff;
using KaVE.Commons.Model.SSTs;
using SSTDiff;

namespace DiffDisplay.UI
{
    /// <summary>
    /// Interaction logic for CompletenessEvaluationWindow.xaml
    /// </summary>
    public partial class CompletenessEvaluationWindow
    {
        public Rating RatingProperty;

        public CompletenessEvaluationWindow(ISST leftTree, ISST rightTree, Settings settings)
        {
            InitializeComponent();

            RatingProperty = Rating.None;

            var xamls = XamlPrinter.PrintOnlyChanges(leftTree, rightTree, settings);

            LeftFile.Xaml = xamls.Item1;
            RightFile.Xaml = xamls.Item2;
            UnifiedDiff.Xaml = xamls.Item3;
        }

        private void CompleteButton(object sender, RoutedEventArgs e)
        {
            RatingProperty = Rating.Complete;
            Close();
        }

        private void IncompleteButton(object sender, RoutedEventArgs e)
        {
            RatingProperty = Rating.Incomplete;
            Close();
        }

        private void NoneButton(object sender, RoutedEventArgs e)
        {
            RatingProperty = Rating.None;
            Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using DiffDisplay.Util;
using DiffTestBench;
using DiffTestBench.Diff;
using KaVE.VS.FeedbackGenerator.SessionManager.Presentation;
using SSTDiff;
using SSTDiff.EditScript.EditOperations;

namespace DiffDisplay.UI
{
    /// <summary>
    ///     Interaction logic for RealCasesViewer.xaml
    /// </summary>
    public partial class RealCasesViewer : Window
    {
        private readonly RealCasesViewerModel _realCasesViewerModel;
        private readonly Settings _settings;

        public RealCasesViewer()
        {
            InitializeComponent();
            _realCasesViewerModel = new RealCasesViewerModel();
            
            SetDefaultFolder();

            ContextList.ItemsSource = _realCasesViewerModel.VersionList;
            ContextList.ItemStringFormat = "G";
            ZipList.ItemsSource = _realCasesViewerModel.ZipFileList;

            _settings = new Settings();
            SettingGrid.SetDataContext(_settings);
        }

        private void SetDefaultFolder()
        {
            FolderPath.Text = Program.RootDir;
            _realCasesViewerModel.UpdateZipList(FolderPath.Text);
        }

        private void BrowseFolder(object sender, RoutedEventArgs e)
        {
            FolderPath.Text = FileDialogUtil.GetFolderPath();
            _realCasesViewerModel.UpdateZipList(FolderPath.Text);
        }

        private void UpdateCasesButtonOnClick(object sender, RoutedEventArgs e)
        {
            UpdateClassList();
        }

        private void UpdateClassList()
        {
            if (FolderPath.Text != "")
            {
                ClassList.ItemsSource = _realCasesViewerModel.GetClassList(ZipList.SelectedItems.Cast<string>());
            }
        }

        private void DisplayDiff(object sender, RoutedEventArgs e)
        {
            var selectedItems = ContextList.SelectedItems.Cast<DateTime>().ToList();
            if (selectedItems.Count != 2) MessageBox.Show("Choose two versions");
            else
            {
                var leftVersion = selectedItems[0];
                var rightVersion = selectedItems[1];
                var leftSST = _realCasesViewerModel.GetSSTNode(leftVersion);
                var rightSST = _realCasesViewerModel.GetSSTNode(rightVersion);

                LeftDateTime.Content = leftVersion.ToString("G", CultureInfo.CurrentUICulture);
                RightDateTime.Content = rightVersion.ToString("G", CultureInfo.CurrentUICulture);

                LeftFile.ClearValue(XamlBindableRichTextBox.XamlProperty);
                RightFile.ClearValue(XamlBindableRichTextBox.XamlProperty);
                UnifiedDiff.ClearValue(XamlBindableRichTextBox.XamlProperty);

                if (DisplayOnlyChangesCheckBox.IsChecked != null && DisplayOnlyChangesCheckBox.IsChecked.Value)
                {
                    var xamls = XamlPrinter.PrintOnlyChanges(leftSST, rightSST, _settings);

                    LeftFile.Xaml = xamls.Item1;
                    RightFile.Xaml = xamls.Item2;
                    UnifiedDiff.Xaml = xamls.Item3;
                }
                else
                {
                    LeftFile.Xaml = XamlPrinter.PrintSST(leftSST);
                    RightFile.Xaml = XamlPrinter.PrintSST(rightSST);
                    UnifiedDiff.Xaml = XamlPrinter.PrintDiff(leftSST, rightSST, _settings);
                }
                
            }
        }


        private void DisplayVersions(object sender, RoutedEventArgs e)
        {
            if (ClassList.SelectionBoxItem.Equals("")) return;

            _realCasesViewerModel.UpdateContextList((string) ClassList.SelectionBoxItem);
        }
    }
}
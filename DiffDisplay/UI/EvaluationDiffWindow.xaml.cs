﻿using System;
using System.Windows;
using DiffTestBench.Diff;
using KaVE.Commons.Model.SSTs;
using SSTDiff;
using SSTDiff.Util;

namespace DiffDisplay.UI
{
    /// <summary>
    /// Interaction logic for EvaluationDiffWindow.xaml
    /// </summary>
    public partial class EvaluationDiffWindow
    {
        public Rating RatingProperty;
        
        public EvaluationDiffWindow(ISST leftTree, ISST rightTree, Settings referenceSettings, Settings settings)
        {
            InitializeComponent();

            RatingProperty = Rating.None;

            var xamlsReference = XamlPrinter.PrintOnlyChanges(leftTree.DeepCloneNode(), rightTree.DeepCloneNode(), referenceSettings);
            var xamlsImproved = XamlPrinter.PrintOnlyChanges(leftTree.DeepCloneNode(), rightTree.DeepCloneNode(), settings);

            LeftFile.Xaml = xamlsReference.Item1;
            RightFile.Xaml = xamlsReference.Item2;
            UnifiedDiffLeft.Xaml = xamlsReference.Item3;
            UnifiedDiffRight.Xaml = xamlsImproved.Item3;
       }

        private void LeftBetterButton(object sender, RoutedEventArgs e)
        {
            RatingProperty = Rating.Worse;
            Close();
        }

        private void EqualButton(object sender, RoutedEventArgs e)
        {
            RatingProperty = Rating.Equal;
            Close();
        }

        private void RightBetterButton(object sender, RoutedEventArgs e)
        {
            RatingProperty = Rating.Better;
            Close();
        }

        private void NoneButton(object sender, RoutedEventArgs e)
        {
            RatingProperty = Rating.None;
            Close();
        }
    }
}

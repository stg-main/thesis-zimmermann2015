﻿using System.Collections.Generic;
using System.IO;
using System.Windows;
using DiffDisplay.Util;
using DiffTestBench;
using DiffTestBench.Diff;
using SSTDiff;
using SSTDiff.EditScript.EditOperations;

namespace DiffDisplay.UI
{
    /// <summary>
    ///     Interaction logic for JsonDiffViewer.xaml
    /// </summary>
    public partial class JsonDiffViewer
    {

        private readonly Settings _settings;

        public JsonDiffViewer()
        {
            InitializeComponent();

            _settings = new Settings();
            SettingGrid.SetDataContext(_settings);
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            if (!File.Exists(PathA.Text) || !File.Exists(PathB.Text))
            {
                MessageBox.Show("One of the file paths is incorrect");
            }
            else
            {
                PrintDiff();
            }
        }

        private void PrintDiff()
        {
            var pathA = PathA.Text;
            var pathB = PathB.Text;

            var leftTree = SSTReader.ReadAndParseSST(pathA);
            var rightTree = SSTReader.ReadAndParseSST(pathB);

            BindableRichTextBox.Xaml = XamlPrinter.PrintDiff(leftTree, rightTree, _settings);
        }

        private void OnBrowseA(object sender, RoutedEventArgs e)
        {
            var filePath = GetJsonFilePath();
            if(filePath != "") PathA.Text = filePath;
        }

        private void OnBrowseB(object sender, RoutedEventArgs e)
        {
            var filePath = GetJsonFilePath();
            if (filePath != "") PathB.Text = filePath;
        }

        private static string GetJsonFilePath()
        {
            return FileDialogUtil.GetFilePath(".json", "Json Files (.json)|*.json", Program.RootDir);
        }

    }
}
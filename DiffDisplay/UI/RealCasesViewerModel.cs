﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using DiffTestBench.Import;
using KaVE.Commons.Model.Events.CompletionEvents;
using KaVE.Commons.Model.SSTs.Visitor;
using SSTDiff.Util;

namespace DiffDisplay.UI
{
    public class RealCasesViewerModel
    {
        private ContextReader _contextReader;
        public Dictionary<string, List<Tuple<Context, DateTime?>>> ClassDictionary;

        public Dictionary<DateTime, Context> ContextDictionary;

        public ObservableCollection<DateTime> VersionList;
        public ObservableCollection<string> ZipFileList;

        public RealCasesViewerModel()
        {
            ContextDictionary = new Dictionary<DateTime, Context>();
            VersionList = new ObservableCollection<DateTime>();
            ZipFileList = new ObservableCollection<string>();
        }

        public IEnumerable<string> GetClassList(IEnumerable<string> selectedZips)
        {
            _contextReader = new ContextReader();
            _contextReader.ReadContextsFromAllZipFiles(selectedZips);
            ClassDictionary = _contextReader.ContextDictionary;
            return ClassDictionary.Keys;
        }

        public void UpdateContextList(string className)
        {
            ContextDictionary.Clear();
            VersionList.Clear();
            var contextTimeTupleList = ClassDictionary[className];
            foreach (var tuple in contextTimeTupleList)
            {
                if (tuple.Item2.HasValue)
                {
                    ContextDictionary.Add(tuple.Item2.Value, tuple.Item1);
                }
            }
            var tempVersionList = ContextDictionary.Keys.ToList().OrderBy(time => time.Ticks);
            foreach (var version in tempVersionList)
            {
                VersionList.Add(version);
            }

        }
        
        public ISSTNode GetSSTNode(DateTime dateTime)
        {
            var context = ContextDictionary[dateTime];
            return context.SST.DeepCloneNode();
        }

        public void UpdateZipList(string folderPath)
        {
            ZipFileList.Clear();
            foreach (var zipPath in ContextReader.FindZipFiles(folderPath))
            {
                ZipFileList.Add(zipPath);
            }
        }
    }
}
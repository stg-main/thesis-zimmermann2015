﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using DiffDisplay.Util;
using DiffTestBench;
using DiffTestBench.Import;
using KaVE.Commons.Model.Events.CompletionEvents;
using KaVE.Commons.Utils.Json;
using SSTDiff;

namespace DiffDisplay.UI
{
    /// <summary>
    ///     Interaction logic for EvaluationViewer.xaml
    /// </summary>
    public partial class EvaluationViewer
    {
        private readonly Settings _settings;
        private readonly Settings _referenceSettings;

        private List<Tuple<string, Context, DateTime>> _contextDictionary;
        private readonly List<PropertyInfo> _settingProperties;

        private int DefaultSeed = 512788;

        private const int DefaultEvaluationAmount = 50;
        private const int TriesTillSearchUnqiueDiffBreaks = 10000;

        public EvaluationViewer()
        {
            InitializeComponent();

            _settingProperties = typeof(Settings).GetProperties().Where(property => property.PropertyType == typeof(bool)).ToList();
            
            SetDefaultFolder();
            _settings = new Settings();
            _referenceSettings = new Settings();
            SettingGrid.SetDataContext(_settings);
            ReferenceSettingGrid.SetDataContext(_referenceSettings);

            DefaultSeed = new Random().Next();
            RandomSeed.Text = DefaultSeed.ToString();
            EvaluationAmount.Text = DefaultEvaluationAmount.ToString();
        }

        private void SetDefaultFolder()
        {
            FolderPath.Text = Program.RootDir;
        }

        private void BrowseFolder(object sender, RoutedEventArgs e)
        {
            FolderPath.Text = FileDialogUtil.GetFolderPath();

        }

        private void ImportContexts(object sender, RoutedEventArgs routedEventArgs)
        {
            Dictionary<string, List<Tuple<Context, DateTime?>>> contextDictionary;
            var pathToContext = Path.Combine(FolderPath.Text, "contextDictionary.json");
            if (File.Exists(pathToContext))
            {
                var contextReader = new ContextReader();
                contextReader.ReadContextsFromSaveFile(pathToContext);
                contextDictionary = contextReader.ContextDictionary;
                TransformContextDictionary(contextDictionary);
            }
            else if (Directory.Exists(FolderPath.Text))
            {
                var contextReader = new ContextReader();
                var zips = Directory.EnumerateFiles(FolderPath.Text).Where(file => file.EndsWith(".zip"));
                contextReader.ReadContextsFromAllZipFiles(zips);
                contextDictionary = contextReader.ContextDictionary;
                TransformContextDictionary(contextDictionary);
            }
            else
            {
                MessageBox.Show("The folder path does not exist");
            }
        }

        private void TransformContextDictionary(Dictionary<string, List<Tuple<Context, DateTime?>>> contextDictionary)
        {
            _contextDictionary = new List<Tuple<string, Context, DateTime>>();
            foreach (var keyValuePair in contextDictionary)
            {
                var contextList =
                    keyValuePair.Value.Where(pair => pair.Item2.HasValue)
                        .Select(pair => Tuple.Create(keyValuePair.Key, pair.Item1, pair.Item2.Value))
                        .ToList();
                // only adds contextList with more than one because else no diffs are possible
                if (contextList.Count > 1) _contextDictionary.AddRange(contextList);
            }
        }

        private void StartEval(object sender, RoutedEventArgs e)
        {
            int seed;
            if(int.TryParse(RandomSeed.Text, out seed))
            {
                var random = new Random(seed);
                var diffsToEvaluate = GenerateDiffToEvaluate(random);
                if (diffsToEvaluate == null)
                {
                    MessageBox.Show("Can't find enough unique diffs for evaluation. Either change seed or reduce amount");
                    return;
                }
                ProcessEvaluation(diffsToEvaluate);
            }
            else
            {
                MessageBox.Show("Wrong input for seed");
            }
        }

        private void ProcessEvaluation(List<Tuple<Tuple<string, Context, DateTime>, Tuple<string, Context, DateTime>>> diffsToEvaluate)
        {
            var betterAmount = 0;
            var equalAmount = 0;
            var worseAmount = 0;
            var noRatingAmount = 0;
            var sb = new StringBuilder();
            for (int i = 0; i < diffsToEvaluate.Count; i++)
            {
                UpdateStatus(i + 1);
                var diff = diffsToEvaluate[i];
                var diffWindow = new EvaluationDiffWindow(diff.Item1.Item2.SST, diff.Item2.Item2.SST, _referenceSettings , _settings);
                diffWindow.ShowDialog();
                var rating = diffWindow.RatingProperty;
                switch (rating)
                {
                    case Rating.Better:
                        betterAmount++;
                        break;
                    case Rating.Equal:
                        equalAmount++;
                        break;
                    case Rating.Worse:
                        worseAmount++;
                        break;
                    case Rating.None:
                        noRatingAmount++;
                        break;
                }
                sb.AppendLine(CreateResultString(diff, rating));
            }
            sb.AppendLine();
            sb.AppendLine("Amount of \"Better\" Ratings: " + betterAmount);
            sb.AppendLine("Amount of \"Equal\" Ratings: " + equalAmount);
            sb.AppendLine("Amount of \"Worse\" Ratings: " + worseAmount);
            sb.AppendLine("Amount of \"None\" Ratings: " + noRatingAmount);
            ResetStatus();
            WriteToFile(sb.ToString());
        }

        private void ResetStatus()
        {
            Status.Content = "";
        }

        private void UpdateStatus(int i)
        {
            Status.Content = "Status: " + i + "/" + EvaluationAmount.Text;
        }

        private void WriteToFile(string result)
        {
            var activatedSettingsString = "";
            for (int i = 0; i < _settingProperties.Count; i++)
            {
                if ((bool) _settingProperties[i].GetValue(_settings)) activatedSettingsString += i;
            }
            result = result.Insert(0,
                string.Format("Evaluated Settings: {0}{1}{2}", Environment.NewLine, Program.GetSelectedSettingsAsString(_settings),
                    Environment.NewLine));
            result = result.Insert(0,
                string.Format("Reference Settings: {0}{1}{2}", Environment.NewLine, Program.GetSelectedSettingsAsString(_referenceSettings),
                    Environment.NewLine));
            File.WriteAllText(Path.Combine(Program.RootDir, "EvaluatationResults" + activatedSettingsString + "Amount" + EvaluationAmount.Text + "Seed" + RandomSeed.Text + ".txt"), result);
        }

        private string CreateResultString(Tuple<Tuple<string, Context, DateTime>, Tuple<string, Context, DateTime>> diff, Rating rating)
        {
            var format = "{0} - {1} / {2} : {3}";
            return string.Format(format,diff.Item1.Item1, diff.Item1.Item3.ToString("G"), diff.Item2.Item3.ToString("G"), rating);
        }
        
        private List<Tuple<Tuple<string,Context, DateTime>, Tuple<string,Context, DateTime>>> GenerateDiffToEvaluate(Random random)
        {
            var diffsToEvaluate = new HashSet<Tuple<Tuple<string, Context, DateTime>, Tuple<string, Context, DateTime>>>();
            for (int i = 0; i < int.Parse(EvaluationAmount.Text); i++)
            {
                Tuple<Tuple<string, Context, DateTime>, Tuple<string, Context, DateTime>> tupleToAdd;
                if (!TryGetUniqueDiffTuple(random, diffsToEvaluate, out tupleToAdd)) return null;
                diffsToEvaluate.Add(tupleToAdd);
            }

            return diffsToEvaluate.ToList();
        }

        private bool TryGetUniqueDiffTuple(Random random, HashSet<Tuple<Tuple<string, Context, DateTime>, Tuple<string, Context, DateTime>>> diffsToEvaluate, out Tuple<Tuple<string, Context, DateTime>, Tuple<string, Context, DateTime>> tupleToAdd)
        {
            var diffFound = false;
            int tries = 0;
            Tuple<Tuple<string, Context, DateTime>, Tuple<string, Context, DateTime>> tempTuple;
            do
            {
                if (tries > TriesTillSearchUnqiueDiffBreaks)
                {
                    tupleToAdd = null;
                    return false;
                }
                var index = random.Next(1, _contextDictionary.Count - 2);

                // diff is linear -> either compares previous or next context

                var contextTuple = _contextDictionary[index];
                var nextContextTuple = _contextDictionary[index + 1];
                var previousContextTuple = _contextDictionary[index - 1];
                
                tempTuple = nextContextTuple.Item2.SST.EnclosingType.Equals(contextTuple.Item2.SST.EnclosingType)
                    ? Tuple.Create(contextTuple, nextContextTuple)
                    : Tuple.Create(previousContextTuple, contextTuple);


                if (tempTuple.Item1.Item2.SST.GetHashCode() != tempTuple.Item2.Item2.SST.GetHashCode() &&
                    !diffsToEvaluate.Contains(tempTuple)) diffFound = true;

                tries++;
            } while (!diffFound);
            tupleToAdd = tempTuple;
            return true;
        }
    }
}
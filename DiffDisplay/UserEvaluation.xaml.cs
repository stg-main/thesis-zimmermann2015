﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows;
using DiffDisplay.UI;
using DiffTestBench;
using KaVE.Commons.Model.Events.CompletionEvents;
using KaVE.Commons.Utils.Json;
using SSTDiff;

namespace DiffDisplay
{
    /// <summary>
    ///     Interaction logic for UserEvaluation.xaml
    /// </summary>
    public partial class UserEvaluation
    {
        private readonly EvaluationSettings _evaluationSettings;

        private readonly Settings _referenceSettings;

        private readonly List<PropertyInfo> _settingProperties;
        
        private Settings _currentSettingsToEvaluate;

        private int _evaluationAmount;

        private readonly
            Dictionary<string, List<Tuple<Tuple<string, Context, DateTime>, Tuple<string, Context, DateTime>>>>
            _diffListDictionary =
                new Dictionary<string, List<Tuple<Tuple<string, Context, DateTime>, Tuple<string, Context, DateTime>>>>();

        protected Dictionary<string, Settings> SettingsToEvaluate = new Dictionary<string, Settings>
        {
            {
                "Wrapping-Unwrapping Detection",
                new Settings
                {
                    ActivateWrappingDetection = true,
                    ActivateUnwrappingDetection = true
                }
            },
            {
                "Block Move Detection",
                new Settings
                {
                    ActivateMoveOfBlockDetection = true
                }
            },
            {
                "Join Insertion-Deletions",
                new Settings
                {
                    ActivateJoinInsertsAndDelete = true
                }
            }
        };

        public UserEvaluation()
        {
            InitializeComponent();

            var pathToSettings = Path.Combine(Program.RootDir, "EvaluationSettings");
            var pathToSettingsRelative = Path.Combine("../", "EvaluationSettings");
            _evaluationSettings = File.Exists(pathToSettingsRelative) ? File.ReadAllText(pathToSettingsRelative).ParseJsonTo<EvaluationSettings>() : File.ReadAllText(pathToSettings).ParseJsonTo<EvaluationSettings>();

            _referenceSettings = new Settings
            {
                ActivateEarlyHashcodeMatchingForMethodDeclarations = true,
                ActivateEarlyHashcodeMatchingForSST = true,
                ActivateMatchingWithParent = true
            };

            _settingProperties =
                typeof (Settings).GetProperties().Where(property => property.PropertyType == typeof (bool)).ToList();

            Status.Content = "Now reading contexts";

            ImportDiffs();

            Status.Content = "Evaluation can be started";
        }

        private void ImportDiffs()
        {
            var settingsToEvaluateNames = SettingsToEvaluate.Keys.ToList();
            var completeness =
                File.ReadAllText(Path.Combine(_evaluationSettings.Path, "Completeness.json"))
                    .ParseJsonTo<List<Tuple<Tuple<string, Context, DateTime>, Tuple<string, Context, DateTime>>>>();

            _diffListDictionary.Add("Completeness", completeness);
            foreach (var settingsToEvaluateName in settingsToEvaluateNames)
            {
                _diffListDictionary.Add(settingsToEvaluateName,
                    File.ReadAllText(Path.Combine(_evaluationSettings.Path, settingsToEvaluateName + ".json"))
                        .ParseJsonTo<List<Tuple<Tuple<string, Context, DateTime>, Tuple<string, Context, DateTime>>>>());
            }
        }

        private void PerformEvaluation()
        {
            var diffsToEvaluate = _diffListDictionary["Completeness"];

            _evaluationAmount = _evaluationSettings.UserEvaluationAmount;
            var index = (_evaluationSettings.DataSetNumber * _evaluationAmount) % _evaluationSettings.EvaluationAmount;
            var partialDiffList = diffsToEvaluate.GetRange(
                index, _evaluationAmount);

            CurrentSetting.Content = "Completeness Evaluation";
            PerformCompletenessEvaluation(partialDiffList);

            foreach (var settings in SettingsToEvaluate)
            {
                CurrentSetting.Content = settings.Key;
                _currentSettingsToEvaluate = settings.Value;
                diffsToEvaluate = _diffListDictionary[settings.Key];
                partialDiffList = diffsToEvaluate.GetRange(index, _evaluationAmount);
                ProcessEvaluation(partialDiffList);
            }

            Status.Content = "Evaluation finished";
            CurrentSetting.Content = "";
            StartButton.IsEnabled = false;
        }

        private void PerformCompletenessEvaluation(
            List<Tuple<Tuple<string, Context, DateTime>, Tuple<string, Context, DateTime>>> diffsToEvaluate)
        {
            var completeAmount = 0;
            var incompleteAmount = 0;
            var noRatingAmount = 0;
            var sb = new StringBuilder();
            var settings = new Settings
            {
                ActivateMatchingWithParent = true,
                ActivateDifferentUpdateOperationDisplay = true,
                ActivateEarlyHashcodeMatchingForMethodDeclarations = true,
                ActivateEarlyHashcodeMatchingForSST = true
            };
            for (var i = 0; i < _evaluationAmount; i++)
            {
                UpdateStatus(i + 1);
                var diff = diffsToEvaluate[i];
                var diffWindow = new CompletenessEvaluationWindow(diff.Item1.Item2.SST, diff.Item2.Item2.SST, settings);
                diffWindow.ShowDialog();
                var rating = diffWindow.RatingProperty;
                switch (rating)
                {
                    case Rating.Complete:
                        completeAmount++;
                        break;
                    case Rating.Incomplete:
                        incompleteAmount++;
                        break;
                    case Rating.None:
                        noRatingAmount++;
                        break;
                }
                sb.AppendLine(CreateResultString(diff, rating));
            }
            sb.AppendLine();
            sb.AppendLine("Amount of \"Complete\" Ratings: " + completeAmount);
            sb.AppendLine("Amount of \"Incomplete\" Ratings: " + incompleteAmount);
            sb.AppendLine("Amount of \"None\" Ratings: " + noRatingAmount);

            var result = sb.ToString();
            result = result.Insert(0,
                string.Format("Evaluated Settings: {0}{1}{2}", Environment.NewLine,
                    Program.GetSelectedSettingsAsString(settings),
                    Environment.NewLine));
            WriteToFile(result, "EvaluatationResultsCompleteness" + "Amount" + _evaluationAmount + "Dataset" + _evaluationSettings.DataSetNumber +
                "Seed" + _evaluationSettings.DefaultSeed + ".txt");
        }

        private void ProcessEvaluation(
            List<Tuple<Tuple<string, Context, DateTime>, Tuple<string, Context, DateTime>>> diffsToEvaluate)
        {
            var betterAmount = 0;
            var equalAmount = 0;
            var worseAmount = 0;
            var noRatingAmount = 0;
            var sb = new StringBuilder();
            for (var i = 0; i < _evaluationAmount; i++)
            {
                UpdateStatus(i + 1);
                var diff = diffsToEvaluate[i];
                var diffWindow = new EvaluationDiffWindow(diff.Item1.Item2.SST, diff.Item2.Item2.SST, _referenceSettings,
                    _currentSettingsToEvaluate);
                diffWindow.ShowDialog();
                var rating = diffWindow.RatingProperty;
                switch (rating)
                {
                    case Rating.Better:
                        betterAmount++;
                        break;
                    case Rating.Equal:
                        equalAmount++;
                        break;
                    case Rating.Worse:
                        worseAmount++;
                        break;
                    case Rating.None:
                        noRatingAmount++;
                        break;
                }
                sb.AppendLine(CreateResultString(diff, rating));
            }
            sb.AppendLine();
            sb.AppendLine("Amount of \"Better\" Ratings: " + betterAmount);
            sb.AppendLine("Amount of \"Equal\" Ratings: " + equalAmount);
            sb.AppendLine("Amount of \"Worse\" Ratings: " + worseAmount);
            sb.AppendLine("Amount of \"None\" Ratings: " + noRatingAmount);
            ResetStatus();

            var activatedSettingsString = "";
            for (var i = 0; i < _settingProperties.Count; i++)
            {
                if ((bool) _settingProperties[i].GetValue(_currentSettingsToEvaluate)) activatedSettingsString += i;
            }
            var result = sb.ToString();
            result = result.Insert(0,
                string.Format("Evaluated Settings: {0}{1}{2}", Environment.NewLine,
                    Program.GetSelectedSettingsAsString(_currentSettingsToEvaluate),
                    Environment.NewLine));
            result = result.Insert(0,
                string.Format("Reference Settings: {0}{1}{2}", Environment.NewLine,
                    Program.GetSelectedSettingsAsString(_referenceSettings),
                    Environment.NewLine));
            WriteToFile(result,
                "EvaluatationResults" + activatedSettingsString + "Amount" + _evaluationAmount + "Dataset" + _evaluationSettings.DataSetNumber + "Seed" +
                _evaluationSettings.DefaultSeed + ".txt");
        }

        private void ResetStatus()
        {
            Status.Content = "Evaluation in progress";
        }

        private void UpdateStatus(int i)
        {
            Status.Content = "Status: " + i + "/" + _evaluationAmount;
        }

        private void WriteToFile(string result, string fileName)
        {
            File.WriteAllText(
                Path.Combine(_evaluationSettings.Path,
                    fileName), result);
        }

        private string CreateResultString(
            Tuple<Tuple<string, Context, DateTime>, Tuple<string, Context, DateTime>> diff, Rating rating)
        {
            var format = "{0} - {1} / {2} : {3}";
            return string.Format(format, diff.Item1.Item1, diff.Item1.Item3.ToString("G"),
                diff.Item2.Item3.ToString("G"), rating);
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            PerformEvaluation();
        }
    }
}
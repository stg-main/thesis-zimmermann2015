﻿using System.Windows.Forms;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;

namespace DiffDisplay.Util
{
    public class FileDialogUtil
    {
        public static string GetFilePath(string defaultExt, string filter, string initialDirectory)
        {
            var dialog = new OpenFileDialog
            {
                DefaultExt = defaultExt,
                Filter = filter,
                InitialDirectory = initialDirectory,
            };
            var result = dialog.ShowDialog();
            if (result.HasValue && result.Value)
            {
                return dialog.FileName;
            }
            return "";
        }

        public static string GetFolderPath()
        {
            var dialog = new FolderBrowserDialog();
            dialog.ShowDialog();
            return dialog.SelectedPath;
        }
    }
}
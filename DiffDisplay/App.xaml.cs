﻿using System;
using System.IO;
using System.Windows;
using DiffTestBench;
using KaVE.Commons.Utils.Json;

namespace DiffDisplay
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void App_OnStartup(object sender, StartupEventArgs e)
        {
            var pathToSettingsRelative = Path.Combine("../", "EvaluationSettings");
            var pathToSettingsRootDir = Path.Combine(Program.RootDir, "EvaluationSettings");
            if (File.Exists(pathToSettingsRelative))
            {
                if(OpenUserEvaluation(pathToSettingsRelative)) return;
            }
            if (File.Exists(pathToSettingsRootDir))
            {
                if (OpenUserEvaluation(pathToSettingsRootDir)) return;
            }
            StartupUri = new Uri("/DiffDisplay;component/Menu.xaml", UriKind.Relative);
        }

        private bool OpenUserEvaluation(string pathToSettingsRootDir)
        {
            var evalSettings = File.ReadAllText(pathToSettingsRootDir).ParseJsonTo<EvaluationSettings>();
            if (evalSettings.EvaluationMode)
            {
                StartupUri = new Uri("/DiffDisplay;component/UserEvaluation.xaml", UriKind.Relative);
                return true;
            }
            return false;
        }
    }
}

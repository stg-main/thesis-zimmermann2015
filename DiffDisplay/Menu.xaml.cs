﻿using System.Windows;
using DiffDisplay.UI;

namespace DiffDisplay
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void BaseCases_OnClick(object sender, RoutedEventArgs e)
        {
            new BaseCasesViewer().Show();
        }

        private void RealCases_OnClick(object sender, RoutedEventArgs e)
        {
            new RealCasesViewer().Show();
        }

        private void FileDiffViewer_OnClick(object sender, RoutedEventArgs e)
        {
            new JsonDiffViewer().Show();
        }

        private void EvaluationViewer_OnClick(object sender, RoutedEventArgs e)
        {
            new EvaluationViewer().Show();
        }
    }
}

namespace DiffDisplay
{
    public enum Rating
    {
        Better,
        Equal,
        Worse,
        Complete,
        Incomplete,
        None
    }
}
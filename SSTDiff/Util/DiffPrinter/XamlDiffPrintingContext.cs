﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KaVE.Commons.Model.Names;
using KaVE.Commons.Model.Names.CSharp;
using KaVE.Commons.Model.SSTs;
using KaVE.Commons.Model.SSTs.Declarations;
using KaVE.Commons.Model.SSTs.Visitor;
using KaVE.Commons.Model.TypeShapes;
using KaVE.Commons.Utils.Collections;
using SSTDiff.EditScript;
using SSTDiff.EditScript.EditOperations;

namespace SSTDiff.Util.DiffPrinter
{
    public class XamlDiffPrintingContext
    {
        private readonly List<IEditOperation> _editScript;
        private readonly List<Move> _moves;
        private readonly Stack<Tuple<IEditOperation, ISSTNode>> _openedEditOperationNodeTupleSpans;

        private readonly StringBuilder _sb;
        private readonly IKaVESet<INamespaceName> _seenNamespaces;
        private readonly Settings _settings;

        public XamlDiffPrintingContext(List<IEditOperation> editScript, Settings settings)
        {
            _editScript = editScript;
            _settings = settings;
            _moves = editScript.OfType<Move>().ToList();
            _openedEditOperationNodeTupleSpans = new Stack<Tuple<IEditOperation, ISSTNode>>();
            _sb = new StringBuilder();
            _seenNamespaces = Sets.NewHashSet<INamespaceName>();
        }

        /// <summary>
        ///     Base indentation level to use while printing SST nodes.
        /// </summary>
        public int IndentationLevel { get; set; }

        /// <summary>
        ///     Type shape (supertype information) of the SST. If the SST is a type and a type shape is
        ///     provided, the supertypes will be included in the print result.
        /// </summary>
        public ITypeShape TypeShape { get; set; }

        /// <summary>
        ///     Collection of namespaces that have been seen by the context while processing an SST.
        /// </summary>
        public IEnumerable<INamespaceName> SeenNamespaces
        {
            get { return _seenNamespaces.AsEnumerable(); }
        }

        /// <summary>
        ///     Appends a string to the context.
        /// </summary>
        /// <param name="text">The string to append.</param>
        /// <returns>The context after appending.</returns>
        public virtual XamlDiffPrintingContext Text(string text)
        {
            _sb.Append(text);
            return this;
        }


        /// <summary>
        ///     Appends a line break to the context.
        /// </summary>
        /// <returns>The context after appending.</returns>
        public XamlDiffPrintingContext NewLine()
        {
            _sb.AppendLine();
            return this;
        }

        /// <summary>
        ///     Appends a space to the context.
        /// </summary>
        /// <returns>The context after appending.</returns>
        public XamlDiffPrintingContext Space()
        {
            _sb.Append(" ");
            return this;
        }

        /// <summary>
        ///     Appends the appropriate amount of indentation to the context according to the current indentation level. Should
        ///     always be used after appending a line break.
        /// </summary>
        /// <returns>The context after appending.</returns>
        public XamlDiffPrintingContext Indentation()
        {
            for (var i = 0; i < IndentationLevel; i++)
            {
                _sb.Append("    ");
            }
            return this;
        }

        /// <summary>
        ///     Formats and appends a type name together with its generic types to the context.
        /// </summary>
        /// <param name="typeName">The type name to append.</param>
        /// <returns>The context after appending.</returns>
        public XamlDiffPrintingContext Type(ITypeName typeName)
        {
            _seenNamespaces.Add(typeName.Namespace);

            TypeNameOnly(typeName);

            if (typeName.HasTypeParameters)
            {
                TypeParameters(typeName.TypeParameters);
            }

            return this;
        }

        public XamlDiffPrintingContext TypeParameters(IList<ITypeName> typeParameters)
        {
            LeftAngleBracket();

            foreach (var p in typeParameters)
            {
                if (p.IsUnknownType || p.TypeParameterType.IsUnknownType)
                {
                    TypeParameterShortName(TypeName.UnknownName.Identifier);
                }
                else
                {
                    Type(p.TypeParameterType);
                }

                if (!ReferenceEquals(p, typeParameters.Last()))
                {
                    _sb.Append(", ");
                }
            }

            RightAngleBracket();

            return this;
        }

        /// <summary>
        ///     Formats and appends a parameter list to the context.
        /// </summary>
        /// <param name="parameters">The list of parameters to append.</param>
        /// <returns>The context after appending.</returns>
        public XamlDiffPrintingContext ParameterList(IList<IParameterName> parameters)
        {
            Text("(");

            foreach (var parameter in parameters)
            {
                if (parameter.IsPassedByReference && parameter.ValueType.IsValueType)
                {
                    Keyword("ref").Space();
                }

                if (parameter.IsOutput)
                {
                    Keyword("out").Space();
                }

                if (parameter.IsOptional)
                {
                    Keyword("opt").Space();
                }

                if (parameter.IsParameterArray)
                {
                    Keyword("params").Space();
                }

                Type(parameter.ValueType).Space().Text(parameter.Name);

                if (!ReferenceEquals(parameter, parameters.Last()))
                {
                    Text(",").Space();
                }
            }

            _sb.Append(")");

            return this;
        }

        public XamlDiffPrintingContext MethodParameterList(IEnumerable<IParameterDeclaration> parameters,
            ISSTNodeVisitor<XamlDiffPrintingContext> visitor)
        {
            Text("(");

            var parameterDeclarations = parameters as IList<IParameterDeclaration> ?? parameters.ToList();
            foreach (var parameter in parameterDeclarations)
            {
                EditOperationEnum editOperationType;
                var wrapping = GetWrappingForNode(parameter, out editOperationType);

                Text(wrapping);

                if (parameter.IsPassedByReference && parameter.Type.TypeName.IsValueType)
                {
                    Keyword("ref").Space();
                }

                if (parameter.IsOutput)
                {
                    Keyword("out").Space();
                }

                if (parameter.IsOptional)
                {
                    Keyword("opt").Space();
                }

                if (parameter.IsParameterArray)
                {
                    Keyword("params").Space();
                }

                if (wrapping != string.Empty) CloseSpanAndPopOperationFromStack();

                parameter.Accept(visitor, this);

                if (!ReferenceEquals(parameter, parameterDeclarations.Last()))
                {
                    Text(",").Space();
                }
            }

            _sb.Append(")");

            return this;
        }

        public XamlDiffPrintingContext StatementBlock(IKaVEList<IStatement> block,
            ISSTNodeVisitor<XamlDiffPrintingContext> visitor,
            bool withBrackets = true, bool noIndentation = false)
        {
            if (!block.Any())
            {
                if (withBrackets)
                {
                    Text(" { }");
                }

                return this;
            }

            if (withBrackets)
            {
                NewLine().Indentation().Text("{");
            }

            if (!noIndentation)
            {
                IndentationLevel++;
            }

            foreach (var statement in block)
            {
                NewLine();
                statement.Accept(visitor, this);
            }

            if (!noIndentation)
            {
                IndentationLevel--;
            }

            if (withBrackets)
            {
                NewLine().Indentation().Text("}");
            }

            return this;
        }

        public XamlDiffPrintingContext LeftAngleBracket()
        {
            return Text("&lt;");
        }

        public XamlDiffPrintingContext RightAngleBracket()
        {
            return Text("&gt;");
        }

        public XamlDiffPrintingContext Keyword(string keyword)
        {
            Text("<Span Foreground=\"Blue\">");
            Text(keyword);
            Text("</Span>");
            return this;
        }

        public XamlDiffPrintingContext UnknownMarker()
        {
            Text("<Span Foreground=\"Blue\">");
            Text("???");
            Text("</Span>");
            return this;
        }

        public XamlDiffPrintingContext CursorPosition()
        {
            Text("<Bold>");
            Text("$");
            Text("</Bold>");
            return this;
        }

        public XamlDiffPrintingContext TypeNameOnly(ITypeName typeName)
        {
            var color = "#2B91AF";

            if (typeName.IsInterfaceType)
            {
                color = "#4EC9B0";
            }
            else if (typeName.IsValueType)
            {
                color = "Blue";
            }

            Text(string.Format("<Span Foreground=\"{0}\">", color));
            Text(typeName.Name);
            Text("</Span>");
            return this;
        }

        protected XamlDiffPrintingContext TypeParameterShortName(string typeParameterShortName)
        {
            Text("<Bold>");
            Text(typeParameterShortName);
            Text("</Bold>");
            return this;
        }

        public XamlDiffPrintingContext StringLiteral(string value)
        {
            Text("<Span Foreground=\"#A31515\">");
            Text("\"").Text(value).Text("\"");
            Text("</Span>");
            return this;
        }

        public XamlDiffPrintingContext Comment(string commentText)
        {
            Text("<Span Foreground=\"#8F8F8F\">");
            Text(commentText);
            Text("</Span>");
            return this;
        }

        private IEditOperation GetEditOperationFor(ISSTNode node)
        {
            return _editScript.FirstOrDefault(edit => edit.ContainsNode(node));
        }

        public string GetWrappingForNode(ISSTNode node, out EditOperationEnum editOperationType)
        {
            var editOperation = GetEditOperationFor(node);
            if (editOperation != null)
            {
                _openedEditOperationNodeTupleSpans.Push(Tuple.Create(editOperation, node));
                editOperationType = editOperation.EditOperation;
                switch (editOperation.EditOperation)
                {
                    case EditOperationEnum.Insert:
                        return "<Span Background=\"GreenYellow\">";
                    case EditOperationEnum.Delete:
                        return "<Span Background=\"Tomato\">";
                    case EditOperationEnum.Update:
                        return "<Span Background=\"Gold\">";
                    case EditOperationEnum.Move:
                        var move = (Move) editOperation;
                        return ReferenceEquals(node, move.MovedNode)
                            ? "<Span Background=\"LightGray\">"
                            : "<Span Background=\"LightGray\" TextDecorations=\"Strikethrough\">";
                }
            }
            editOperationType = EditOperationEnum.None;
            return "";
        }

        public XamlDiffPrintingContext CloseSpanAndPopOperationFromStack()
        {
            // TODO: Search for Open Spans here
            var currentEditOperationNodeTuple = _openedEditOperationNodeTupleSpans.Pop();
            // ReSharper disable once SwitchStatementMissingSomeCases
            switch (currentEditOperationNodeTuple.Item1.EditOperation)
            {
                case EditOperationEnum.Move:
                    CloseSpan();
                    var move = (Move) currentEditOperationNodeTuple.Item1;
                    Text(
                        string.Format(
                            "<Span FontSize=\"8pt\" Background=\"LightGray\" BaselineAlignment=\"Superscript\">({0})</Span>",
                            _moves.IndexOf(move) + 1));

                    var overridenEdit = _editScript.FindOverridenEditForMovedNode(move,
                        currentEditOperationNodeTuple.Item2);
                    if (overridenEdit != null)
                    {
                        var overridenInsertByMove = overridenEdit as Insert;
                        var overridenDeleteByMove = overridenEdit as Delete;
                        var overridenMoveByMove = overridenEdit as Move;

                        if (overridenInsertByMove != null)
                        {
                            Text(string.Format(" <Span Background=\"GreenYellow\">{0}</Span>",
                                overridenInsertByMove.InsertedNode.GetDiffPrinterValue(_editScript, _settings)));
                        }
                        if (overridenDeleteByMove != null)
                        {
                            Text(string.Format(" <Span Background=\"Tomato\">{0}</Span>",
                                overridenDeleteByMove.DeletedNode.GetDiffPrinterValue(_editScript, _settings)));
                        }
                        if (overridenMoveByMove != null)
                        {
                            Text(string.Format(" <Span Background=\"LightGray\">{0}</Span>",
                                overridenMoveByMove.MovedNode.GetDiffPrinterValue(_editScript, _settings)));
                        }
                    }
                    break;
                case EditOperationEnum.Update:
                    CloseSpan();
                    if (_settings.ActivateDifferentUpdateOperationDisplay)
                    {
                        var moveWithUpdatedNode =
                            _moves.FirstOrDefault(
                                editOperation => editOperation.ContainsNode(currentEditOperationNodeTuple.Item2));
                        if (moveWithUpdatedNode != null)
                        {
                            Text(
                                string.Format(
                                    "<Span FontSize=\"8pt\" Background=\"LightGray\" BaselineAlignment=\"Superscript\">({0})</Span>",
                                    _moves.IndexOf(moveWithUpdatedNode) + 1));
                        }
                        var update = (Update) currentEditOperationNodeTuple.Item1;
                        Text(string.Format(" <Span TextDecorations=\"Strikethrough\" Background=\"Gold\">{0}</Span>",
                            update.UpdatedNodeBackup.GetDiffPrinterValue(_editScript, _settings)));
                    }
                    break;
                case EditOperationEnum.Insert:
                    CloseSpan();
                    var insert = (Insert) currentEditOperationNodeTuple.Item1;
                    var overridenDeleteByInsert = _editScript.FindOverridenDeleteForInsertedNode(insert,
                        insert.InsertedNode);
                    if (overridenDeleteByInsert != null)
                    {
                        Text(string.Format(" <Span Background=\"Tomato\">{0}</Span>",
                            overridenDeleteByInsert.DeletedNode.GetDiffPrinterValue(_editScript, _settings)));
                    }
                    break;
                default:
                    CloseSpan();
                    break;
            }
            return this;
        }

        public XamlDiffPrintingContext CloseSpan()
        {
            Text("</Span>");
            return this;
        }

        public override string ToString()
        {
            return _sb.ToString();
        }
    }
}
﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using KaVE.Commons.Model.SSTs;
using KaVE.Commons.Model.SSTs.Blocks;
using KaVE.Commons.Model.SSTs.Declarations;
using KaVE.Commons.Model.SSTs.Expressions.Assignable;
using KaVE.Commons.Model.SSTs.Expressions.LoopHeader;
using KaVE.Commons.Model.SSTs.Expressions.Simple;
using KaVE.Commons.Model.SSTs.Impl.References;
using KaVE.Commons.Model.SSTs.References;
using KaVE.Commons.Model.SSTs.Statements;
using KaVE.Commons.Model.SSTs.Visitor;
using KaVE.Commons.Utils.Collections;
using SSTDiff.EditScript.EditOperations;
using SSTDiff.Matching;

namespace SSTDiff.Util.DiffPrinter
{
    public class DiffPrintingVisitor : ISSTNodeVisitor<XamlDiffPrintingContext>
    {
        private readonly Dictionary<Match, bool> _changedMethodsDictionary;

        public DiffPrintingVisitor(Dictionary<Match,bool> changedMethodsDictionary)
        {
            _changedMethodsDictionary = changedMethodsDictionary;
        }

        public void Visit(ISST sst, XamlDiffPrintingContext c)
        {
            c.Indentation();

            if (sst.EnclosingType.IsInterfaceType)
            {
                c.Keyword("interface");
            }
            else if (sst.EnclosingType.IsEnumType)
            {
                c.Keyword("enum");
            }
            else if (sst.EnclosingType.IsStructType)
            {
                c.Keyword("struct");
            }
            else
            {
                c.Keyword("class");
            }

            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(sst, out editOperationType);

            c.Space().Text(wrapping).Type(sst.EnclosingType);

            CloseWrapping(c, wrapping, true);

            if (c.TypeShape != null && c.TypeShape.TypeHierarchy.HasSupertypes)
            {
                c.Text(" : ");

                if (c.TypeShape.TypeHierarchy.HasSuperclass && c.TypeShape.TypeHierarchy.Extends != null)
                {
                    c.Type(c.TypeShape.TypeHierarchy.Extends.Element);

                    if (c.TypeShape.TypeHierarchy.IsImplementingInterfaces)
                    {
                        c.Text(", ");
                    }
                }

                foreach (var i in c.TypeShape.TypeHierarchy.Implements)
                {
                    c.Type(i.Element);

                    if (!ReferenceEquals(i, c.TypeShape.TypeHierarchy.Implements.Last()))
                    {
                        c.Text(", ");
                    }
                }
            }

            c.NewLine()
                .Indentation().Text("{").NewLine();

            c.IndentationLevel++;

            AppendMemberDeclarationGroup(c, sst.Delegates);
            AppendMemberDeclarationGroup(c, sst.Events);
            AppendMemberDeclarationGroup(c, sst.Fields);
            AppendMemberDeclarationGroup(c, sst.Properties);
            if (!_changedMethodsDictionary.Any()) AppendMemberDeclarationGroup(c, sst.Methods, 2, 1);
            else
            {
                AppendMethodDeclarationGroup(c, sst.Methods, 2, 1);
            }

            c.IndentationLevel--;

            c.Indentation().Text("}");
        }

        public void Visit(IDelegateDeclaration stmt, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(stmt, out editOperationType);

            c.Indentation()
                .Text(wrapping)
                .Keyword("delegate").Space().Type(stmt.Name)
                .ParameterList(stmt.Name.Parameters)
                .Text(";");

            CloseWrapping(c, wrapping, true);
        }

        public void Visit(IEventDeclaration stmt, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(stmt, out editOperationType);

            c.Indentation()
                .Text(wrapping)
                .Keyword("event")
                .Space()
                .Type(stmt.Name.HandlerType)
                .Space()
                .Text(stmt.Name.Name)
                .Text(";");

            CloseWrapping(c, wrapping, true);
        }

        public void Visit(IFieldDeclaration stmt, XamlDiffPrintingContext c)
        {
            c.Indentation();

            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(stmt, out editOperationType);

            c.Text(wrapping);

            if (stmt.IsStatic)
            {
                c.Keyword("static").Space();
            }
            CloseWrapping(c, wrapping, true);

            stmt.ValueType.Accept(this, c);
            c.Space();
            stmt.FieldName.Accept(this, c);
        }

        public void Visit(IMethodDeclaration stmt, XamlDiffPrintingContext c)
        {
            c.Indentation();

            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(stmt, out editOperationType);

            c.Text(wrapping);

            if (stmt.IsStatic)
            {
                c.Keyword("static").Space();
            }

            if (editOperationType == EditOperationEnum.Update) CloseWrapping(c, wrapping, true);

            stmt.ReturnType.Accept(this, c);

            c.Space();

            stmt.MethodName.Accept(this, c);

            if (stmt.Name.HasTypeParameters)
            {
                c.TypeParameters(stmt.Name.TypeParameters);
            }

            c.MethodParameterList(stmt.Parameters, this);

            c.StatementBlock(stmt.Body, this);

            if (editOperationType != EditOperationEnum.Update) CloseWrapping(c, wrapping, true);
        }

        public void Visit(IParameterDeclaration decl, XamlDiffPrintingContext c)
        {
            decl.Type.Accept(this, c);
            c.Space();
            decl.Name.Accept(this, c);
        }

        public void Visit(IPropertyDeclaration stmt, XamlDiffPrintingContext c)
        {
            c.Indentation();

            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(stmt, out editOperationType);

            c.Text(wrapping);

            c.Type(stmt.Name.ValueType).Space().Text(stmt.Name.Name);

            var hasBody = stmt.Get.Any() || stmt.Set.Any();

            if (hasBody) // Long version: At least one body exists --> line breaks + indentation
            {
                c.NewLine()
                    .Indentation();

                CloseWrapping(c, wrapping, true);

                c.IndentationLevel++;

                c.Text("{").NewLine();

                if (stmt.Name.HasGetter)
                {
                    AppendPropertyAccessor(c, stmt.Get, "get");
                }

                if (stmt.Name.HasSetter)
                {
                    AppendPropertyAccessor(c, stmt.Set, "set");
                }

                c.IndentationLevel--;

                c.Indentation().Text("}");
            }
            else // Short Version: No bodies --> getter/setter declaration in same line
            {
                c.Text(" { ");
                if (stmt.Name.HasGetter)
                {
                    c.Keyword("get").Text(";").Space();
                }
                if (stmt.Name.HasSetter)
                {
                    c.Keyword("set").Text(";").Space();
                }
                c.Text("}");

                CloseWrapping(c, wrapping, true);
            }
        }

        public void Visit(IVariableDeclaration stmt, XamlDiffPrintingContext c)
        {
            c.Indentation();

            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(stmt, out editOperationType);

            c.Text(wrapping).Type(stmt.Type).Space();
            stmt.Reference.Accept(this, c);
            c.Text(";");
            CloseWrapping(c,wrapping, true);
        }

        public void Visit(IAssignment stmt, XamlDiffPrintingContext c)
        {
            c.Indentation();
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(stmt, out editOperationType);

            c.Text(wrapping);
            stmt.Reference.Accept(this, c);
            c.Text(" = ");
            stmt.Expression.Accept(this, c);
            c.Text(";");

            CloseWrapping(c, wrapping, true);
        }

        public void Visit(IBreakStatement stmt, XamlDiffPrintingContext c)
        {
            c.Indentation();

            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(stmt, out editOperationType);

            c.Text(wrapping)
                .Keyword("break").Text(";");

            CloseWrapping(c, wrapping, true);
        }

        public void Visit(IContinueStatement stmt, XamlDiffPrintingContext c)
        {
            c.Indentation();

            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(stmt, out editOperationType);

            c.Text(wrapping).Keyword("continue").Text(";");

            CloseWrapping(c, wrapping, true);
        }

        public void Visit(IEventSubscriptionStatement stmt, XamlDiffPrintingContext c)
        {
            c.Indentation();

            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(stmt, out editOperationType);

            c.Text(wrapping);

            stmt.Reference.Accept(this, c);
            c.Space();

            if (stmt.Operation == EventSubscriptionOperation.Add)
            {
                c.Text("+=");
            }
            else if (stmt.Operation == EventSubscriptionOperation.Remove)
            {
                c.Text("-=");
            }

            c.Space();
            stmt.Expression.Accept(this, c);
            c.Text(";");

            CloseWrapping(c, wrapping, true);
        }

        public void Visit(IExpressionStatement stmt, XamlDiffPrintingContext c)
        {
            c.Indentation();
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(stmt, out editOperationType);
            c.Text(wrapping);
            stmt.Expression.Accept(this, c);
            c.Text(";");
            CloseWrapping(c, wrapping, true);
        }

        public void Visit(IGotoStatement stmt, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(stmt, out editOperationType);

            c.Indentation().Text(wrapping).Keyword("goto").Space().Text(stmt.Label).Text(";");

            CloseWrapping(c, wrapping, true);
        }

        public void Visit(ILabelledStatement stmt, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(stmt, out editOperationType);

            c.Indentation().Text(wrapping).Keyword(stmt.Label).Text(":").NewLine();
            CloseWrapping(c, wrapping, true);
            stmt.Statement.Accept(this, c);
        }

        public void Visit(IReturnStatement stmt, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(stmt, out editOperationType);


            c.Indentation().Text(wrapping).Keyword("return");

            if (!stmt.IsVoid)
            {
                c.Space();
                stmt.Expression.Accept(this, c);
            }

            c.Text(";");

            CloseWrapping(c, wrapping, true);
        }

        public void Visit(IThrowStatement stmt, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(stmt, out editOperationType);

            c.Indentation().Text(wrapping).Keyword("throw");

            if (!stmt.IsReThrow)
            {
                c.Space();
                stmt.Reference.Accept(this, c);
            }

            c.Text(";");

            CloseWrapping(c, wrapping, true);
        }

        public void Visit(IDoLoop block, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(block, out editOperationType);

            c.Indentation().Text(wrapping).Keyword("do");
            CloseWrapping(c, wrapping, false);

            c.StatementBlock(block.Body, this);

            c.NewLine().Indentation().Text(wrapping).Keyword("while").Space().Text("(");
            c.IndentationLevel++;
            block.Condition.Accept(this, c);
            c.IndentationLevel--;
            c.NewLine().Indentation().Text(")");
            CloseWrapping(c, wrapping, true);
        }

        public void Visit(IForEachLoop block, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(block, out editOperationType);

            c.Indentation()
                .Text(wrapping)
                .Keyword("foreach").Space().Text("(")
                .Type(block.Declaration.Type)
                .Space();
            block.Declaration.Reference.Accept(this, c);
            c.Space().Keyword("in").Space();
            block.LoopedReference.Accept(this, c);
            c.Text(")");
            CloseWrapping(c, wrapping, true);

            c.StatementBlock(block.Body, this);
        }

        public void Visit(IForLoop block, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(block, out editOperationType);

            c.Indentation().Text(wrapping).Keyword("for").Space().Text("(");

            CloseWrapping(c, wrapping, false);
            c.IndentationLevel++;

            c.StatementBlock(block.Init, this);
            c.Text(wrapping).Text(";");
            CloseWrapping(c, wrapping, false);
            block.Condition.Accept(this, c);
            c.Text(wrapping).Text(";");
            CloseWrapping(c, wrapping, false);
            c.StatementBlock(block.Step, this);

            c.IndentationLevel--;

            c.NewLine().Indentation().Text(wrapping).Text(")");
            CloseWrapping(c, wrapping, true);

            c.StatementBlock(block.Body, this);
        }

        public void Visit(IIfElseBlock block, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(block, out editOperationType);

            c.Indentation().Text(wrapping).Keyword("if").Space().Text("(");
            block.Condition.Accept(this, c);
            c.Text(")");
            CloseWrapping(c, wrapping, !block.Else.Any());

            c.StatementBlock(block.Then, this);

            if (block.Else.Any())
            {
                c.NewLine().Indentation().Text(wrapping).Keyword("else");
                CloseWrapping(c, wrapping, true);

                c.StatementBlock(block.Else, this);
            }
        }

        public void Visit(ILockBlock stmt, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(stmt, out editOperationType);

            c.Indentation().Text(wrapping).Keyword("lock").Space().Text("(");
            CloseWrapping(c, wrapping, false);
            stmt.Reference.Accept(this, c);
            c.Text(wrapping).Text(")");
            CloseWrapping(c, wrapping, true);
            c.StatementBlock(stmt.Body, this);
        }

        public void Visit(ISwitchBlock block, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(block, out editOperationType);

            c.Indentation().Text(wrapping).Keyword("switch").Space().Text("(");
            CloseWrapping(c, wrapping, false);
            block.Reference.Accept(this, c);
            c.Text(wrapping).Text(")").NewLine().Indentation();
            c.IndentationLevel++;
            c.Text("{");
            CloseWrapping(c, wrapping, true);
            foreach (var section in block.Sections)
            {
                c.NewLine().Indentation().Keyword("case").Space();
                section.Label.Accept(this, c);
                c.Text(":").StatementBlock(section.Body, this, false);
            }

            if (block.DefaultSection.Any())
            {
                c.NewLine().Indentation().Keyword("default").Text(":")
                    .StatementBlock(block.DefaultSection, this, false);
            }

            c.NewLine();
            c.IndentationLevel--;
            c.Indentation().Text("}");
        }

        public void Visit(ITryBlock block, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(block, out editOperationType);

            c.Indentation().Text(wrapping).Keyword("try");
            CloseWrapping(c, wrapping, false);

            c.StatementBlock(block.Body, this);

            wrapping = c.GetWrappingForNode(block, out editOperationType);
            c.Text(wrapping);
            foreach (var catchBlock in block.CatchBlocks)
            {
                c.NewLine().Indentation().Keyword("catch");

                if (catchBlock.Kind != CatchBlockKind.General)
                {
                    c.Space().Text("(")
                        .Type(catchBlock.Parameter.ValueType);

                    if (catchBlock.Kind != CatchBlockKind.Unnamed)
                    {
                        c.Space()
                            .Text(catchBlock.Parameter.Name);
                    }

                    c.Text(")");
                }

                c.StatementBlock(catchBlock.Body, this);
            }
            CloseWrapping(c, wrapping, true);

            if (block.Finally.Any())
            {
                c.NewLine().Indentation().Keyword("finally")
                    .StatementBlock(block.Finally, this);
            }
        }

        public void Visit(IUncheckedBlock block, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(block, out editOperationType);

            c.Indentation().Text(wrapping).Keyword("unchecked");
            CloseWrapping(c, wrapping, true);

            c.StatementBlock(block.Body, this);
        }

        public void Visit(IUnsafeBlock block, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(block, out editOperationType);

            c.Indentation().Text(wrapping).Keyword("unsafe").Text(" { ").Comment("/* content ignored */").Text(" }");
            CloseWrapping(c, wrapping, true);
        }

        public void Visit(IUsingBlock block, XamlDiffPrintingContext c)
        {
            if (block.Reference.Equals(new VariableReference()))
            {
                EditOperationEnum editOperationType;
                var wrapping = c.GetWrappingForNode(block, out editOperationType);

                c.Indentation().Text(wrapping).StatementBlock(block.Body, this, false, true);
                CloseWrapping(c, wrapping, true);
            }
            else
            {
                EditOperationEnum editOperationType;
                var wrapping = c.GetWrappingForNode(block, out editOperationType);

                c.Indentation().Text(wrapping).Keyword("using").Space().Text("(");
                CloseWrapping(c, wrapping, true);
                block.Reference.Accept(this, c);
                c.Text(")").StatementBlock(block.Body, this);
            }
        }

        public void Visit(IWhileLoop block, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(block, out editOperationType);

            c.Indentation().Text(wrapping).Keyword("while").Space().Text("(");
            CloseWrapping(c, wrapping, true);
            c.IndentationLevel++;
            block.Condition.Accept(this, c);
            c.IndentationLevel--;
            c.NewLine().Indentation().Text(")");

            c.StatementBlock(block.Body, this);
        }

        public void Visit(ICompletionExpression entity, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(entity, out editOperationType);

            c.Text(wrapping);
            if (entity.VariableReference != null)
            {
                c.Text(entity.VariableReference.Identifier).Text(".");
            }
            else if (entity.TypeReference != null)
            {
                c.Type(entity.TypeReference).Text(".");
            }

            c.Text(entity.Token).CursorPosition();
            CloseWrapping(c, wrapping, true);
        }

        public void Visit(IComposedExpression expr, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(expr, out editOperationType);

            c.Text(wrapping).Keyword("composed").Text("(");

            CloseWrapping(c, wrapping, true);

            foreach (var reference in expr.References)
            {
                reference.Accept(this, c);

                AddCommaIfNotLast(reference, expr.References, c);
            }

            c.Text(")");
        }

        public void Visit(IIfElseExpression expr, XamlDiffPrintingContext c)
        {
            c.Text("(");
            expr.Condition.Accept(this, c);
            c.Text(")").Space().Text("?").Space();
            expr.ThenExpression.Accept(this, c);
            c.Space().Text(":").Space();
            expr.ElseExpression.Accept(this, c);
        }

        public void Visit(IInvocationExpression expr, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(expr, out editOperationType);

            c.Text(wrapping);
            if (expr.MethodName.IsConstructor)
            {
                c.Keyword("new").Space().Type(expr.MethodName.DeclaringType);
            }
            else
            {
                if (expr.MethodName.IsStatic)
                {
                    c.Text(expr.MethodName.DeclaringType.Name);
                }
                else
                {
                    expr.Reference.Accept(this, c);
                }

                c.Text(".").Text(expr.MethodName.Name);
            }

            if (expr.MethodName.HasTypeParameters && expr.MethodName.TypeParameters.All(type => type.TypeParameterType != null))
            {
                c.TypeParameters(expr.MethodName.TypeParameters);
            }

            c.Text("(");
            
            foreach (var parameter in expr.Parameters)
            {
                parameter.Accept(this, c);

                AddCommaIfNotLast(parameter, expr.Parameters, c);
            }

            c.Text(")");

            CloseWrapping(c, wrapping, true);
        }

        public void Visit(ILambdaExpression expr, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(expr, out editOperationType);

            c.Text(wrapping).ParameterList(expr.Name.Parameters).Space().Text("=>");
            CloseWrapping(c, wrapping, true);
            c.StatementBlock(expr.Body, this);
        }

        public void Visit(ILoopHeaderBlockExpression expr, XamlDiffPrintingContext c)
        {
            c.StatementBlock(expr.Body, this);
        }

        public void Visit(IConstantValueExpression expr, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(expr, out editOperationType);

            c.Text(wrapping);

            var value = expr.Value ?? "...";

            double parsed;
            if (double.TryParse(expr.Value, out parsed)
                || value.Equals("false", StringComparison.Ordinal)
                || value.Equals("true", StringComparison.Ordinal))
            {
                c.Keyword(value);
            }
            else
            {
                c.StringLiteral(value);
            }

            CloseWrapping(c, wrapping, true);
        }

        public void Visit(INullExpression expr, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(expr, out editOperationType);
            c.Text(wrapping).Keyword("null");
            CloseWrapping(c, wrapping, true);
        }

        public void Visit(IReferenceExpression expr, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(expr, out editOperationType);
            c.Text(wrapping);
            expr.Reference.Accept(this, c);
            CloseWrapping(c, wrapping, true);
        }

        public void Visit(ICastExpression expr, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(expr, out editOperationType);

            c.Text(wrapping).Text("(").Type(expr.TargetType).Text(")").Space();
            CloseWrapping(c, wrapping, true);
            expr.Reference.Accept(this, c);
        }

        public void Visit(IIndexAccessExpression expr, XamlDiffPrintingContext c)
        {
            expr.Reference.Accept(this, c);
            c.Text("[");

            foreach (var indexExpr in expr.Indices)
            {
                indexExpr.Accept(this, c);

                AddCommaIfNotLast(indexExpr, expr.Indices, c);
            }

            c.Text("]");
        }

        public void Visit(ITypeCheckExpression expr, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(expr, out editOperationType);

            expr.Reference.Accept(this, c);
            c.Text(wrapping);
            c.Space().Keyword("is").Space().Type(expr.Type);
            CloseWrapping(c, wrapping, true);
        }

        public void Visit(IUnaryExpression expr, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(expr, out editOperationType);

            c.Text(wrapping);
            c.Text(expr.Operator.ToString()).Space();
            CloseWrapping(c, wrapping, true);

            expr.Operand.Accept(this, c);
        }

        public void Visit(IBinaryExpression expr, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(expr, out editOperationType);

            expr.LeftOperand.Accept(this, c);

            c.Text(wrapping);
            c.Space().Text(expr.Operator.ToString()).Space();
            CloseWrapping(c, wrapping, true);

            expr.RightOperand.Accept(this, c);
        }


        public void Visit(IEventReference eventRef, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(eventRef, out editOperationType);
            c.Text(wrapping);
            if (eventRef.EventName.IsStatic)
            {
                c.Type(eventRef.EventName.DeclaringType);
            }
            else
            {
                c.Text(eventRef.Reference.Identifier);
            }
            c.Text(".").Text(eventRef.EventName.Name);
            CloseWrapping(c, wrapping, true);
        }

        public void Visit(IFieldReference fieldRef, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(fieldRef, out editOperationType);
            c.Text(wrapping);
            if (fieldRef.FieldName.IsStatic)
            {
                c.Type(fieldRef.FieldName.DeclaringType);
            }
            else
            {
                c.Text(fieldRef.Reference.Identifier);
            }
            c.Text(".").Text(fieldRef.FieldName.Name);
            CloseWrapping(c, wrapping, true);
        }

        public void Visit(IMethodReference methodRef, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(methodRef, out editOperationType);
            c.Text(wrapping);
            if (methodRef.MethodName.IsStatic)
            {
                c.Type(methodRef.MethodName.DeclaringType);
            }
            else
            {
                c.Text(methodRef.Reference.Identifier);
            }

            c.Text(".").Text(methodRef.MethodName.Name);
            CloseWrapping(c, wrapping, true);
        }

        public void Visit(IPropertyReference propertyRef, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(propertyRef, out editOperationType);
            c.Text(wrapping);
            if (propertyRef.PropertyName.IsStatic)
            {
                c.Type(propertyRef.PropertyName.DeclaringType);
            }
            else
            {
                c.Text(propertyRef.Reference.Identifier);
            }
            c.Text(".").Text(propertyRef.PropertyName.Name);
            CloseWrapping(c, wrapping, true);
        }

        public void Visit(ITypeReference typeReference, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(typeReference, out editOperationType);
            c.Text(wrapping).Type(typeReference.TypeName);
            CloseWrapping(c, wrapping, true);
        }

        public void Visit(IVariableReference varRef, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(varRef, out editOperationType);
            c.Text(wrapping).Text(varRef.Identifier);
            CloseWrapping(c, wrapping, true);
        }

        public void Visit(IIndexAccessReference indexAccessRef, XamlDiffPrintingContext context)
        {
            indexAccessRef.Expression.Accept(this, context);
        }

        public void Visit(ISimpleName simpleName, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(simpleName, out editOperationType);
            c.Text(wrapping).Text(simpleName.Name);
            CloseWrapping(c, wrapping, true);
        }

        public void Visit(IUnknownReference unknownRef, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(unknownRef, out editOperationType);
            c.Text(wrapping).UnknownMarker();
            CloseWrapping(c, wrapping, true);
        }

        public void Visit(IUnknownExpression unknownExpr, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(unknownExpr, out editOperationType);
            c.Text(wrapping).UnknownMarker();
            CloseWrapping(c, wrapping, true);
        }

        public void Visit(IUnknownStatement unknownStmt, XamlDiffPrintingContext c)
        {
            EditOperationEnum editOperationType;
            var wrapping = c.GetWrappingForNode(unknownStmt, out editOperationType);
            c.Indentation().Text(wrapping).UnknownMarker().Text(";");
            CloseWrapping(c, wrapping, true);
        }

        private void AppendMemberDeclarationGroup(XamlDiffPrintingContext c,
            IEnumerable<IMemberDeclaration> nodeGroup,
            int inBetweenNewLineCount = 1,
            int trailingNewLineCount = 2)
        {
            var sstNodes = nodeGroup.ToList();
            foreach (var node in sstNodes)
            {
                node.Accept(this, c);

                var newLinesNeeded = !ReferenceEquals(node, sstNodes.Last())
                    ? inBetweenNewLineCount
                    : trailingNewLineCount;

                for (var i = 0; i < newLinesNeeded; i++)
                {
                    c.NewLine();
                }
            }
        }

        private void AppendMethodDeclarationGroup(XamlDiffPrintingContext c,
            IEnumerable<IMemberDeclaration> nodeGroup,
            int inBetweenNewLineCount = 1,
            int trailingNewLineCount = 2)
        {
            var sstNodes = nodeGroup.ToList();
            var dotsTriggered = false;
            foreach (var node in sstNodes)
            {
                var matchPair = _changedMethodsDictionary.FirstOrDefault(pair => pair.Key.ContainsNode(node));
                if (matchPair.Key != null)
                {
                    if (matchPair.Value)
                    {
                        AppendMethodToContext(c, inBetweenNewLineCount, trailingNewLineCount, node, sstNodes);
                        dotsTriggered = false;
                    }
                    else if (!dotsTriggered)
                    {
                        AppendDots(c);
                        dotsTriggered = true;
                    }
                }
                else
                {
                    AppendMethodToContext(c, inBetweenNewLineCount, trailingNewLineCount, node, sstNodes);
                }
            }
        }

        private void AppendMethodToContext(XamlDiffPrintingContext c, int inBetweenNewLineCount, int trailingNewLineCount,
            IMemberDeclaration node, List<IMemberDeclaration> sstNodes)
        {
            node.Accept(this, c);

            var newLinesNeeded = !ReferenceEquals(node, sstNodes.Last())
                ? inBetweenNewLineCount
                : trailingNewLineCount;

            for (var i = 0; i < newLinesNeeded; i++)
            {
                c.NewLine();
            }
        }

        private void AppendPropertyAccessor(XamlDiffPrintingContext c, IKaVEList<IStatement> body, string keyword)
        {
            if (body.Any())
            {
                c.Indentation().Text(keyword);
                c.StatementBlock(body, this);
            }
            else
            {
                c.Indentation().Text(keyword).Text(";");
            }

            c.NewLine();
        }

        private static void AppendDots(XamlDiffPrintingContext c)
        {
            c.Indentation().Text("<Span FontSize=\"14\" Foreground=\"DarkSlateGray\">[...]</Span>").NewLine().NewLine();
        }


        private static void AddCommaIfNotLast<T>(T node, IKaVEList<T> list, XamlDiffPrintingContext c)
            where T : ISSTNode
        {
            if (!ReferenceEquals(node, list.Last()))
            {
                c.Text(", ");
            }
        }

        private static void CloseWrapping(XamlDiffPrintingContext c, string wrapping, bool popFromStack)
        {
            if (wrapping == string.Empty) return;
            if (popFromStack)
            {
                c.CloseSpanAndPopOperationFromStack();
            }
            else
            {
                c.CloseSpan();
            }
        }
    }
}
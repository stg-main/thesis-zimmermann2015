﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;
using KaVE.Commons.Model.SSTs;
using KaVE.Commons.Model.SSTs.Blocks;
using KaVE.Commons.Model.SSTs.Declarations;
using KaVE.Commons.Model.SSTs.Expressions.Assignable;
using KaVE.Commons.Model.SSTs.Expressions.LoopHeader;
using KaVE.Commons.Model.SSTs.Expressions.Simple;
using KaVE.Commons.Model.SSTs.References;
using KaVE.Commons.Model.SSTs.Statements;
using KaVE.Commons.Model.SSTs.Visitor;
using KaVE.Commons.Utils.Collections;

namespace SSTDiff.Util.Visitor.Traversal
{
    public class NodesListVisitor : ISSTNodeVisitor<List<ISSTNode>>
    {
        public void Visit(ISST sst, List<ISSTNode> context)
        {
            context.Add(sst);

            foreach (var f in sst.Fields) f.Accept(this, context);

            foreach (var p in sst.Properties) p.Accept(this, context);

            foreach (var m in sst.Methods) m.Accept(this, context);

            foreach (var e in sst.Events) e.Accept(this, context);

            foreach (var d in sst.Delegates) d.Accept(this, context);
        }

        public void Visit(IDelegateDeclaration decl, List<ISSTNode> context)
        {
            context.Add(decl);
        }

        public void Visit(IEventDeclaration decl, List<ISSTNode> context)
        {
            context.Add(decl);
        }

        public void Visit(IFieldDeclaration decl, List<ISSTNode> context)
        {
            context.Add(decl);
            decl.ValueType.Accept(this, context);
            decl.FieldName.Accept(this, context);
        }

        public void Visit(IMethodDeclaration decl, List<ISSTNode> context)
        {
            context.Add(decl);
            Visit(decl.MethodName, context);
            Visit(decl.ReturnType, context);

            foreach (var parameterDeclaration in decl.Parameters)
            {
                Visit(parameterDeclaration,context);
            }

            Visit(decl.Body, context);

        }

        public void Visit(IParameterDeclaration decl, List<ISSTNode> context)
        {
            context.Add(decl);
            Visit(decl.Name,context);
            Visit(decl.Type,context);
        }

        public void Visit(IPropertyDeclaration decl, List<ISSTNode> context)
        {
            context.Add(decl);
            Visit(decl.Get, context);
            Visit(decl.Set, context);
        }

        public void Visit(IVariableDeclaration decl, List<ISSTNode> context)
        {
            context.Add(decl);
            decl.Reference.Accept(this, context);
        }

        public void Visit(IAssignment stmt, List<ISSTNode> context)
        {
            context.Add(stmt);
            stmt.Expression.Accept(this, context);
            stmt.Reference.Accept(this, context);
        }

        public void Visit(IBreakStatement stmt, List<ISSTNode> context)
        {
            context.Add(stmt);
        }

        public void Visit(IContinueStatement stmt, List<ISSTNode> context)
        {
            context.Add(stmt);
        }

        public void Visit(IEventSubscriptionStatement stmt, List<ISSTNode> context)
        {
            context.Add(stmt);
            stmt.Expression.Accept(this, context);
            stmt.Reference.Accept(this, context);
        }

        public void Visit(IExpressionStatement stmt, List<ISSTNode> context)
        {
            context.Add(stmt);
            stmt.Expression.Accept(this, context);
        }

        public void Visit(IGotoStatement stmt, List<ISSTNode> context)
        {
            context.Add(stmt);
        }

        public void Visit(ILabelledStatement stmt, List<ISSTNode> context)
        {
            context.Add(stmt);
            stmt.Statement.Accept(this, context);
        }

        public void Visit(IReturnStatement stmt, List<ISSTNode> context)
        {
            context.Add(stmt);
            stmt.Expression.Accept(this, context);
        }

        public void Visit(IThrowStatement stmt, List<ISSTNode> context)
        {
            context.Add(stmt);
            stmt.Reference.Accept(this, context);
        }

        public void Visit(IDoLoop block, List<ISSTNode> context)
        {
            context.Add(block);
            block.Condition.Accept(this, context);
            Visit(block.Body, context);
        }

        public void Visit(IForEachLoop block, List<ISSTNode> context)
        {
            context.Add(block);
            block.Declaration.Accept(this, context);
            block.LoopedReference.Accept(this, context);
            Visit(block.Body, context);
        }

        public void Visit(IForLoop block, List<ISSTNode> context)
        {
            context.Add(block);
            block.Condition.Accept(this, context);
            Visit(block.Body, context);
            Visit(block.Init, context);
            Visit(block.Step, context);
        }

        public void Visit(IIfElseBlock block, List<ISSTNode> context)
        {
            context.Add(block);
            block.Condition.Accept(this, context);
            Visit(block.Then, context);
            Visit(block.Else, context);
        }

        public void Visit(ILockBlock block, List<ISSTNode> context)
        {
            context.Add(block);
            block.Reference.Accept(this, context);
            Visit(block.Body, context);
        }

        public void Visit(ISwitchBlock block, List<ISSTNode> context)
        {
            context.Add(block);
            block.Reference.Accept(this, context);
            Visit(block.DefaultSection, context);
            // No support for CaseBlocks
            //foreach (var caseBlock in block.Sections)
            //{
            //    caseBlock.Label.Accept(this, context);
            //    Visit(caseBlock.Body, context);
            //}
        }

        public void Visit(ITryBlock block, List<ISSTNode> context)
        {
            context.Add(block);
            Visit(block.Body, context);
            Visit(block.Finally, context);
            // No support for CatchBlocks
            //foreach (var catchBlock in block.CatchBlocks)
            //{
            //    Visit(catchBlock.Body, context);
            //}
        }

        public void Visit(IUncheckedBlock block, List<ISSTNode> context)
        {
            context.Add(block);
            Visit(block.Body, context);
        }

        public void Visit(IUnsafeBlock block, List<ISSTNode> context)
        {
            context.Add(block);
        }

        public void Visit(IUsingBlock block, List<ISSTNode> context)
        {
            context.Add(block);
            block.Reference.Accept(this, context);
            Visit(block.Body, context);
        }

        public void Visit(IWhileLoop block, List<ISSTNode> context)
        {
            context.Add(block);
            block.Condition.Accept(this, context);
            Visit(block.Body, context);
        }

        public void Visit(ICompletionExpression expr, List<ISSTNode> context)
        {
            context.Add(expr);
            if (expr.VariableReference != null) expr.VariableReference.Accept(this, context);
        }

        public void Visit(IComposedExpression expr, List<ISSTNode> context)
        {
            context.Add(expr);
            foreach (var vr in expr.References) vr.Accept(this, context);
        }

        public void Visit(IIfElseExpression expr, List<ISSTNode> context)
        {
            context.Add(expr);
            expr.Condition.Accept(this, context);
            expr.ThenExpression.Accept(this, context);
            expr.ElseExpression.Accept(this, context);
        }

        public void Visit(IInvocationExpression expr, List<ISSTNode> context)
        {
            context.Add(expr);
            foreach (var param in expr.Parameters) param.Accept(this, context);
            expr.Reference.Accept(this, context);
        }

        public void Visit(ILambdaExpression expr, List<ISSTNode> context)
        {
            context.Add(expr);
            Visit(expr.Body, context);
        }

        public void Visit(ILoopHeaderBlockExpression expr, List<ISSTNode> context)
        {
            context.Add(expr);
            Visit(expr.Body, context);
        }

        public void Visit(IConstantValueExpression expr, List<ISSTNode> context)
        {
            context.Add(expr);
        }

        public void Visit(INullExpression expr, List<ISSTNode> context)
        {
            context.Add(expr);
        }

        public void Visit(IReferenceExpression expr, List<ISSTNode> context)
        {
            context.Add(expr);
            expr.Reference.Accept(this, context);
        }

        public void Visit(ICastExpression expr, List<ISSTNode> context)
        {
            context.Add(expr);
            expr.Reference.Accept(this, context);
        }

        public void Visit(IIndexAccessExpression expr, List<ISSTNode> context)
        {
            context.Add(expr);
            expr.Reference.Accept(this, context);
            foreach (var simpleExpression in expr.Indices) simpleExpression.Accept(this, context);
        }

        public void Visit(ITypeCheckExpression expr, List<ISSTNode> context)
        {
            context.Add(expr);
            expr.Reference.Accept(this, context);
        }

        public void Visit(IUnaryExpression expr, List<ISSTNode> context)
        {
            context.Add(expr);
            expr.Operand.Accept(this, context);
        }

        public void Visit(IBinaryExpression expr, List<ISSTNode> context)
        {
            context.Add(expr);
            expr.LeftOperand.Accept(this, context);
            expr.RightOperand.Accept(this, context);
        }

        public void Visit(IEventReference eventRef, List<ISSTNode> context)
        {
            context.Add(eventRef);
            eventRef.Reference.Accept(this, context);
        }

        public void Visit(IFieldReference fieldRef, List<ISSTNode> context)
        {
            context.Add(fieldRef);
            fieldRef.Reference.Accept(this, context);
        }

        public void Visit(IMethodReference methodRef, List<ISSTNode> context)
        {
            context.Add(methodRef);
            methodRef.Reference.Accept(this, context);
        }

        public void Visit(IPropertyReference propertyRef, List<ISSTNode> context)
        {
            context.Add(propertyRef);
            propertyRef.Reference.Accept(this, context);
        }

        public void Visit(ITypeReference typeReference, List<ISSTNode> context)
        {
            context.Add(typeReference);
        }

        public void Visit(IVariableReference varRef, List<ISSTNode> context)
        {
            context.Add(varRef);
        }

        public void Visit(IIndexAccessReference indexAccessRef, List<ISSTNode> context)
        {
            context.Add(indexAccessRef);
            indexAccessRef.Expression.Accept(this, context);
        }
        
        public void Visit(ISimpleName simpleName, List<ISSTNode> context)
        {
            context.Add(simpleName);
        }

        public void Visit(IUnknownReference unknownRef, List<ISSTNode> context)
        {
            context.Add(unknownRef);
        }

        public void Visit(IUnknownExpression unknownExpr, List<ISSTNode> context)
        {
            context.Add(unknownExpr);
        }

        public void Visit(IUnknownStatement unknownStmt, List<ISSTNode> context)
        {
            context.Add(unknownStmt);
        }

        private void Visit(IKaVEList<IStatement> stmts, List<ISSTNode> context)
        {
            foreach (var sstNode in stmts) sstNode.Accept(this, context);
        }
    }
}
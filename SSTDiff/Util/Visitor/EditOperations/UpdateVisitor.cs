﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using KaVE.Commons.Model.SSTs;
using KaVE.Commons.Model.SSTs.Blocks;
using KaVE.Commons.Model.SSTs.Declarations;
using KaVE.Commons.Model.SSTs.Expressions.Assignable;
using KaVE.Commons.Model.SSTs.Expressions.LoopHeader;
using KaVE.Commons.Model.SSTs.Expressions.Simple;
using KaVE.Commons.Model.SSTs.Impl;
using KaVE.Commons.Model.SSTs.Impl.Declarations;
using KaVE.Commons.Model.SSTs.Impl.Expressions.Assignable;
using KaVE.Commons.Model.SSTs.Impl.Expressions.Simple;
using KaVE.Commons.Model.SSTs.Impl.References;
using KaVE.Commons.Model.SSTs.Impl.Statements;
using KaVE.Commons.Model.SSTs.References;
using KaVE.Commons.Model.SSTs.Statements;
using KaVE.Commons.Model.SSTs.Visitor;

namespace SSTDiff.Util.Visitor.EditOperations
{
    public class UpdateVisitor : ISSTNodeVisitor<ISSTNode>
    {
        public void Visit(ISST sst, ISSTNode context)
        {
            var newNode = context as ISST;
            var sstNode = (SST)sst;
            if (newNode != null) sstNode.EnclosingType = newNode.EnclosingType;
        }

        public void Visit(IDelegateDeclaration decl, ISSTNode context)
        {
            var newNode = context as IDelegateDeclaration;
            var delegateDeclaration = (DelegateDeclaration) decl;
            if (newNode != null) delegateDeclaration.Name = newNode.Name;
        }

        public void Visit(IEventDeclaration decl, ISSTNode context)
        {
            var newNode = context as IEventDeclaration;
            var eventDeclaration = (EventDeclaration) decl;
            if (newNode != null) eventDeclaration.Name = newNode.Name;
        }

        public void Visit(IFieldDeclaration decl, ISSTNode context)
        {
            var newNode = context as IFieldDeclaration;
            var fieldDeclaration = (FieldDeclaration)decl;
            if (newNode != null) fieldDeclaration.IsStatic = newNode.IsStatic;
        }

        public void Visit(IMethodDeclaration decl, ISSTNode context)
        {
            var newNode = context as IMethodDeclaration;
            var methodDeclaration = (MethodDeclaration)decl;
            if (newNode != null) methodDeclaration.IsStatic = newNode.IsStatic;
        }

        public void Visit(IParameterDeclaration decl, ISSTNode context)
        {
            var newNode = context as IParameterDeclaration;
            var parameterDecl = (ParameterDeclaration) decl;
            if (newNode == null) return;
            parameterDecl.IsOptional = newNode.IsOptional;
            parameterDecl.IsOutput = newNode.IsOutput;
            parameterDecl.IsParameterArray = newNode.IsParameterArray;
            parameterDecl.IsPassedByReference = newNode.IsPassedByReference;
        }

        public void Visit(IPropertyDeclaration decl, ISSTNode context)
        {
            var newNode = context as IPropertyDeclaration;
            var propertyDeclaration = (PropertyDeclaration)decl;
            if (newNode != null) propertyDeclaration.Name = newNode.Name;
        }

        public void Visit(IVariableDeclaration decl, ISSTNode context)
        {
            var newNode = context as IVariableDeclaration;
            var variableDeclaration = (VariableDeclaration)decl;
            if (newNode != null) variableDeclaration.Type = newNode.Type;
        }

        public void Visit(IAssignment stmt, ISSTNode context)
        {
        }

        public void Visit(IBreakStatement stmt, ISSTNode context)
        {
        }

        public void Visit(IContinueStatement stmt, ISSTNode context)
        {
        }

        public void Visit(IEventSubscriptionStatement stmt, ISSTNode context)
        {
            var newNode = context as IEventSubscriptionStatement;
            var eventSubscriptionStatement = (EventSubscriptionStatement)stmt;
            if (newNode != null) eventSubscriptionStatement.Operation = newNode.Operation;
        }

        public void Visit(IExpressionStatement stmt, ISSTNode context)
        {
        }

        public void Visit(IGotoStatement stmt, ISSTNode context)
        {
            var newNode = context as IGotoStatement;
            var gotoStatement = (GotoStatement)stmt;
            if (newNode != null) gotoStatement.Label = newNode.Label;
        }

        public void Visit(ILabelledStatement stmt, ISSTNode context)
        {
            var newNode = context as ILabelledStatement;
            var labelledStatement = (LabelledStatement)stmt;
            if (newNode != null) labelledStatement.Label = newNode.Label;
        }

        public void Visit(IReturnStatement stmt, ISSTNode context)
        {
        }

        public void Visit(IThrowStatement stmt, ISSTNode context)
        {
        }

        public void Visit(IDoLoop block, ISSTNode context)
        {
        }

        public void Visit(IForEachLoop block, ISSTNode context)
        {
        }

        public void Visit(IForLoop block, ISSTNode context)
        {
        }

        public void Visit(IIfElseBlock block, ISSTNode context)
        {
        }

        public void Visit(ILockBlock block, ISSTNode context)
        {
        }

        public void Visit(ISwitchBlock block, ISSTNode context)
        {
        }

        public void Visit(ITryBlock block, ISSTNode context)
        {
        }

        public void Visit(IUncheckedBlock block, ISSTNode context)
        {
        }

        public void Visit(IUnsafeBlock block, ISSTNode context)
        {
        }

        public void Visit(IUsingBlock block, ISSTNode context)
        {
        }

        public void Visit(IWhileLoop block, ISSTNode context)
        {
        }

        public void Visit(ICompletionExpression expr, ISSTNode context)
        {
            var newNode = context as ICompletionExpression;
            var completionExpression = (CompletionExpression)expr;
            if (newNode != null)
            {
                completionExpression.TypeReference = newNode.TypeReference;
                completionExpression.Token = newNode.Token;
            }
        }

        public void Visit(IComposedExpression expr, ISSTNode context)
        {
        }

        public void Visit(IIfElseExpression expr, ISSTNode context)
        {
        }

        public void Visit(IInvocationExpression expr, ISSTNode context)
        {
            var newNode = context as IInvocationExpression;
            var expression = (InvocationExpression)expr;
            if (newNode != null) expression.MethodName = newNode.MethodName;
        }

        public void Visit(ILambdaExpression expr, ISSTNode context)
        {
            var newNode = context as ILambdaExpression;
            var expression = (LambdaExpression)expr;
            if (newNode != null) expression.Name = newNode.Name;
        }

        public void Visit(ILoopHeaderBlockExpression expr, ISSTNode context)
        {
        }

        public void Visit(IConstantValueExpression expr, ISSTNode context)
        {
            var newNode = context as IConstantValueExpression;
            var expression = (ConstantValueExpression)expr;
            if (newNode != null) expression.Value = newNode.Value;
        }

        public void Visit(INullExpression expr, ISSTNode context)
        {
        }

        public void Visit(IReferenceExpression expr, ISSTNode context)
        {
        }

        public void Visit(ICastExpression expr, ISSTNode context)
        {
            var newNode = context as ICastExpression;
            var expression = (CastExpression)expr;
            if (newNode != null) expression.TargetType = newNode.TargetType;
        }

        public void Visit(IIndexAccessExpression expr, ISSTNode context)
        {
        }

        public void Visit(ITypeCheckExpression expr, ISSTNode context)
        {
            var newNode = context as ITypeCheckExpression;
            var expression = (TypeCheckExpression)expr;
            if (newNode != null) expression.Type = newNode.Type;
        }

        public void Visit(IUnaryExpression expr, ISSTNode context)
        {
            var newNode = context as IUnaryExpression;
            var unaryExpression = (UnaryExpression)expr;
            if (newNode != null) unaryExpression.Operator = newNode.Operator;
        }

        public void Visit(IBinaryExpression expr, ISSTNode context)
        {
            var newNode = context as IBinaryExpression;
            var binaryExpression = (BinaryExpression)expr;
            if (newNode != null) binaryExpression.Operator = newNode.Operator;
        }

        public void Visit(IEventReference eventRef, ISSTNode context)
        {
            var newNode = context as IEventReference;
            var reference = (EventReference)eventRef;
            if (newNode != null) reference.EventName = newNode.EventName;
        }

        public void Visit(IFieldReference fieldRef, ISSTNode context)
        {
            var newNode = context as IFieldReference;
            var reference = (FieldReference)fieldRef;
            if (newNode != null) reference.FieldName = newNode.FieldName;
        }

        public void Visit(IMethodReference methodRef, ISSTNode context)
        {
            var newNode = context as IMethodReference;
            var reference = (MethodReference)methodRef;
            if (newNode != null) reference.MethodName = newNode.MethodName;
        }

        public void Visit(IPropertyReference propertyRef, ISSTNode context)
        {
            var newNode = context as IPropertyReference;
            var reference = (PropertyReference)propertyRef;
            if (newNode != null) reference.PropertyName = newNode.PropertyName;
        }

        public void Visit(ITypeReference typeReference, ISSTNode context)
        {
            var newNode = context as ITypeReference;
            var reference = (TypeReference)typeReference;
            if (newNode != null) reference.TypeName= newNode.TypeName;
        }

        public void Visit(IVariableReference varRef, ISSTNode context)
        {
            var newNode = context as IVariableReference;
            var reference = (VariableReference)varRef;
            if (newNode != null) reference.Identifier = newNode.Identifier;
        }

        
        public void Visit(IIndexAccessReference indexAccessRef, ISSTNode context)
        {

        }
        
        public void Visit(ISimpleName simpleName, ISSTNode context)
        {
            var newNode = context as ISimpleName;
            var name = (SimpleName)simpleName;
            if (newNode != null) name.Name = newNode.Name;
        }

        public void Visit(IUnknownReference unknownRef, ISSTNode context)
        {
        }

        public void Visit(IUnknownExpression unknownExpr, ISSTNode context)
        {
        }

        public void Visit(IUnknownStatement unknownStmt, ISSTNode context)
        {
        }
    }
}
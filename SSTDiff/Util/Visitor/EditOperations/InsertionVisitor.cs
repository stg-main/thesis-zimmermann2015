﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Linq;
using KaVE.Commons.Model.SSTs;
using KaVE.Commons.Model.SSTs.Blocks;
using KaVE.Commons.Model.SSTs.Declarations;
using KaVE.Commons.Model.SSTs.Expressions;
using KaVE.Commons.Model.SSTs.Expressions.Assignable;
using KaVE.Commons.Model.SSTs.Expressions.LoopHeader;
using KaVE.Commons.Model.SSTs.Expressions.Simple;
using KaVE.Commons.Model.SSTs.Impl;
using KaVE.Commons.Model.SSTs.Impl.Blocks;
using KaVE.Commons.Model.SSTs.Impl.Declarations;
using KaVE.Commons.Model.SSTs.Impl.Expressions.Assignable;
using KaVE.Commons.Model.SSTs.Impl.Expressions.Simple;
using KaVE.Commons.Model.SSTs.Impl.References;
using KaVE.Commons.Model.SSTs.Impl.Statements;
using KaVE.Commons.Model.SSTs.References;
using KaVE.Commons.Model.SSTs.Statements;
using KaVE.Commons.Model.SSTs.Visitor;
using KaVE.Commons.Utils.Collections;
using SSTDiff.EditScript;
using SSTDiff.EditScript.EditOperations;

namespace SSTDiff.Util.Visitor.EditOperations
{
    public class InsertionVisitor : ISSTNodeVisitor<Insert>
    {
        private readonly EditScriptAlgorithm _editScriptAlgorithm;

        public InsertionVisitor(EditScriptAlgorithm editScriptAlgorithm)
        {
            _editScriptAlgorithm = editScriptAlgorithm;
        }

        public void Visit(ISST sst, Insert insert)
        {
            var sstImpl = (SST) sst;
            var delegateDecl = insert.InsertedNode as IDelegateDeclaration;
            if (delegateDecl != null)
            {
                var delegateList = Lists.NewListFrom(sst.Delegates);
                delegateList.SafeInsert(insert.ChildPosition, delegateDecl);
                sstImpl.Delegates = Sets.NewHashSetFrom(delegateList);
                return;
            }

            var eventDecl = insert.InsertedNode as IEventDeclaration;
            if (eventDecl != null)
            {
                var eventList = Lists.NewListFrom(sst.Events);
                eventList.SafeInsert(insert.ChildPosition, eventDecl);
                sstImpl.Events = Sets.NewHashSetFrom(eventList);
                return;
            }

            var fieldDecl = insert.InsertedNode as IFieldDeclaration;
            if (fieldDecl != null)
            {
                var fieldList = Lists.NewListFrom(sst.Fields);
                fieldList.SafeInsert(insert.ChildPosition, fieldDecl);
                sstImpl.Fields = Sets.NewHashSetFrom(fieldList);
                return;
            }

            var propertyDecl = insert.InsertedNode as IPropertyDeclaration;
            if (propertyDecl != null)
            {
                var propertyList = Lists.NewListFrom(sst.Properties);
                propertyList.SafeInsert(insert.ChildPosition, propertyDecl);
                sstImpl.Properties = Sets.NewHashSetFrom(propertyList);
                return;
            }

            var methodDecl = insert.InsertedNode as IMethodDeclaration;
            if (methodDecl != null)
            {
                var methodList = Lists.NewListFrom(sst.Methods);
                methodList.SafeInsert(insert.ChildPosition, methodDecl);
                sstImpl.Methods = Sets.NewHashSetFrom(methodList);
            }
        }

        public void Visit(IDelegateDeclaration decl, Insert insert)
        {
        }

        public void Visit(IEventDeclaration decl, Insert insert)
        {
        }

        public void Visit(IFieldDeclaration decl, Insert insert)
        {
            var simpleName = insert.InsertedNode as ISimpleName;
            if (simpleName != null)
            {
                ((FieldDeclaration) decl).FieldName = simpleName;
            }
            var typeReference = insert.InsertedNode as ITypeReference;
            if (typeReference != null)
            {
                ((FieldDeclaration) decl).ValueType = typeReference;
            }
        }

        public void Visit(IMethodDeclaration decl, Insert insert)
        {
            var statement = insert.InsertedNode as IStatement;
            if (statement != null) decl.Body.SafeInsert(insert.ChildPosition - 2 - decl.Parameters.Count(), statement);

            var simpleName = insert.InsertedNode as ISimpleName;
            if (simpleName != null)
            {
                ((MethodDeclaration) decl).MethodName = simpleName;
            }
            var typeReference = insert.InsertedNode as ITypeReference;
            if (typeReference != null)
            {
                ((MethodDeclaration) decl).ReturnType = typeReference;
            }

            var parametersDecl = insert.InsertedNode as IParameterDeclaration;
            if (parametersDecl != null)
            {
                var kaVeList = Lists.NewListFrom(decl.Parameters);
                kaVeList.SafeInsert(insert.ChildPosition - 2, parametersDecl);
                ((MethodDeclaration) decl).Parameters = kaVeList;
            }
        }

        public void Visit(IParameterDeclaration decl, Insert insert)
        {
            var simpleName = insert.InsertedNode as ISimpleName;
            if (simpleName != null)
            {
                ((ParameterDeclaration) decl).Name = simpleName;
            }
            var typeReference = insert.InsertedNode as ITypeReference;
            if (typeReference != null)
            {
                ((ParameterDeclaration) decl).Type = typeReference;
            }
        }

        public void Visit(IPropertyDeclaration decl, Insert insert)
        {
            PropertyDeclaration oldPropertyDelcaration = null;
            ISSTNode partnerNode = null;
            GetOldBlock(insert, ref partnerNode, ref oldPropertyDelcaration);

            var statement = insert.InsertedNode as IStatement;
            if (statement != null)
            {
                var position = insert.ChildPosition;
                if (decl.Get.Count < position && oldPropertyDelcaration != null &&
                    oldPropertyDelcaration.Get.Contains(partnerNode))
                {
                    decl.Set.SafeInsert(position - decl.Get.Count - 1, statement);
                }
                else
                {
                    decl.Get.SafeInsert(position, statement);
                }
            }
        }

        public void Visit(IVariableDeclaration decl, Insert insert)
        {
            var reference = insert.InsertedNode as IVariableReference;
            if (reference != null)
            {
                ((VariableDeclaration) decl).Reference = reference;
            }
        }

        public void Visit(IAssignment stmt, Insert insert)
        {
            var reference = insert.InsertedNode as IAssignableReference;
            if (reference != null)
            {
                ((Assignment) stmt).Reference = reference;
            }
            var expression = insert.InsertedNode as IAssignableExpression;
            if (expression != null)
            {
                ((Assignment) stmt).Expression = expression;
            }
        }

        public void Visit(IBreakStatement stmt, Insert insert)
        {
        }

        public void Visit(IContinueStatement stmt, Insert insert)
        {
        }

        public void Visit(IEventSubscriptionStatement stmt, Insert insert)
        {
            var reference = insert.InsertedNode as IAssignableReference;
            if (reference != null)
            {
                ((EventSubscriptionStatement) stmt).Reference = reference;
            }
            var expression = insert.InsertedNode as IAssignableExpression;
            if (expression != null)
            {
                ((EventSubscriptionStatement) stmt).Expression = expression;
            }
        }

        public void Visit(IExpressionStatement stmt, Insert insert)
        {
            var expression = insert.InsertedNode as IAssignableExpression;
            if (expression != null)
            {
                ((ExpressionStatement) stmt).Expression = expression;
            }
        }

        public void Visit(IGotoStatement stmt, Insert insert)
        {
        }

        public void Visit(ILabelledStatement stmt, Insert insert)
        {
            var statement = insert.InsertedNode as IStatement;
            if (statement != null)
            {
                ((LabelledStatement) stmt).Statement = statement;
            }
        }

        public void Visit(IReturnStatement stmt, Insert insert)
        {
            var expression = insert.InsertedNode as ISimpleExpression;
            if (expression != null)
            {
                ((ReturnStatement) stmt).Expression = expression;
            }
        }

        public void Visit(IThrowStatement stmt, Insert insert)
        {
            var reference = insert.InsertedNode as IVariableReference;
            if (reference != null)
            {
                ((ThrowStatement) stmt).Reference = reference;
            }
        }

        public void Visit(IDoLoop block, Insert insert)
        {
            var statement = insert.InsertedNode as IStatement;
            if (statement != null) block.Body.SafeInsert(insert.ChildPosition - 1, statement);
            var simpleExpr = insert.InsertedNode as ILoopHeaderExpression;
            if (simpleExpr != null)
            {
                ((DoLoop) block).Condition = simpleExpr;
            }
        }

        public void Visit(IForEachLoop block, Insert insert)
        {
            var statement = insert.InsertedNode as IStatement;
            if (statement != null && insert.ChildPosition - 2 >= 0)
            {
                block.Body.SafeInsert(insert.ChildPosition - 2, statement);
                return;
            }

            var variableReference = insert.InsertedNode as IVariableReference;
            if (variableReference != null)
            {
                ((ForEachLoop) block).LoopedReference = variableReference;
            }

            var variableDeclaration = insert.InsertedNode as VariableDeclaration;
            if (variableDeclaration != null)
            {
                ((ForEachLoop) block).Declaration = variableDeclaration;
            }
        }

        public void Visit(IForLoop block, Insert insert)
        {
            ForLoop oldForLoop = null;
            ISSTNode partnerNode = null;
            GetOldBlock(insert, ref partnerNode, ref oldForLoop);


            var statement = insert.InsertedNode as IStatement;
            if (statement != null)
            {
                var position = insert.ChildPosition;
                if (position < block.Init.Count && oldForLoop != null &&
                    oldForLoop.Init.Contains(partnerNode))
                {
                    block.Init.SafeInsert(position, statement);
                }
                else if (position < (block.Init.Count + 1 + block.Body.Count) && oldForLoop != null &&
                         oldForLoop.Body.Contains(partnerNode))
                {
                    block.Step.SafeInsert(position - 1 - block.Init.Count, statement);
                }
                else
                {
                    block.Body.SafeInsert(position - block.Init.Count - 1 - block.Step.Count, statement);
                }
            }

            var simpleExpr = insert.InsertedNode as ILoopHeaderExpression;
            if (simpleExpr != null)
            {
                ((ForLoop) block).Condition = simpleExpr;
            }
        }

        public void Visit(IIfElseBlock block, Insert insert)
        {
            IfElseBlock oldIfElseBlock = null;
            ISSTNode partnerNode = null;
            GetOldBlock(insert, ref partnerNode, ref oldIfElseBlock);

            var statement = insert.InsertedNode as IStatement;
            if (statement != null)
            {
                var position = insert.ChildPosition;
                if (position <= block.Then.Count + 1 && oldIfElseBlock != null &&
                    oldIfElseBlock.Then.Contains(partnerNode))
                {
                    block.Then.SafeInsert(position - 1, statement);
                }
                else
                {
                    block.Else.SafeInsert(position - 1 - block.Then.Count, statement);
                }
            }

            var simpleExpr = insert.InsertedNode as ISimpleExpression;
            if (simpleExpr != null)
            {
                ((IfElseBlock) block).Condition = simpleExpr;
            }
        }

        public void Visit(ILockBlock block, Insert insert)
        {
            var statement = insert.InsertedNode as IStatement;
            if (statement != null) block.Body.SafeInsert(insert.ChildPosition - 1, statement);

            var variableReference = insert.InsertedNode as IVariableReference;
            if (variableReference != null)
            {
                ((LockBlock) block).Reference = variableReference;
            }
        }

        public void Visit(ISwitchBlock block, Insert insert)
        {
            var statement = insert.InsertedNode as IStatement;
            if (statement != null) block.DefaultSection.SafeInsert(insert.ChildPosition - 1, statement);

            var variableReference = insert.InsertedNode as IVariableReference;
            if (variableReference != null)
            {
                ((SwitchBlock) block).Reference = variableReference;
            }
        }

        public void Visit(ITryBlock block, Insert insert)
        {
            TryBlock oldTryBlock = null;
            ISSTNode partnerNode = null;
            GetOldBlock(insert, ref partnerNode, ref oldTryBlock);


            var statement = insert.InsertedNode as IStatement;
            if (statement != null)
            {
                var position = insert.ChildPosition;
                if (position <= block.Body.Count && oldTryBlock != null &&
                    oldTryBlock.Body.Contains(partnerNode))
                {
                    block.Body.SafeInsert(position, statement);
                }
                else
                {
                    block.Finally.SafeInsert(position - block.Body.Count, statement);
                }
            }
        }

        public void Visit(IUncheckedBlock block, Insert insert)
        {
            var statement = insert.InsertedNode as IStatement;
            if (statement != null) block.Body.SafeInsert(insert.ChildPosition, statement);
        }

        public void Visit(IUnsafeBlock block, Insert insert)
        {
        }

        public void Visit(IUsingBlock block, Insert insert)
        {
            var statement = insert.InsertedNode as IStatement;
            if (statement != null) block.Body.SafeInsert(insert.ChildPosition, statement);

            var variableReference = insert.InsertedNode as IVariableReference;
            if (variableReference != null)
            {
                ((UsingBlock) block).Reference = variableReference;
            }
        }

        public void Visit(IWhileLoop block, Insert insert)
        {
            var statement = insert.InsertedNode as IStatement;
            if (statement != null) block.Body.SafeInsert(insert.ChildPosition - 1, statement);

            var simpleExpr = insert.InsertedNode as ILoopHeaderExpression;
            if (simpleExpr != null)
            {
                ((WhileLoop) block).Condition = simpleExpr;
            }
        }

        public void Visit(ICompletionExpression expr, Insert insert)
        {
            var variableReference = insert.InsertedNode as IVariableReference;
            if (variableReference != null)
            {
                ((CompletionExpression) expr).VariableReference = variableReference;
            }
        }

        public void Visit(IComposedExpression expr, Insert insert)
        {
            var varRef = insert.InsertedNode as IVariableReference;
            if (varRef != null) expr.References.SafeInsert(insert.ChildPosition, varRef);
        }

        public void Visit(IIfElseExpression expr, Insert insert)
        {
            var position = insert.ChildPosition;

            switch (position)
            {
                case 0:
                    var condition = insert.InsertedNode as ISimpleExpression;
                    if (condition != null)
                    {
                        ((IfElseExpression) expr).Condition = condition;
                    }
                    break;
                case 1:
                    var thenExpression = insert.InsertedNode as ISimpleExpression;
                    if (thenExpression != null)
                    {
                        ((IfElseExpression) expr).ThenExpression = thenExpression;
                    }
                    break;
                default:
                    var elseExpression = insert.InsertedNode as ISimpleExpression;
                    if (elseExpression != null)
                    {
                        ((IfElseExpression) expr).ElseExpression = elseExpression;
                    }
                    break;
            }
        }

        public void Visit(IInvocationExpression expr, Insert insert)
        {
            var simpleExpr = insert.InsertedNode as ISimpleExpression;
            if (simpleExpr != null) expr.Parameters.SafeInsert(insert.ChildPosition - 1, simpleExpr);

            var variableReference = insert.InsertedNode as IVariableReference;
            if (variableReference != null)
            {
                ((InvocationExpression) expr).Reference = variableReference;
            }
        }

        public void Visit(ILambdaExpression expr, Insert insert)
        {
            var statement = insert.InsertedNode as IStatement;
            if (statement != null) expr.Body.SafeInsert(insert.ChildPosition, statement);
        }

        public void Visit(ILoopHeaderBlockExpression expr, Insert insert)
        {
            var statement = insert.InsertedNode as IStatement;
            if (statement != null) expr.Body.SafeInsert(insert.ChildPosition, statement);
        }

        public void Visit(IConstantValueExpression expr, Insert insert)
        {
        }

        public void Visit(INullExpression expr, Insert insert)
        {
        }

        public void Visit(IReferenceExpression expr, Insert insert)
        {
            var reference = insert.InsertedNode as IReference;
            if (reference != null)
            {
                ((ReferenceExpression) expr).Reference = reference;
            }
        }

        public void Visit(ICastExpression expr, Insert insert)
        {
            var reference = insert.InsertedNode as IVariableReference;
            if (reference != null)
            {
                ((CastExpression) expr).Reference = reference;
            }
        }

        public void Visit(IIndexAccessExpression expr, Insert insert)
        {
            var reference = insert.InsertedNode as IVariableReference;
            if (reference != null)
            {
                ((IndexAccessExpression) expr).Reference = reference;
            }

            var simpleExpression = insert.InsertedNode as ISimpleExpression;
            if (simpleExpression != null)
            {
                expr.Indices.SafeInsert(insert.ChildPosition - 1, simpleExpression);
            }
        }

        public void Visit(ITypeCheckExpression expr, Insert insert)
        {
            var reference = insert.InsertedNode as IVariableReference;
            if (reference != null)
            {
                ((TypeCheckExpression) expr).Reference = reference;
            }
        }

        public void Visit(IUnaryExpression expr, Insert insert)
        {
            var operand = insert.InsertedNode as ISimpleExpression;
            if (operand != null)
            {
                ((UnaryExpression) expr).Operand = operand;
            }
        }

        public void Visit(IBinaryExpression expr, Insert insert)
        {
            var position = insert.ChildPosition;

            switch (position)
            {
                case 0:
                    var leftOperand = insert.InsertedNode as ISimpleExpression;
                    if (leftOperand != null)
                    {
                        ((BinaryExpression) expr).LeftOperand = leftOperand;
                    }
                    break;
                case 1:
                    var rightOperand = insert.InsertedNode as ISimpleExpression;
                    if (rightOperand != null)
                    {
                        ((BinaryExpression) expr).RightOperand = rightOperand;
                    }
                    break;
            }
        }

        public void Visit(IEventReference eventRef, Insert insert)
        {
            var variableReference = insert.InsertedNode as IVariableReference;
            if (variableReference != null)
            {
                ((EventReference) eventRef).Reference = variableReference;
            }
        }

        public void Visit(IFieldReference fieldRef, Insert insert)
        {
            var variableReference = insert.InsertedNode as IVariableReference;
            if (variableReference != null)
            {
                ((FieldReference) fieldRef).Reference = variableReference;
            }
        }

        public void Visit(IMethodReference methodRef, Insert insert)
        {
            var variableReference = insert.InsertedNode as IVariableReference;
            if (variableReference != null)
            {
                ((MethodReference) methodRef).Reference = variableReference;
            }
        }

        public void Visit(IPropertyReference propertyRef, Insert insert)
        {
            var variableReference = insert.InsertedNode as IVariableReference;
            if (variableReference != null)
            {
                ((PropertyReference) propertyRef).Reference = variableReference;
            }
        }

        public void Visit(ITypeReference typeReference, Insert context)
        {
        }

        public void Visit(IVariableReference varRef, Insert insert)
        {
        }

        public void Visit(ISimpleName simpleName, Insert context)
        {
        }

        public void Visit(IIndexAccessReference indexAccessRef, Insert insert)
        {
            var indexAccessExpression = insert.InsertedNode as IIndexAccessExpression;
            if (indexAccessExpression != null)
            {
                ((IndexAccessReference) indexAccessRef).Expression = indexAccessExpression;
            }
        }

        public void Visit(IUnknownReference unknownRef, Insert insert)
        {
        }

        public void Visit(IUnknownExpression unknownExpr, Insert insert)
        {
        }

        public void Visit(IUnknownStatement unknownStmt, Insert insert)
        {
        }

        private void GetOldBlock<T>(Insert insert, ref ISSTNode node, ref T oldBlock) where T : class
        {
            if (_editScriptAlgorithm.LeftToRightMatch.ContainsKey(insert.InsertedNode))
            {
                node = _editScriptAlgorithm.LeftToRightMatch[insert.InsertedNode];
                var parentNodeInTree2 = node.GetParent(_editScriptAlgorithm.RootT2, true,
                    SSTNodeUtil.TreeSelection.Tree2);
                oldBlock = parentNodeInTree2 as T;
            }
            else
            {
                node = insert.InsertedNode;
                var parentNodeInTree1 = insert.InsertedNode.GetParent(_editScriptAlgorithm.RootT1, true,
                    SSTNodeUtil.TreeSelection.Tree1);
                oldBlock = parentNodeInTree1 as T;
            }
            if (oldBlock == null)
            {
                var parentNode = insert.ParentNode;
                if (_editScriptAlgorithm.MoveClonesFromTree1.ContainsKey(parentNode))
                {
                    var cachedParentNode = _editScriptAlgorithm.MoveClonesFromTree1[parentNode];
                    node = _editScriptAlgorithm.MoveClonesFromTree1.ContainsKey(insert.InsertedNode)
                        ? _editScriptAlgorithm.MoveClonesFromTree1[insert.InsertedNode]
                        : insert.InsertedNode;
                    oldBlock = cachedParentNode as T;
                }
            }
        }
    }
}
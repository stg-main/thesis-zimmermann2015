﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using KaVE.Commons.Model.SSTs;
using KaVE.Commons.Model.SSTs.Blocks;
using KaVE.Commons.Model.SSTs.Declarations;
using KaVE.Commons.Model.SSTs.Expressions;
using KaVE.Commons.Model.SSTs.Expressions.Assignable;
using KaVE.Commons.Model.SSTs.Expressions.LoopHeader;
using KaVE.Commons.Model.SSTs.Expressions.Simple;
using KaVE.Commons.Model.SSTs.Impl;
using KaVE.Commons.Model.SSTs.Impl.Declarations;
using KaVE.Commons.Model.SSTs.Impl.References;
using KaVE.Commons.Model.SSTs.References;
using KaVE.Commons.Model.SSTs.Statements;
using KaVE.Commons.Model.SSTs.Visitor;

namespace SSTDiff.Util.Visitor.EditOperations
{
    public class DeletionVisitor : ISSTNodeVisitor<ISSTNode>
    {
        public void Visit(ISST sst, ISSTNode context)
        {
            var delegateDecl = context as IDelegateDeclaration;
            if (delegateDecl != null)
            {
                sst.Delegates.Remove(delegateDecl);
                return;
            }

            var eventDecl = context as IEventDeclaration;
            if (eventDecl != null)
            {
                sst.Events.Remove(eventDecl);
                return;
            }

            var fieldDecl = context as IFieldDeclaration;
            if (fieldDecl != null)
            {
                sst.Fields.Remove(fieldDecl);
                return;
            }


            var methodDecl = context as IMethodDeclaration;
            if (methodDecl != null)
            {
                sst.Methods.Remove(methodDecl);
                return;
            }

            var propertyDecl = context as IPropertyDeclaration;
            if (propertyDecl != null)
            {
                sst.Properties.Remove(propertyDecl);
            }
        }

        public void Visit(IDelegateDeclaration decl, ISSTNode context)
        {
        }

        public void Visit(IEventDeclaration decl, ISSTNode context)
        {
        }

        public void Visit(IFieldDeclaration decl, ISSTNode context)
        {
        }

        public void Visit(IMethodDeclaration decl, ISSTNode context)
        {
            var statement = context as IStatement;
            if (statement != null) decl.Body.Remove(statement);
        }

        public void Visit(IParameterDeclaration decl, ISSTNode context)
        {
        }

        public void Visit(IPropertyDeclaration decl, ISSTNode context)
        {
            var statement = context as IStatement;
            if (statement != null)
            {
                decl.Get.Remove(statement);
                decl.Set.Remove(statement);
            }
        }

        public void Visit(IVariableDeclaration decl, ISSTNode context)
        {
        }

        public void Visit(IAssignment stmt, ISSTNode context)
        {
        }

        public void Visit(IBreakStatement stmt, ISSTNode context)
        {
        }

        public void Visit(IContinueStatement stmt, ISSTNode context)
        {
        }

        public void Visit(IEventSubscriptionStatement stmt, ISSTNode context)
        {
        }

        public void Visit(IExpressionStatement stmt, ISSTNode context)
        {
        }

        public void Visit(IGotoStatement stmt, ISSTNode context)
        {
        }

        public void Visit(ILabelledStatement stmt, ISSTNode context)
        {
        }

        public void Visit(IReturnStatement stmt, ISSTNode context)
        {
        }

        public void Visit(IThrowStatement stmt, ISSTNode context)
        {
        }

        public void Visit(IDoLoop block, ISSTNode context)
        {
            var statement = context as IStatement;
            if (statement != null) block.Body.Remove(statement);
        }

        public void Visit(IForEachLoop block, ISSTNode context)
        {
            var statement = context as IStatement;
            if (statement != null) block.Body.Remove(statement);
        }

        public void Visit(IForLoop block, ISSTNode context)
        {
            var statement = context as IStatement;
            if (statement != null)
            {
                block.Init.Remove(statement);
                block.Body.Remove(statement);
                block.Step.Remove(statement);
            }
        }

        public void Visit(IIfElseBlock block, ISSTNode context)
        {
            var statement = context as IStatement;
            if (statement != null)
            {
                block.Then.Remove(statement);
                block.Else.Remove(statement);
            }
        }

        public void Visit(ILockBlock block, ISSTNode context)
        {
            var statement = context as IStatement;
            if (statement != null) block.Body.Remove(statement);
        }

        public void Visit(ISwitchBlock block, ISSTNode context)
        {
            var statement = context as IStatement;
            if (statement != null) block.DefaultSection.Remove(statement);
        }

        public void Visit(ITryBlock block, ISSTNode context)
        {
            var statement = context as IStatement;
            if (statement != null)
            {
                block.Body.Remove(statement);
                block.Finally.Remove(statement);
            }
        }

        public void Visit(IUncheckedBlock block, ISSTNode context)
        {
            var statement = context as IStatement;
            if (statement != null) block.Body.Remove(statement);
        }

        public void Visit(IUnsafeBlock block, ISSTNode context)
        {
        }

        public void Visit(IUsingBlock block, ISSTNode context)
        {
            var statement = context as IStatement;
            if (statement != null) block.Body.Remove(statement);
        }

        public void Visit(IWhileLoop block, ISSTNode context)
        {
            var statement = context as IStatement;
            if (statement != null) block.Body.Remove(statement);
        }

        public void Visit(ICompletionExpression expr, ISSTNode context)
        {
        }

        public void Visit(IComposedExpression expr, ISSTNode context)
        {
            var varRef = context as IVariableReference;
            if (varRef != null) expr.References.Remove(varRef);
        }

        public void Visit(IIfElseExpression expr, ISSTNode context)
        {
        }

        public void Visit(IInvocationExpression expr, ISSTNode context)
        {
            var simpleExpr = context as ISimpleExpression;
            if (simpleExpr != null) expr.Parameters.Remove(simpleExpr);
        }

        public void Visit(ILambdaExpression expr, ISSTNode context)
        {
            var statement = context as IStatement;
            if (statement != null) expr.Body.Remove(statement);
        }

        public void Visit(ILoopHeaderBlockExpression expr, ISSTNode context)
        {
            var statement = context as IStatement;
            if (statement != null) expr.Body.Remove(statement);
        }

        public void Visit(IConstantValueExpression expr, ISSTNode context)
        {
        }

        public void Visit(INullExpression expr, ISSTNode context)
        {
        }

        public void Visit(IReferenceExpression expr, ISSTNode context)
        {
        }

        public void Visit(ICastExpression expr, ISSTNode context)
        {
        }

        public void Visit(IIndexAccessExpression expr, ISSTNode context)
        {
            var simpleExpr = context as ISimpleExpression;
            if (simpleExpr != null) expr.Indices.Remove(simpleExpr);
        }

        public void Visit(ITypeCheckExpression expr, ISSTNode context)
        {
        }

        public void Visit(IUnaryExpression expr, ISSTNode context)
        {
        }

        public void Visit(IBinaryExpression expr, ISSTNode context)
        {
        }

        public void Visit(IEventReference eventRef, ISSTNode context)
        {
        }

        public void Visit(IFieldReference fieldRef, ISSTNode context)
        {
        }

        public void Visit(IMethodReference methodRef, ISSTNode context)
        {
        }

        public void Visit(IPropertyReference propertyRef, ISSTNode context)
        {
        }

        public void Visit(ITypeReference typeReference, ISSTNode context)
        {
        }

        public void Visit(IVariableReference varRef, ISSTNode context)
        {
        }

        public void Visit(IIndexAccessReference indexAccessRef, ISSTNode context)
        {
        }
        
        
        public void Visit(ISimpleName simpleName, ISSTNode context)
        {
        }

        public void Visit(IUnknownReference unknownRef, ISSTNode context)
        {
        }

        public void Visit(IUnknownExpression unknownExpr, ISSTNode context)
        {
        }

        public void Visit(IUnknownStatement unknownStmt, ISSTNode context)
        {
        }
    }
}
﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using KaVE.Commons.Model.SSTs;
using KaVE.Commons.Model.SSTs.Blocks;
using KaVE.Commons.Model.SSTs.Declarations;
using KaVE.Commons.Model.SSTs.Expressions.Assignable;
using KaVE.Commons.Model.SSTs.Expressions.LoopHeader;
using KaVE.Commons.Model.SSTs.Expressions.Simple;
using KaVE.Commons.Model.SSTs.References;
using KaVE.Commons.Model.SSTs.Statements;
using KaVE.Commons.Model.SSTs.Visitor;

namespace SSTDiff.Util.Visitor.NodeUtil
{
    public class OldValuePrintVisitor : ISSTNodeVisitor<object, string>
    {
        public string Visit(ISST sst, object context)
        {
            return sst.EnclosingType.ToString();
        }

        public string Visit(IDelegateDeclaration decl, object context)
        {
            return decl.Name.ToString();
        }

        public string Visit(IEventDeclaration decl, object context)
        {
            return decl.Name.ToString();
        }

        public string Visit(IFieldDeclaration decl, object context)
        {
            return decl.IsStatic ? "static" : "";
        }

        public string Visit(IMethodDeclaration decl, object context)
        {
            return decl.IsStatic ? "static" : "";
        }

        public string Visit(IParameterDeclaration decl, object context)
        {
            if (decl.IsPassedByReference)
            {
                return "ref";
            }

            if (decl.IsOutput)
            {
                return "out";
            }

            if (decl.IsOptional)
            {
                return "opt";
            }

            if (decl.IsParameterArray)
            {
                return ("params");
            }

            return "";
        }

        public string Visit(IPropertyDeclaration decl, object context)
        {
            return decl.Name.ToString();
        }

        public string Visit(IVariableDeclaration decl, object context)
        {
            return decl.Type.Name;
        }

        public string Visit(IAssignment stmt, object context)
        {
            return "";
        }

        public string Visit(IBreakStatement stmt, object context)
        {
            return "";
        }

        public string Visit(IContinueStatement stmt, object context)
        {
            return "";
        }

        public string Visit(IEventSubscriptionStatement stmt, object context)
        {
            return stmt.Operation.ToString();
        }

        public string Visit(IExpressionStatement stmt, object context)
        {
            return "";
        }

        public string Visit(IGotoStatement stmt, object context)
        {
            return stmt.Label;
        }

        public string Visit(ILabelledStatement stmt, object context)
        {
            return stmt.Label;
        }

        public string Visit(IReturnStatement stmt, object context)
        {
            return "";
        }

        public string Visit(IThrowStatement stmt, object context)
        {
            return "";
        }

        public string Visit(IDoLoop block, object context)
        {
            return "";
        }

        public string Visit(IForEachLoop block, object context)
        {
            return "";
        }

        public string Visit(IForLoop block, object context)
        {
            return "";
        }

        public string Visit(IIfElseBlock block, object context)
        {
            return "";
        }

        public string Visit(ILockBlock block, object context)
        {
            return "";
        }

        public string Visit(ISwitchBlock block, object context)
        {
            return "";
        }

        public string Visit(ITryBlock block, object context)
        {
            return "";
        }

        public string Visit(IUncheckedBlock block, object context)
        {
            return "";
        }

        public string Visit(IUnsafeBlock block, object context)
        {
            return "";
        }

        public string Visit(IUsingBlock block, object context)
        {
            return "";
        }

        public string Visit(IWhileLoop block, object context)
        {
            return "";
        }

        public string Visit(ICompletionExpression expr, object context)
        {
            return expr.TypeReference + " " + expr.Token;
        }

        public string Visit(IComposedExpression expr, object context)
        {
            return "";
        }

        public string Visit(IIfElseExpression expr, object context)
        {
            return "";
        }

        public string Visit(IInvocationExpression expr, object context)
        {
            return expr.MethodName.ToString();
        }

        public string Visit(ILambdaExpression expr, object context)
        {
            return expr.Name.ToString();
        }

        public string Visit(ILoopHeaderBlockExpression expr, object context)
        {
            return "";
        }

        public string Visit(IConstantValueExpression expr, object context)
        {
            return expr.Value ?? "";
        }

        public string Visit(INullExpression expr, object context)
        {
            return "";
        }

        public string Visit(IReferenceExpression expr, object context)
        {
            return "";
        }

        public string Visit(ICastExpression expr, object context)
        {
            return expr.TargetType.ToString();
        }

        public string Visit(IIndexAccessExpression expr, object context)
        {
            return "";
        }

        public string Visit(ITypeCheckExpression expr, object context)
        {
            return expr.Type.ToString();
        }

        public string Visit(IUnaryExpression expr, object context)
        {
            return expr.Operator.ToString();
        }

        public string Visit(IBinaryExpression expr, object context)
        {
            return expr.Operator.ToString();
        }

        public string Visit(IEventReference eventRef, object context)
        {
            return eventRef.EventName.ToString();
        }

        public string Visit(IFieldReference fieldRef, object context)
        {
            return fieldRef.FieldName.ToString();
        }

        public string Visit(IMethodReference methodRef, object context)
        {
            return methodRef.MethodName.ToString();
        }

        public string Visit(IPropertyReference propertyRef, object context)
        {
            return propertyRef.PropertyName.ToString();
        }

        public string Visit(ITypeReference typeReference, object context)
        {
            return typeReference.TypeName.Name;
        }

        public string Visit(IVariableReference varRef, object context)
        {
            return varRef.Identifier;
        }

        public string Visit(IIndexAccessReference indexAccessRef, object context)
        {
            return "";
        }

        public string Visit(ISimpleName simpleName, object context)
        {
            return simpleName.Name;
        }

        public string Visit(IUnknownReference unknownRef, object context)
        {
            return "";
        }

        public string Visit(IUnknownExpression unknownExpr, object context)
        {
            return "";
        }

        public string Visit(IUnknownStatement unknownStmt, object context)
        {
            return "";
        }
    }
}
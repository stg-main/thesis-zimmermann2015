﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using KaVE.Commons.Model.SSTs;
using KaVE.Commons.Model.SSTs.Blocks;
using KaVE.Commons.Model.SSTs.Declarations;
using KaVE.Commons.Model.SSTs.Expressions.Assignable;
using KaVE.Commons.Model.SSTs.Expressions.LoopHeader;
using KaVE.Commons.Model.SSTs.Expressions.Simple;
using KaVE.Commons.Model.SSTs.Impl;
using KaVE.Commons.Model.SSTs.Impl.Blocks;
using KaVE.Commons.Model.SSTs.Impl.Declarations;
using KaVE.Commons.Model.SSTs.Impl.Expressions.Assignable;
using KaVE.Commons.Model.SSTs.Impl.Expressions.LoopHeader;
using KaVE.Commons.Model.SSTs.Impl.Expressions.Simple;
using KaVE.Commons.Model.SSTs.Impl.References;
using KaVE.Commons.Model.SSTs.Impl.Statements;
using KaVE.Commons.Model.SSTs.References;
using KaVE.Commons.Model.SSTs.Statements;
using KaVE.Commons.Model.SSTs.Visitor;

namespace SSTDiff.Util.Visitor.NodeUtil
{
    public class CloneVisitor : ISSTNodeVisitor<ISSTNode, ISSTNode>
    {
        public ISSTNode Visit(ISST sst, ISSTNode context)
        {
            return new SST
            {
                EnclosingType = sst.EnclosingType
            };
        }

        public ISSTNode Visit(IDelegateDeclaration decl, ISSTNode context)
        {
            return new DelegateDeclaration
            {
                Name = decl.Name
            };
        }

        public ISSTNode Visit(IEventDeclaration decl, ISSTNode context)
        {
            return new EventDeclaration
            {
                Name = decl.Name
            };
        }

        public ISSTNode Visit(IFieldDeclaration decl, ISSTNode context)
        {
            return new FieldDeclaration
            {
                Name = decl.Name,
                IsStatic = decl.IsStatic
            };
        }

        public ISSTNode Visit(IMethodDeclaration decl, ISSTNode context)
        {
            return new MethodDeclaration
            {
                IsEntryPoint = decl.IsEntryPoint,
                IsStatic = decl.IsStatic,
                MethodName = decl.MethodName,
                ReturnType = decl.ReturnType,
                DeclaringType = decl.DeclaringType
            };
        }

        public ISSTNode Visit(IParameterDeclaration decl, ISSTNode context)
        {
            return new ParameterDeclaration
            {
                IsOptional = decl.IsOptional,
                IsOutput = decl.IsOutput,
                IsParameterArray = decl.IsParameterArray,
                IsPassedByReference = decl.IsPassedByReference
            };
        }

        public ISSTNode Visit(IPropertyDeclaration decl, ISSTNode context)
        {
            return new PropertyDeclaration
            {
                Name = decl.Name
            };
        }

        public ISSTNode Visit(IVariableDeclaration decl, ISSTNode context)
        {
            return new VariableDeclaration
            {
                Type = decl.Type,
                // creating temporary reference
                Reference = (VariableReference) decl.Reference.CloneNode()
            };
        }

        public ISSTNode Visit(IAssignment stmt, ISSTNode context)
        {
            return new Assignment
            {
                Reference = stmt.Reference,
                Expression = stmt.Expression
            };
        }

        public ISSTNode Visit(IBreakStatement stmt, ISSTNode context)
        {
            return new BreakStatement();
        }

        public ISSTNode Visit(IContinueStatement stmt, ISSTNode context)
        {
            return new ContinueStatement();
        }

        public ISSTNode Visit(IEventSubscriptionStatement stmt, ISSTNode context)
        {
            return new EventSubscriptionStatement
            {
                Operation = stmt.Operation,
                Reference = stmt.Reference,
                Expression = stmt.Expression
            };
        }

        public ISSTNode Visit(IExpressionStatement stmt, ISSTNode context)
        {
            return new ExpressionStatement
            {
                Expression = stmt.Expression
            };
        }

        public ISSTNode Visit(IGotoStatement stmt, ISSTNode context)
        {
            return new GotoStatement
            {
                Label = stmt.Label
            };
        }

        public ISSTNode Visit(ILabelledStatement stmt, ISSTNode context)
        {
            return new LabelledStatement
            {
                Label = stmt.Label,
                Statement = stmt.Statement
            };
        }

        public ISSTNode Visit(IReturnStatement stmt, ISSTNode context)
        {
            return new ReturnStatement
            {
                IsVoid = stmt.IsVoid,
                Expression = stmt.Expression
            };
        }

        public ISSTNode Visit(IThrowStatement stmt, ISSTNode context)
        {
            return new ThrowStatement
            {
                Reference = stmt.Reference
            };
        }

        public ISSTNode Visit(IDoLoop block, ISSTNode context)
        {
            return new DoLoop
            {
                Condition = block.Condition
            };
        }

        public ISSTNode Visit(IForEachLoop block, ISSTNode context)
        {
            return new ForEachLoop
            {
                Declaration = block.Declaration,
                LoopedReference= block.LoopedReference
            };
        }

        public ISSTNode Visit(IForLoop block, ISSTNode context)
        {
            return new ForLoop
            {
                Condition = block.Condition
            };
        }

        public ISSTNode Visit(IIfElseBlock block, ISSTNode context)
        {
            return new IfElseBlock
            {
                Condition = block.Condition
            };
        }

        public ISSTNode Visit(ILockBlock block, ISSTNode context)
        {
            return new LockBlock
            {
                Reference = block.Reference
            };
        }

        public ISSTNode Visit(ISwitchBlock block, ISSTNode context)
        {
            return new SwitchBlock
            {
                Sections = block.Sections,
                Reference = block.Reference
            };
        }

        public ISSTNode Visit(ITryBlock block, ISSTNode context)
        {
            return new TryBlock
            {
                CatchBlocks = block.CatchBlocks
            };
        }

        public ISSTNode Visit(IUncheckedBlock block, ISSTNode context)
        {
            return new UncheckedBlock();
        }

        public ISSTNode Visit(IUnsafeBlock block, ISSTNode context)
        {
            return new UnsafeBlock();
        }

        public ISSTNode Visit(IUsingBlock block, ISSTNode context)
        {
            return new UsingBlock
            {
                Reference = block.Reference
            };
        }

        public ISSTNode Visit(IWhileLoop block, ISSTNode context)
        {
            return new WhileLoop
            {
                Condition = block.Condition
            };
        }

        public ISSTNode Visit(ICompletionExpression expr, ISSTNode context)
        {
            return new CompletionExpression
            {
                Token = expr.Token,
                TypeReference = expr.TypeReference
            };
        }

        public ISSTNode Visit(IComposedExpression expr, ISSTNode context)
        {
            return new ComposedExpression();
        }

        public ISSTNode Visit(IIfElseExpression expr, ISSTNode context)
        {
            return new IfElseExpression
            {
                Condition = expr.Condition,
                ThenExpression = expr.ThenExpression,
                ElseExpression = expr.ElseExpression
            };
        }

        public ISSTNode Visit(IInvocationExpression expr, ISSTNode context)
        {
            return new InvocationExpression
            {
                MethodName = expr.MethodName,
                Reference = expr.Reference
            };
        }

        public ISSTNode Visit(ILambdaExpression expr, ISSTNode context)
        {
            return new LambdaExpression
            {
                Name = expr.Name
            };
        }

        public ISSTNode Visit(ILoopHeaderBlockExpression expr, ISSTNode context)
        {
            return new LoopHeaderBlockExpression();
        }

        public ISSTNode Visit(IConstantValueExpression expr, ISSTNode context)
        {
            return new ConstantValueExpression
            {
                Value = expr.Value
            };
        }

        public ISSTNode Visit(INullExpression expr, ISSTNode context)
        {
            return new NullExpression();
        }

        public ISSTNode Visit(IReferenceExpression expr, ISSTNode context)
        {
            return new ReferenceExpression
            {
                Reference = expr.Reference
            };
        }

        public ISSTNode Visit(ICastExpression expr, ISSTNode context)
        {
            return new CastExpression
            {
                TargetType = expr.TargetType,
                Reference = expr.Reference
            };
        }

        public ISSTNode Visit(IIndexAccessExpression expr, ISSTNode context)
        {
            return new IndexAccessExpression
            {
                Reference = expr.Reference
            };
        }

        public ISSTNode Visit(ITypeCheckExpression expr, ISSTNode context)
        {
            return new TypeCheckExpression
            {
                Type = expr.Type,
                Reference = expr.Reference
            };
        }

        public ISSTNode Visit(IUnaryExpression expr, ISSTNode context)
        {
            return new UnaryExpression
            {
                Operand = expr.Operand,
                Operator = expr.Operator
            };
        }

        public ISSTNode Visit(IBinaryExpression expr, ISSTNode context)
        {
            return new BinaryExpression
            {
                LeftOperand= expr.LeftOperand,
                RightOperand= expr.RightOperand,
                Operator = expr.Operator
            };
        }

        public ISSTNode Visit(IEventReference eventRef, ISSTNode context)
        {
            return new EventReference
            {
                EventName = eventRef.EventName,
                Reference = eventRef.Reference
            };
        }

        public ISSTNode Visit(IFieldReference fieldRef, ISSTNode context)
        {
            return new FieldReference
            {
                FieldName = fieldRef.FieldName,
                Reference = fieldRef.Reference
            };
        }

        public ISSTNode Visit(IMethodReference methodRef, ISSTNode context)
        {
            return new MethodReference
            {
                MethodName = methodRef.MethodName,
                Reference = methodRef.Reference
            };
        }

        public ISSTNode Visit(IPropertyReference propertyRef, ISSTNode context)
        {
            return new PropertyReference
            {
                PropertyName = propertyRef.PropertyName,
                Reference = propertyRef.Reference
            };
        }

        public ISSTNode Visit(ITypeReference typeReference, ISSTNode context)
        {
            return new TypeReference
            {
                TypeName = typeReference.TypeName,
            };
        }

        public ISSTNode Visit(IVariableReference varRef, ISSTNode context)
        {
            return new VariableReference
            {
                Identifier = varRef.Identifier
            };
        }

        public ISSTNode Visit(ISimpleName simpleName, ISSTNode context)
        {
            return new SimpleName
            {
                Name = simpleName.Name
            };
        }

        public ISSTNode Visit(IIndexAccessReference indexAccessRef, ISSTNode context)
        {
            return new IndexAccessReference
            {
                Expression = indexAccessRef.Expression
            };
        }

        public ISSTNode Visit(IUnknownReference unknownRef, ISSTNode context)
        {
            return new UnknownReference();
        }

        public ISSTNode Visit(IUnknownExpression unknownExpr, ISSTNode context)
        {
            return new UnknownExpression();
        }

        public ISSTNode Visit(IUnknownStatement unknownStmt, ISSTNode context)
        {
            return new UnknownStatement();
        }
    }
}
﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using KaVE.Commons.Model.SSTs;
using KaVE.Commons.Model.SSTs.Blocks;
using KaVE.Commons.Model.SSTs.Declarations;
using KaVE.Commons.Model.SSTs.Expressions.Assignable;
using KaVE.Commons.Model.SSTs.Expressions.LoopHeader;
using KaVE.Commons.Model.SSTs.Expressions.Simple;
using KaVE.Commons.Model.SSTs.References;
using KaVE.Commons.Model.SSTs.Statements;
using KaVE.Commons.Model.SSTs.Visitor;

namespace SSTDiff.Util.Visitor.NodeUtil
{
    public class ValueAsStringVisitor : ISSTNodeVisitor<bool, string>
    {
        public string Visit(ISST sst, bool context)
        {
            return sst.EnclosingType.ToString();
        }

        public string Visit(IDelegateDeclaration decl, bool context)
        {
            return "CSharp.DelegateTypeName:" + decl.Name;
        }

        public string Visit(IEventDeclaration decl, bool context)
        {
            return "CSharp.EventName:" + decl.Name;
        }

        public string Visit(IFieldDeclaration decl, bool context)
        {
            return decl.IsStatic ? "static" : "";
        }

        public string Visit(IMethodDeclaration decl, bool context)
        {
            return decl.IsStatic ? "static" : "";
        }

        public string Visit(IParameterDeclaration decl, bool context)
        {
            if (decl.IsPassedByReference)
            {
                return "ref";
            }

            if (decl.IsOutput)
            {
                return "out";
            }

            if (decl.IsOptional)
            {
                return "opt";
            }

            if (decl.IsParameterArray)
            {
                return ("params");
            }

            return "";
        }

        public string Visit(IPropertyDeclaration decl, bool context)
        {
            return "CSharp.PropertyName" + decl.Name;
        }

        public string Visit(IVariableDeclaration decl, bool context)
        {
            return decl.Type.ToString();
        }

        public string Visit(IAssignment stmt, bool context)
        {
            return "";
        }

        public string Visit(IBreakStatement stmt, bool context)
        {
            return "";
        }

        public string Visit(IContinueStatement stmt, bool context)
        {
            return "";
        }

        public string Visit(IEventSubscriptionStatement stmt, bool context)
        {
            return stmt.Operation.ToString();
        }

        public string Visit(IExpressionStatement stmt, bool context)
        {
            return "";
        }

        public string Visit(IGotoStatement stmt, bool context)
        {
            return stmt.Label;
        }

        public string Visit(ILabelledStatement stmt, bool context)
        {
            return stmt.Label;
        }

        public string Visit(IReturnStatement stmt, bool context)
        {
            return "";
        }

        public string Visit(IThrowStatement stmt, bool context)
        {
            return "";
        }

        public string Visit(IDoLoop block, bool context)
        {
            return "";
        }

        public string Visit(IForEachLoop block, bool context)
        {
            return "";
        }

        public string Visit(IForLoop block, bool context)
        {
            return "";
        }

        public string Visit(IIfElseBlock block, bool context)
        {
            return "";
        }

        public string Visit(ILockBlock block, bool context)
        {
            return "";
        }

        public string Visit(ISwitchBlock block, bool context)
        {
            return "";
        }

        public string Visit(ITryBlock block, bool context)
        {
            return "";
        }

        public string Visit(IUncheckedBlock block, bool context)
        {
            return "";
        }

        public string Visit(IUnsafeBlock block, bool context)
        {
            return "";
        }

        public string Visit(IUsingBlock block, bool context)
        {
            return "";
        }

        public string Visit(IWhileLoop block, bool context)
        {
            return "";
        }

        public string Visit(ICompletionExpression expr, bool context)
        {
            return expr.TypeReference + " " + expr.Token;
        }

        public string Visit(IComposedExpression expr, bool context)
        {
            return "";
        }

        public string Visit(IIfElseExpression expr, bool context)
        {
            return "";
        }

        public string Visit(IInvocationExpression expr, bool context)
        {
            if (context)
            {
                return expr.MethodName.Identifier.Replace(expr.MethodName.ReturnType.Identifier, "");
            }
            return "CSharp.MethodName:" + expr.MethodName;
        }

        public string Visit(ILambdaExpression expr, bool context)
        {
            return expr.Name.Parameters.ToString();
        }

        public string Visit(ILoopHeaderBlockExpression expr, bool context)
        {
            return "";
        }

        public string Visit(IConstantValueExpression expr, bool context)
        {
            return expr.Value ?? "";
        }

        public string Visit(INullExpression expr, bool context)
        {
            return "";
        }

        public string Visit(IReferenceExpression expr, bool context)
        {
            return "";
        }

        public string Visit(ICastExpression expr, bool context)
        {
            return expr.TargetType.ToString();
        }

        public string Visit(IIndexAccessExpression expr, bool context)
        {
            return "";
        }

        public string Visit(ITypeCheckExpression expr, bool context)
        {
            return expr.Type.ToString();
        }

        public string Visit(IUnaryExpression expr, bool context)
        {
            return expr.Operator.ToString();
        }

        public string Visit(IBinaryExpression expr, bool context)
        {
            return expr.Operator.ToString();
        }

        public string Visit(IEventReference eventRef, bool context)
        {
            return eventRef.EventName.Name;
        }

        public string Visit(IFieldReference fieldRef, bool context)
        {
            return fieldRef.FieldName.Name;
        }

        public string Visit(IMethodReference methodRef, bool context)
        {
            return "CSharp.MethodName:" + methodRef.MethodName;
        }

        public string Visit(IPropertyReference propertyRef, bool context)
        {
            return propertyRef.PropertyName.Name;
        }

        public string Visit(ITypeReference typeReference, bool context)
        {
            return typeReference.TypeName.FullName;
        }

        public string Visit(IVariableReference varRef, bool context)
        {
            return varRef.Identifier;
        }

        public string Visit(IIndexAccessReference indexAccessRef, bool context)
        {
            return "";
        }
        
        public string Visit(ISimpleName simpleName, bool context)
        {
            return simpleName.Name;
        }

        public string Visit(IUnknownReference unknownRef, bool context)
        {
            return "";
        }

        public string Visit(IUnknownExpression unknownExpr, bool context)
        {
            return "";
        }

        public string Visit(IUnknownStatement unknownStmt, bool context)
        {
            return "";
        }
    }
}
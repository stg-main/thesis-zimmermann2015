﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;
using KaVE.Commons.Model.SSTs;
using KaVE.Commons.Model.SSTs.Blocks;
using KaVE.Commons.Model.SSTs.Declarations;
using KaVE.Commons.Model.SSTs.Expressions.Assignable;
using KaVE.Commons.Model.SSTs.Expressions.LoopHeader;
using KaVE.Commons.Model.SSTs.Expressions.Simple;
using KaVE.Commons.Model.SSTs.References;
using KaVE.Commons.Model.SSTs.Statements;
using KaVE.Commons.Model.SSTs.Visitor;
using KaVE.Commons.Utils.Collections;

namespace SSTDiff.Util.Visitor.NodeUtil
{
    public class ChildrenListVisitor : ISSTNodeVisitor<object, List<ISSTNode>>
    {
        public List<ISSTNode> Visit(ISST sst, object context)
        {
            var children = new KaVEList<ISSTNode>();
            children.AddRange(sst.Delegates);
            children.AddRange(sst.Events);
            children.AddRange(sst.Fields);
            children.AddRange(sst.Properties);
            children.AddRange(sst.Methods);
            return children;
        }

        public List<ISSTNode> Visit(IDelegateDeclaration decl, object context)
        {
            return new KaVEList<ISSTNode>();
        }

        public List<ISSTNode> Visit(IEventDeclaration decl, object context)
        {
            return new KaVEList<ISSTNode>();
        }

        public List<ISSTNode> Visit(IFieldDeclaration decl, object context)
        {
            return new KaVEList<ISSTNode>{decl.ValueType, decl.FieldName};
        }

        public List<ISSTNode> Visit(IMethodDeclaration decl, object context)
        {
            var children = new KaVEList<ISSTNode>{decl.MethodName, decl.ReturnType};
            children.AddRange(decl.Parameters);
            children.AddRange(decl.Body);
            return children;
        }

        public List<ISSTNode> Visit(IParameterDeclaration decl, object context)
        {
            var children = new KaVEList<ISSTNode>{decl.Name, decl.Type};
            return children;
        }

        public List<ISSTNode> Visit(IPropertyDeclaration decl, object context)
        {
            var children = new KaVEList<ISSTNode>();
            children.AddRange(decl.Get);
            children.AddRange(decl.Set);
            return children;
        }

        public List<ISSTNode> Visit(IVariableDeclaration decl, object context)
        {
            var children = new KaVEList<ISSTNode> {decl.Reference};
            return children;
        }

        public List<ISSTNode> Visit(IAssignment stmt, object context)
        {
            var children = new KaVEList<ISSTNode> {stmt.Reference, stmt.Expression};
            return children;
        }

        public List<ISSTNode> Visit(IBreakStatement stmt, object context)
        {
            return new KaVEList<ISSTNode>();
        }

        public List<ISSTNode> Visit(IContinueStatement stmt, object context)
        {
            return new KaVEList<ISSTNode>();
        }

        public List<ISSTNode> Visit(IEventSubscriptionStatement stmt, object context)
        {
            var children = new KaVEList<ISSTNode> {stmt.Reference, stmt.Expression};
            return children;
        }

        public List<ISSTNode> Visit(IExpressionStatement stmt, object context)
        {
            var children = new KaVEList<ISSTNode> {stmt.Expression};
            return children;
        }

        public List<ISSTNode> Visit(IGotoStatement stmt, object context)
        {
            return new KaVEList<ISSTNode>();
        }

        public List<ISSTNode> Visit(ILabelledStatement stmt, object context)
        {
            var children = new KaVEList<ISSTNode> {stmt.Statement};
            return children;
        }

        public List<ISSTNode> Visit(IReturnStatement stmt, object context)
        {
            var children = new KaVEList<ISSTNode> {stmt.Expression};
            return children;
        }

        public List<ISSTNode> Visit(IThrowStatement stmt, object context)
        {
            var children = new KaVEList<ISSTNode> {stmt.Reference};
            return children;
        }

        public List<ISSTNode> Visit(IDoLoop block, object context)
        {
            var children = new KaVEList<ISSTNode> {block.Condition};
            children.AddRange(block.Body);
            return children;
        }

        public List<ISSTNode> Visit(IForEachLoop block, object context)
        {
            var children = new KaVEList<ISSTNode> {block.Declaration, block.LoopedReference};
            children.AddRange(block.Body);
            return children;
        }

        public List<ISSTNode> Visit(IForLoop block, object context)
        {
            var children = new KaVEList<ISSTNode> {block.Condition};
            children.AddRange(block.Init);
            children.AddRange(block.Step);
            children.AddRange(block.Body);
            return children;
        }

        public List<ISSTNode> Visit(IIfElseBlock block, object context)
        {
            var children = new KaVEList<ISSTNode> {block.Condition};
            children.AddRange(block.Then);
            children.AddRange(block.Else);
            return children;
        }

        public List<ISSTNode> Visit(ILockBlock block, object context)
        {
            var children = new KaVEList<ISSTNode> {block.Reference};
            children.AddRange(block.Body);
            return children;
        }

        public List<ISSTNode> Visit(ISwitchBlock block, object context)
        {
            var children = new KaVEList<ISSTNode> {block.Reference};
            // No support for CaseBlocks
            children.AddRange(block.DefaultSection);
            return children;
        }

        public List<ISSTNode> Visit(ITryBlock block, object context)
        {
            var children = new KaVEList<ISSTNode>();
            // No support for Catchblocks
            children.AddRange(block.Body);
            children.AddRange(block.Finally);
            return children;
        }

        public List<ISSTNode> Visit(IUncheckedBlock block, object context)
        {
            var children = new KaVEList<ISSTNode>();
            children.AddRange(block.Body);
            return children;
        }

        public List<ISSTNode> Visit(IUnsafeBlock block, object context)
        {
            return new KaVEList<ISSTNode>();
        }

        public List<ISSTNode> Visit(IUsingBlock block, object context)
        {
            var children = new KaVEList<ISSTNode> {block.Reference};
            children.AddRange(block.Body);
            return children;
        }

        public List<ISSTNode> Visit(IWhileLoop block, object context)
        {
            var children = new KaVEList<ISSTNode> {block.Condition};
            children.AddRange(block.Body);
            return children;
        }

        public List<ISSTNode> Visit(ICompletionExpression expr, object context)
        {
            if (expr.VariableReference != null)
            {
                var children = new KaVEList<ISSTNode> {expr.VariableReference};
                return children;
            }
            return new KaVEList<ISSTNode>();
        }

        public List<ISSTNode> Visit(IComposedExpression expr, object context)
        {
            var children = new KaVEList<ISSTNode>();
            children.AddRange(expr.References);
            return children;
        }

        public List<ISSTNode> Visit(IIfElseExpression expr, object context)
        {
            var children = new KaVEList<ISSTNode> {expr.Condition, expr.ThenExpression, expr.ElseExpression};
            return children;
        }

        public List<ISSTNode> Visit(IInvocationExpression expr, object context)
        {
            var children = new KaVEList<ISSTNode> {expr.Reference};
            children.AddRange(expr.Parameters);
            return children;
        }

        public List<ISSTNode> Visit(ILambdaExpression expr, object context)
        {
            var children = new KaVEList<ISSTNode>();
            children.AddRange(expr.Body);
            return children;
        }

        public List<ISSTNode> Visit(ILoopHeaderBlockExpression expr, object context)
        {
            var children = new KaVEList<ISSTNode>();
            children.AddRange(expr.Body);
            return children;
        }

        public List<ISSTNode> Visit(IConstantValueExpression expr, object context)
        {
            return new KaVEList<ISSTNode>();
        }

        public List<ISSTNode> Visit(INullExpression expr, object context)
        {
            return new KaVEList<ISSTNode>();
        }

        public List<ISSTNode> Visit(IReferenceExpression expr, object context)
        {
            var children = new KaVEList<ISSTNode> {expr.Reference};
            return children;
        }

        public List<ISSTNode> Visit(ICastExpression expr, object context)
        {
            return new KaVEList<ISSTNode> {expr.Reference};
        }

        public List<ISSTNode> Visit(IIndexAccessExpression expr, object context)
        {
            var children = new KaVEList<ISSTNode> { expr.Reference};
            children.AddRange(expr.Indices);
            return children;
        }

        public List<ISSTNode> Visit(ITypeCheckExpression expr, object context)
        {
            return new KaVEList<ISSTNode> {expr.Reference};
        }

        public List<ISSTNode> Visit(IUnaryExpression expr, object context)
        {
            return new KaVEList<ISSTNode> {expr.Operand};
        }

        public List<ISSTNode> Visit(IBinaryExpression expr, object context)
        {
            return new KaVEList<ISSTNode> { expr.LeftOperand, expr.RightOperand };
        }

        public List<ISSTNode> Visit(IEventReference eventRef, object context)
        {
            var children = new KaVEList<ISSTNode> {eventRef.Reference};
            return children;
        }

        public List<ISSTNode> Visit(IFieldReference fieldRef, object context)
        {
            var children = new KaVEList<ISSTNode> {fieldRef.Reference};
            return children;
        }

        public List<ISSTNode> Visit(IMethodReference methodRef, object context)
        {
            var children = new KaVEList<ISSTNode> {methodRef.Reference};
            return children;
        }

        public List<ISSTNode> Visit(IPropertyReference propertyRef, object context)
        {
            var children = new KaVEList<ISSTNode> {propertyRef.Reference};
            return children;
        }

        public List<ISSTNode> Visit(ITypeReference typeReference, object context)
        {
            return new KaVEList<ISSTNode>();
        }

        public List<ISSTNode> Visit(IVariableReference varRef, object context)
        {
            return new KaVEList<ISSTNode>();
        }

        public List<ISSTNode> Visit(ISimpleName simpleName, object context)
        {
            return new KaVEList<ISSTNode>();
        }

        public List<ISSTNode> Visit(IIndexAccessReference indexAccessRef, object context)
        {
            return new KaVEList<ISSTNode> { indexAccessRef.Expression};
        }

        public List<ISSTNode> Visit(IUnknownReference unknownRef, object context)
        {
            return new KaVEList<ISSTNode>();
        }

        public List<ISSTNode> Visit(IUnknownExpression unknownExpr, object context)
        {
            return new KaVEList<ISSTNode>();
        }

        public List<ISSTNode> Visit(IUnknownStatement unknownStmt, object context)
        {
            return new KaVEList<ISSTNode>();
        }
    }
}
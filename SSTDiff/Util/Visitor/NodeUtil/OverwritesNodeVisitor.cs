﻿using KaVE.Commons.Model.SSTs;
using KaVE.Commons.Model.SSTs.Blocks;
using KaVE.Commons.Model.SSTs.Declarations;
using KaVE.Commons.Model.SSTs.Expressions;
using KaVE.Commons.Model.SSTs.Expressions.Assignable;
using KaVE.Commons.Model.SSTs.Expressions.LoopHeader;
using KaVE.Commons.Model.SSTs.Expressions.Simple;
using KaVE.Commons.Model.SSTs.References;
using KaVE.Commons.Model.SSTs.Statements;
using KaVE.Commons.Model.SSTs.Visitor;

namespace SSTDiff.Util.Visitor.NodeUtil
{
    public class OverwritesNodeVisitor : ISSTNodeVisitor<ISSTNode, ISSTNode>
    {
        private readonly int _position;

        public OverwritesNodeVisitor(int position)
        {
            _position = position;
        }

        public ISSTNode Visit(ISST sst, ISSTNode context)
        {
            return null;
        }

        public ISSTNode Visit(IDelegateDeclaration decl, ISSTNode context)
        {
            return null;
        }

        public ISSTNode Visit(IEventDeclaration decl, ISSTNode context)
        {
            return null;
        }

        public ISSTNode Visit(IFieldDeclaration decl, ISSTNode context)
        {
            if (context is ISimpleName)
            {
                return decl.FieldName;
            }
            if (context is ITypeReference)
            {
                return decl.ValueType;
            }
            return null;
        }

        public ISSTNode Visit(IMethodDeclaration decl, ISSTNode context)
        {
            if (context is ISimpleName)
            {
                return decl.MethodName;
            }
            if (context is ITypeReference)
            {
                return decl.ReturnType;
            }
            return null;
        }

        public ISSTNode Visit(IParameterDeclaration decl, ISSTNode context)
        {
            if (context is ISimpleName)
            {
                return decl.Name;
            }
            if (context is ITypeReference)
            {
                return decl.Type;
            }
            return null;
        }

        public ISSTNode Visit(IPropertyDeclaration decl, ISSTNode context)
        {
            return null;
        }

        public ISSTNode Visit(IVariableDeclaration decl, ISSTNode context)
        {
            if (context is IVariableReference)
            {
                return decl.Reference;
            }
            return null;
        }

        public ISSTNode Visit(IAssignment stmt, ISSTNode context)
        {
            if (context is IAssignableReference)
            {
                return stmt.Reference;
            }
            return stmt.Expression;
        }

        public ISSTNode Visit(IBreakStatement stmt, ISSTNode context)
        {
            return null;
        }

        public ISSTNode Visit(IContinueStatement stmt, ISSTNode context)
        {
            return null;
        }

        public ISSTNode Visit(IEventSubscriptionStatement stmt, ISSTNode context)
        {
            if (context is IAssignableReference)
            {
                return stmt.Reference;
            }
            return stmt.Expression;
        }

        public ISSTNode Visit(IExpressionStatement stmt, ISSTNode context)
        {
            return stmt.Expression;
        }

        public ISSTNode Visit(IGotoStatement stmt, ISSTNode context)
        {
            return null;
        }

        public ISSTNode Visit(ILabelledStatement stmt, ISSTNode context)
        {
            return stmt.Statement;
        }

        public ISSTNode Visit(IReturnStatement stmt, ISSTNode context)
        {
            return stmt.Expression;
        }

        public ISSTNode Visit(IThrowStatement stmt, ISSTNode context)
        {
            return stmt.Reference;
        }

        public ISSTNode Visit(IDoLoop block, ISSTNode context)
        {
            if (context is ILoopHeaderExpression)
            {
                return block.Condition;
            }
            return null;
        }

        public ISSTNode Visit(IForEachLoop block, ISSTNode context)
        {
            if (context is IVariableReference)
            {
                return block.LoopedReference;
            }
            if (context is IVariableDeclaration)
            {
                return block.Declaration;
            }
            return null;
        }

        public ISSTNode Visit(IForLoop block, ISSTNode context)
        {
            if (context is ILoopHeaderExpression)
            {
                return block.Condition;
            }
            return null;
        }

        public ISSTNode Visit(IIfElseBlock block, ISSTNode context)
        {
            if (context is ISimpleExpression)
            {
                return block.Condition;
            }
            return null;
        }

        public ISSTNode Visit(ILockBlock block, ISSTNode context)
        {
            if (context is IVariableReference)
            {
                return block.Reference;
            }
            return null;
        }

        public ISSTNode Visit(ISwitchBlock block, ISSTNode context)
        {
            if (context is IVariableReference)
            {
                return block.Reference;
            }
            return null;
        }

        public ISSTNode Visit(ITryBlock block, ISSTNode context)
        {
            return null;
        }

        public ISSTNode Visit(IUncheckedBlock block, ISSTNode context)
        {
            return null;
        }

        public ISSTNode Visit(IUnsafeBlock block, ISSTNode context)
        {
            return null;
        }

        public ISSTNode Visit(IUsingBlock block, ISSTNode context)
        {
            if (context is IVariableReference)
            {
                return block.Reference;
            }
            return null;
        }

        public ISSTNode Visit(IWhileLoop block, ISSTNode context)
        {
            if (context is ILoopHeaderExpression)
            {
                return block.Condition;
            }
            return null;
        }

        public ISSTNode Visit(ICompletionExpression expr, ISSTNode context)
        {
            if (context is IVariableReference)
            {
                return expr.VariableReference;
            }
            return null;
        }

        public ISSTNode Visit(IComposedExpression expr, ISSTNode context)
        {
            return null;
        }

        public ISSTNode Visit(IIfElseExpression expr, ISSTNode context)
        {
            switch (_position)
            {
                case 0:
                    return expr.Condition;
                case 1:
                    return expr.ThenExpression;
                case 2:
                    return expr.ElseExpression;
            }
            return null;
        }

        public ISSTNode Visit(IInvocationExpression expr, ISSTNode context)
        {
            if (context is IVariableReference)
            {
                return expr.Reference;
            }
            return null;
        }

        public ISSTNode Visit(ILambdaExpression expr, ISSTNode context)
        {
            return null;
        }

        public ISSTNode Visit(ILoopHeaderBlockExpression expr, ISSTNode context)
        {
            return null;
        }

        public ISSTNode Visit(IConstantValueExpression expr, ISSTNode context)
        {
            return null;
        }

        public ISSTNode Visit(INullExpression expr, ISSTNode context)
        {
            return null;
        }

        public ISSTNode Visit(IReferenceExpression expr, ISSTNode context)
        {
            if (context is IReference)
            {
                return expr.Reference;
            }
            return null;
        }

        public ISSTNode Visit(ICastExpression expr, ISSTNode context)
        {
            if (context is IVariableReference)
            {
                return expr.Reference;
            }
            return null;
        }

        public ISSTNode Visit(IIndexAccessExpression expr, ISSTNode context)
        {
            if (context is IVariableReference)
            {
                return expr.Reference;
            }
            return null;
        }

        public ISSTNode Visit(ITypeCheckExpression expr, ISSTNode context)
        {
            if (context is IVariableReference)
            {
                return expr.Reference;
            }
            return null;
        }

        public ISSTNode Visit(IUnaryExpression expr, ISSTNode context)
        {
            if (context is ISimpleExpression)
            {
                return expr.Operand;
            }
            return null;
        }

        public ISSTNode Visit(IBinaryExpression expr, ISSTNode context)
        {
            if (context is ISimpleExpression)
            {
                switch (_position)
                {
                    case 0:
                        return expr.LeftOperand;
                    case 1:
                        return expr.RightOperand;
                }
            }
            return null;
        }

        public ISSTNode Visit(IEventReference eventRef, ISSTNode context)
        {
            if (context is IVariableReference)
            {
                return eventRef.Reference;
            }
            return null;
        }

        public ISSTNode Visit(IFieldReference fieldRef, ISSTNode context)
        {
            if (context is IVariableReference)
            {
                return fieldRef.Reference;
            }
            return null;
        }

        public ISSTNode Visit(IMethodReference methodRef, ISSTNode context)
        {
            if (context is IVariableReference)
            {
                return methodRef.Reference;
            }
            return null;
        }

        public ISSTNode Visit(IPropertyReference propertyRef, ISSTNode context)
        {
            if (context is IVariableReference)
            {
                return propertyRef.Reference;
            }
            return null;
        }

        public ISSTNode Visit(ITypeReference typeReference, ISSTNode context)
        {
            return null;
        }

        public ISSTNode Visit(IVariableReference varRef, ISSTNode context)
        {
            return null;
        }

        public ISSTNode Visit(IIndexAccessReference indexAccessRef, ISSTNode context)
        {
            if (context is IIndexAccessExpression)
            {
                return indexAccessRef.Expression;
            }
            return null;
        }

        public ISSTNode Visit(ISimpleName simpleName, ISSTNode context)
        {
            return null;
        }

        public ISSTNode Visit(IUnknownReference unknownRef, ISSTNode context)
        {
            return null;
        }

        public ISSTNode Visit(IUnknownExpression unknownExpr, ISSTNode context)
        {
            return null;
        }

        public ISSTNode Visit(IUnknownStatement unknownStmt, ISSTNode context)
        {
            return null;
        }
    }
}
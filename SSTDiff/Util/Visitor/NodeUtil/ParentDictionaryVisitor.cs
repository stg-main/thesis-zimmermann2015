﻿using System.Collections.Generic;
using KaVE.Commons.Model.SSTs;
using KaVE.Commons.Model.SSTs.Blocks;
using KaVE.Commons.Model.SSTs.Declarations;
using KaVE.Commons.Model.SSTs.Expressions.Assignable;
using KaVE.Commons.Model.SSTs.Expressions.LoopHeader;
using KaVE.Commons.Model.SSTs.Expressions.Simple;
using KaVE.Commons.Model.SSTs.References;
using KaVE.Commons.Model.SSTs.Statements;
using KaVE.Commons.Model.SSTs.Visitor;

namespace SSTDiff.Util.Visitor.NodeUtil
{
    public class ParentDictionaryVisitor : ISSTNodeVisitor<Dictionary<ISSTNode, ISSTNode>>
    {
        public void Visit(ISST sst, Dictionary<ISSTNode, ISSTNode> context)
        {
            foreach (var delegateDeclaration in sst.Delegates)
            {
                context.Add(delegateDeclaration, sst);
                delegateDeclaration.Accept(this, context);
            }
            foreach (var eventDeclaration in sst.Events)
            {
                context.Add(eventDeclaration, sst);
                eventDeclaration.Accept(this, context);
            }
            foreach (var fieldDeclaration in sst.Fields)
            {
                context.Add(fieldDeclaration, sst);
                fieldDeclaration.Accept(this, context);
            }
            foreach (var propertyDeclaration in sst.Properties)
            {
                context.Add(propertyDeclaration, sst);
                propertyDeclaration.Accept(this, context);
            }
            foreach (var methodDeclaration in sst.Methods)
            {
                context.Add(methodDeclaration, sst);
                methodDeclaration.Accept(this, context);
            }
        }

        public void Visit(IDelegateDeclaration decl, Dictionary<ISSTNode, ISSTNode> context)
        {
        }

        public void Visit(IEventDeclaration decl, Dictionary<ISSTNode, ISSTNode> context)
        {
        }

        public void Visit(IFieldDeclaration decl, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(decl.ValueType, decl);
            context.Add(decl.FieldName, decl);
        }

        public void Visit(IMethodDeclaration decl, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(decl.MethodName, decl);
            context.Add(decl.ReturnType, decl);
            foreach (var parameterDeclaration in decl.Parameters)
            {
                context.Add(parameterDeclaration, decl);
                Visit(parameterDeclaration, context);
            }
            foreach (var statement in decl.Body)
            {
                context.Add(statement, decl);
                statement.Accept(this, context);
            }
            
        }

        public void Visit(IParameterDeclaration decl, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(decl.Name, decl);
            context.Add(decl.Type, decl);
        }

        public void Visit(IPropertyDeclaration decl, Dictionary<ISSTNode, ISSTNode> context)
        {
            foreach (var statement in decl.Get)
            {
                context.Add(statement, decl);
                statement.Accept(this, context);
            }
            foreach (var statement in decl.Set)
            {
                context.Add(statement, decl);
                statement.Accept(this, context);
            }
        }

        public void Visit(IVariableDeclaration decl, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(decl.Reference, decl);
        }

        public void Visit(IAssignment stmt, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(stmt.Reference, stmt);
            context.Add(stmt.Expression, stmt);
            stmt.Expression.Accept(this, context);
            stmt.Reference.Accept(this, context);
        }

        public void Visit(IBreakStatement stmt, Dictionary<ISSTNode, ISSTNode> context)
        {
        }

        public void Visit(IContinueStatement stmt, Dictionary<ISSTNode, ISSTNode> context)
        {
        }

        public void Visit(IEventSubscriptionStatement stmt, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(stmt.Reference, stmt);
            context.Add(stmt.Expression, stmt);
            stmt.Expression.Accept(this, context);
            stmt.Reference.Accept(this, context);
        }

        public void Visit(IExpressionStatement stmt, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(stmt.Expression, stmt);
            stmt.Expression.Accept(this, context);
        }

        public void Visit(IGotoStatement stmt, Dictionary<ISSTNode, ISSTNode> context)
        {
        }

        public void Visit(ILabelledStatement stmt, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(stmt.Statement, stmt);
            stmt.Statement.Accept(this, context);

        }

        public void Visit(IReturnStatement stmt, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(stmt.Expression, stmt);
            stmt.Expression.Accept(this, context);

        }

        public void Visit(IThrowStatement stmt, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(stmt.Reference, stmt);
            stmt.Reference.Accept(this, context);

        }

        public void Visit(IDoLoop block, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(block.Condition, block);
            block.Condition.Accept(this, context);
            foreach (var statement in block.Body)
            {
                context.Add(statement, block);
                statement.Accept(this, context);
            }
        }

        public void Visit(IForEachLoop block, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(block.Declaration, block);
            context.Add(block.LoopedReference, block);
            block.Declaration.Accept(this, context);
            block.LoopedReference.Accept(this, context);
            foreach (var statement in block.Body)
            {
                context.Add(statement, block);
                statement.Accept(this, context);

            }
        }

        public void Visit(IForLoop block, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(block.Condition, block);
            block.Condition.Accept(this, context);
            foreach (var statement in block.Init)
            {
                context.Add(statement, block);
                statement.Accept(this, context);
            }
            foreach (var statement in block.Body)
            {
                context.Add(statement, block);
                statement.Accept(this, context);
            }
            foreach (var statement in block.Step)
            {
                context.Add(statement, block);
                statement.Accept(this, context);
            }
        }

        public void Visit(IIfElseBlock block, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(block.Condition, block);
            block.Condition.Accept(this, context);
            foreach (var statement in block.Then)
            {
                context.Add(statement, block);
                statement.Accept(this, context);

            }
            foreach (var statement in block.Else)
            {
                context.Add(statement, block);
                statement.Accept(this, context);

            }
        }

        public void Visit(ILockBlock block, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(block.Reference, block);
            block.Reference.Accept(this, context);
            foreach (var statement in block.Body)
            {
                context.Add(statement, block);
                statement.Accept(this, context);

            }
        }

        public void Visit(ISwitchBlock block, Dictionary<ISSTNode, ISSTNode> context)
        {
            // No support for Caseblocks

            context.Add(block.Reference, block);
            block.Reference.Accept(this, context);
            foreach (var statement in block.DefaultSection)
            {
                context.Add(statement, block);
                statement.Accept(this, context);

            }
        }

        public void Visit(ITryBlock block, Dictionary<ISSTNode, ISSTNode> context)
        {
            // No support for Catchblocks

            foreach (var statement in block.Body)
            {
                context.Add(statement, block);
                statement.Accept(this, context);

            }
            foreach (var statement in block.Finally)
            {
                context.Add(statement, block);
                statement.Accept(this, context);

            }
        }

        public void Visit(IUncheckedBlock block, Dictionary<ISSTNode, ISSTNode> context)
        {
            foreach (var statement in block.Body)
            {
                context.Add(statement, block);
                statement.Accept(this, context);

            }
        }

        public void Visit(IUnsafeBlock block, Dictionary<ISSTNode, ISSTNode> context)
        {
        }

        public void Visit(IUsingBlock block, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(block.Reference, block);
            block.Reference.Accept(this, context);

            foreach (var statement in block.Body)
            {
                context.Add(statement, block);
                statement.Accept(this, context);

            }
        }

        public void Visit(IWhileLoop block, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(block.Condition, block);
            block.Condition.Accept(this, context);

            foreach (var statement in block.Body)
            {
                context.Add(statement, block);
                statement.Accept(this, context);

            }
        }

        public void Visit(ICompletionExpression expr, Dictionary<ISSTNode, ISSTNode> context)
        {
            if (expr.VariableReference != null)
            {
                context.Add(expr.VariableReference,expr);
            }
        }

        public void Visit(IComposedExpression expr, Dictionary<ISSTNode, ISSTNode> context)
        {
            foreach (var reference in expr.References)
            {
                context.Add(reference, expr);
                reference.Accept(this, context);

            }
        }

        public void Visit(IIfElseExpression expr, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(expr.Condition,expr);
            context.Add(expr.ThenExpression,expr);
            context.Add(expr.ElseExpression,expr);

            expr.Condition.Accept(this, context);
            expr.ThenExpression.Accept(this, context);
            expr.ElseExpression.Accept(this, context);
        }

        public void Visit(IInvocationExpression expr, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(expr.Reference, expr);
            expr.Reference.Accept(this, context);
            foreach (var simpleExpression in expr.Parameters)
            {
                context.Add(simpleExpression, expr);
                simpleExpression.Accept(this, context);
            }
        }

        public void Visit(ILambdaExpression expr, Dictionary<ISSTNode, ISSTNode> context)
        {
            foreach (var expression in expr.Body)
            {
                context.Add(expression, expr);
                expression.Accept(this, context);
            }
        }

        public void Visit(ILoopHeaderBlockExpression expr, Dictionary<ISSTNode, ISSTNode> context)
        {
            foreach (var expression in expr.Body)
            {
                context.Add(expression, expr);
                expression.Accept(this, context);
            }
        }

        public void Visit(IConstantValueExpression expr, Dictionary<ISSTNode, ISSTNode> context)
        {
        }

        public void Visit(INullExpression expr, Dictionary<ISSTNode, ISSTNode> context)
        {
        }

        public void Visit(IReferenceExpression expr, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(expr.Reference, expr);
            expr.Reference.Accept(this, context);

        }

        public void Visit(ICastExpression expr, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(expr.Reference, expr);
            expr.Reference.Accept(this, context);

        }

        public void Visit(IIndexAccessExpression expr, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(expr.Reference, expr);
            expr.Reference.Accept(this, context);

            foreach (var expression in expr.Indices)
            {
                context.Add(expression, expr);
                expression.Accept(this, context);

            }
        }

        public void Visit(ITypeCheckExpression expr, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(expr.Reference, expr);
            expr.Reference.Accept(this, context);
        }

        public void Visit(IUnaryExpression expr, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(expr.Operand, expr);
            expr.Operand.Accept(this, context);
        }

        public void Visit(IBinaryExpression expr, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(expr.LeftOperand, expr);
            context.Add(expr.RightOperand, expr);
            expr.LeftOperand.Accept(this, context);
            expr.RightOperand.Accept(this, context);
        }

        public void Visit(IEventReference eventRef, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(eventRef.Reference,eventRef);
        }

        public void Visit(IFieldReference fieldRef, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(fieldRef.Reference, fieldRef);
        }

        public void Visit(IMethodReference methodRef, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(methodRef.Reference, methodRef);
        }

        public void Visit(IPropertyReference propertyRef, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(propertyRef.Reference, propertyRef);
        }

        public void Visit(ITypeReference typeReference, Dictionary<ISSTNode, ISSTNode> context)
        {
        }

        public void Visit(IVariableReference varRef, Dictionary<ISSTNode, ISSTNode> context)
        {
        }

        public void Visit(ISimpleName simpleName, Dictionary<ISSTNode, ISSTNode> context)
        {
        }

        public void Visit(IIndexAccessReference indexAccessRef, Dictionary<ISSTNode, ISSTNode> context)
        {
            context.Add(indexAccessRef.Expression, indexAccessRef);
            indexAccessRef.Expression.Accept(this, context);
        }

        public void Visit(IUnknownReference unknownRef, Dictionary<ISSTNode, ISSTNode> context)
        {
        }

        public void Visit(IUnknownExpression unknownExpr, Dictionary<ISSTNode, ISSTNode> context)
        {
        }

        public void Visit(IUnknownStatement unknownStmt, Dictionary<ISSTNode, ISSTNode> context)
        {
        }

    }
}
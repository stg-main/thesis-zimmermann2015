﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using KaVE.Commons.Model.SSTs;
using KaVE.Commons.Model.SSTs.Blocks;
using KaVE.Commons.Model.SSTs.Declarations;
using KaVE.Commons.Model.SSTs.Expressions.Assignable;
using KaVE.Commons.Model.SSTs.Expressions.LoopHeader;
using KaVE.Commons.Model.SSTs.Expressions.Simple;
using KaVE.Commons.Model.SSTs.References;
using KaVE.Commons.Model.SSTs.Statements;
using KaVE.Commons.Model.SSTs.Visitor;
using KaVE.Commons.Utils;

namespace SSTDiff.Util.Visitor.NodeUtil
{
    public class PrintVisitor : ISSTNodeVisitor<object, string>
    {
        public string Visit(ISST sst, object context)
        {
            return "SST Root";
        }

        public string Visit(IDelegateDeclaration decl, object context)
        {
            var delegateTypeName = decl.Name;
            return string.Format("{0} {1} Parameters: {2}", delegateTypeName.ReturnType, delegateTypeName.Name, delegateTypeName.Parameters.ToStringReflection());
        }

        public string Visit(IEventDeclaration decl, object context)
        {
            var eventName = decl.Name;
            return string.Format("{0} {1}", eventName.HandlerType, eventName.Name);
        }

        public string Visit(IFieldDeclaration decl, object context)
        {
            var fieldName = decl.Name;
            return string.Format("{0} {1}", fieldName.ValueType, fieldName.Name);
        }

        public string Visit(IMethodDeclaration decl, object context)
        {
            var methodName = decl.Name;
            return string.Format("{0} {1} Parameters: {2}", methodName.ReturnType, methodName.Name, methodName.Parameters.ToStringReflection());
        }

        public string Visit(IParameterDeclaration decl, object context)
        {
            return string.Format("{0} {1}", decl.Type.Accept(this, null), decl.Name.Accept(this, null));
        }

        public string Visit(IPropertyDeclaration decl, object context)
        {
            var propertyName = decl.Name;
            return string.Format("{0} {1}", propertyName.ValueType, propertyName.Name);
        }

        public string Visit(IVariableDeclaration decl, object context)
        {
            return string.Format("{0} {1}",decl.Type.Name,decl.Reference.Identifier);
        }

        public string Visit(IAssignment stmt, object context)
        {
            return string.Format("{0} = {1}",stmt.Reference.Accept(this,null),stmt.Expression.Accept(this,null));
        }

        public string Visit(IBreakStatement stmt, object context)
        {
            return "Break";
        }

        public string Visit(IContinueStatement stmt, object context)
        {
            return "Contine";
        }

        public string Visit(IEventSubscriptionStatement stmt, object context)
        {
            return string.Format("{0} {1} {2}", stmt.Reference.Accept(this, null), stmt.Operation ,stmt.Expression.Accept(this, null));
        }

        public string Visit(IExpressionStatement stmt, object context)
        {
            return string.Format("ExpressionStatement: {0}", stmt.Expression.Accept(this, null));
        }

        public string Visit(IGotoStatement stmt, object context)
        {
            return "Goto " + stmt.Label;
        }

        public string Visit(ILabelledStatement stmt, object context)
        {
            return stmt.Label + stmt.Statement.Accept(this,null);
        }

        public string Visit(IReturnStatement stmt, object context)
        {
            return "Return " + stmt.Expression.Accept(this,null);
        }

        public string Visit(IThrowStatement stmt, object context)
        {
            return "Throw " + stmt.Reference.Identifier;
        }

        public string Visit(IDoLoop block, object context)
        {
            return "Do Loop";
        }

        public string Visit(IForEachLoop block, object context)
        {
            return "ForEach Loop";
        }

        public string Visit(IForLoop block, object context)
        {
            return "For Loop";
        }

        public string Visit(IIfElseBlock block, object context)
        {
            return "If Else Block";
        }

        public string Visit(ILockBlock block, object context)
        {
            return "Lock Block";
        }

        public string Visit(ISwitchBlock block, object context)
        {
            return "Switch Block";
        }

        public string Visit(ITryBlock block, object context)
        {
            return "Try Block";
        }

        public string Visit(IUncheckedBlock block, object context)
        {
            return "Unchecked Block";
        }

        public string Visit(IUnsafeBlock block, object context)
        {
            return "Unsafe Block";
        }

        public string Visit(IUsingBlock block, object context)
        {
            return "Using Block";
        }

        public string Visit(IWhileLoop block, object context)
        {
            return "While Loop";
        }

        public string Visit(ICompletionExpression expr, object context)
        {
            return expr.TypeReference + " " + expr.Token;
        }

        public string Visit(IComposedExpression expr, object context)
        {
            return "Composed Expression";
        }

        public string Visit(IIfElseExpression expr, object context)
        {
            return "IIf Else Expression";
        }

        public string Visit(IInvocationExpression expr, object context)
        {
            return "Invocation of " + expr.MethodName;
        }

        public string Visit(ILambdaExpression expr, object context)
        {
            return "Lambda Expression " + expr.Name.ToString();
        }

        public string Visit(ILoopHeaderBlockExpression expr, object context)
        {
            return "Loop Header Block Expression";
        }

        public string Visit(IConstantValueExpression expr, object context)
        {
            return "Constant Value: " + expr.Value;
        }

        public string Visit(INullExpression expr, object context)
        {
            return "Null";
        }

        public string Visit(IReferenceExpression expr, object context)
        {
            return "Reference Expression: " + expr.Reference.Accept(this,null);
        }

        public string Visit(ICastExpression expr, object context)
        {
            return "Cast " + expr.TargetType + expr.Reference.Accept(this, null);
        }

        public string Visit(IIndexAccessExpression expr, object context)
        {
            return "IndexAccessExpression";
        }

        public string Visit(ITypeCheckExpression expr, object context)
        {
            return "TypeCheck " + expr.Type + expr.Reference.Accept(this, null);
        }

        public string Visit(IUnaryExpression expr, object context)
        {
            return string.Format("{0} {1}", expr.Operand.Accept(this, null), expr.Operator);
        }

        public string Visit(IBinaryExpression expr, object context)
        {
            return string.Format("{0} {1} {2}", expr.LeftOperand.Accept(this, null), expr.Operator, expr.RightOperand.Accept(this, null));
        }

        public string Visit(IEventReference eventRef, object context)
        {
            var eventName = eventRef.EventName;
            return string.Format("Event Reference: {0} {1}", eventName.HandlerType, eventName.Name);
        }

        public string Visit(IFieldReference fieldRef, object context)
        {
            var fieldName = fieldRef.FieldName;
            return string.Format("Field Reference: {0} {1}", fieldName.ValueType, fieldName.Name);
        }

        public string Visit(IMethodReference methodRef, object context)
        {
            var methodName = methodRef.MethodName;
            return string.Format("Method Reference: {0} {1} Parameters: {2}", methodName.ReturnType, methodName.Name, methodName.Parameters.ToStringReflection());
        }

        public string Visit(IPropertyReference propertyRef, object context)
        {
            var propertyName = propertyRef.PropertyName;
            return string.Format("Property Reference: {0} {1}", propertyName.ValueType, propertyName.Name);
        }

        public string Visit(ITypeReference typeReference, object context)
        {
            return typeReference.TypeName.Name;
        }

        public string Visit(IVariableReference varRef, object context)
        {
            return "Variable Reference: " + varRef.Identifier;
        }

        public string Visit(IIndexAccessReference indexAccessRef, object context)
        {
            return "IndexAccessReference";
        }
        
        public string Visit(ISimpleName simpleName, object context)
        {
            return simpleName.Name;
        }

        public string Visit(IUnknownReference unknownRef, object context)
        {
            return "Unknown Reference";
        }

        public string Visit(IUnknownExpression unknownExpr, object context)
        {
            return "Unknown Expression";
        }

        public string Visit(IUnknownStatement unknownStmt, object context)
        {
            return "Unknown Statement";
        }
    }
}
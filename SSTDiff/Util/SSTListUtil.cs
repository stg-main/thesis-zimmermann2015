﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
using System.Collections.Generic;
using System.Linq;
using KaVE.Commons.Model.SSTs.Visitor;
using KaVE.Commons.Utils.Collections;
using SSTDiff.Util.Visitor.Traversal;

namespace SSTDiff.Util
{
    public static class SSTListUtil
    {
        public static List<ISSTNode> GetLeaves(this ISSTNode root, List<ISSTNode> fromList = null )
        {
            var allNodes = fromList ?? root.GetAllNodes();
            var leaves = allNodes.Where(IsLeaf).ToList();
            return leaves;
        }

        public static List<ISSTNode> GetAllNodes(this ISSTNode root)
        {
            List<ISSTNode> nodesList = new KaVEList<ISSTNode>();
            root.Accept(new NodesListVisitor(), nodesList);
            return nodesList;
        }

        public static List<ISSTNode> GetPostOrderNodes(this ISSTNode root)
        {
            List<ISSTNode> nodesList = new KaVEList<ISSTNode>();
            root.Accept(new PostOrderListVisitor(), nodesList);
            return nodesList;
        }

        public static List<ISSTNode> GetBreadthFirstOrderNodes(this ISSTNode root)
        {
            var nodesList = new KaVEList<ISSTNode>();

            var queue = new Queue<ISSTNode>();

            queue.Enqueue(root);
            nodesList.Add(root);

            while (queue.Count != 0)
            {
                var node = queue.Dequeue();

                foreach (var sstNode in node.GetChildren())
                {
                    nodesList.Add(sstNode);
                    queue.Enqueue(sstNode);
                }
            }
            
            return nodesList;
        }

        public static Dictionary<ISSTNode, bool> CreateInOrderDictionary(ISSTNode root)
        {
            return root.GetAllNodes()
                .ToDictionary(node => node, node => false, ObjectReferenceEqualityComparer<ISSTNode>.Default);
        }

        public static bool IsLeaf(ISSTNode node)
        {
            return node.GetChildren().Count == 0;
        }
    }
}
﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using KaVE.Commons.Model.SSTs.Impl;
using KaVE.Commons.Model.SSTs.Impl.Declarations;
using KaVE.Commons.Model.SSTs.Impl.Expressions.Assignable;
using KaVE.Commons.Model.SSTs.Visitor;
using KaVE.Commons.Utils.Collections;
using KaVE.Commons.Utils.Json;
using KaVE.Commons.Utils.SSTPrinter;
using SSTDiff.EditScript.EditOperations;
using SSTDiff.Util.DiffPrinter;
using SSTDiff.Util.Visitor.NodeUtil;
using static System.String;

namespace SSTDiff.Util
{
    public static class SSTNodeUtil
    {
        public static Dictionary<ISSTNode, ISSTNode> ParentCacheTree1;
        public static Dictionary<ISSTNode, ISSTNode> ParentCacheTree2;

        public enum TreeSelection
        {
            None,
            Tree1,
            Tree2
        }

        public static List<ISSTNode> GetChildren(this ISSTNode node)
        {
            var childrenListVisitor = new ChildrenListVisitor();

            var children = node.Accept(childrenListVisitor, null);

            return children;
        }

        public static ISSTNode GetParent(this ISSTNode node, ISSTNode root, bool cacheActivation, TreeSelection selectedTree = TreeSelection.None)
        {
            if (cacheActivation)
            {
                switch (selectedTree)
                {
                    case TreeSelection.Tree1:
                        if(ParentCacheTree1.ContainsKey(node)) return ParentCacheTree1[node];
                        break;
                    case TreeSelection.Tree2:
                        if(ParentCacheTree2.ContainsKey(node)) return ParentCacheTree2[node];
                        break;
                    case TreeSelection.None:
                        break;
                }
            }

            var nodeList = root.GetBreadthFirstOrderNodes();
            return nodeList.FirstOrDefault(sstNode => sstNode.GetChildren().Contains(node, new ObjectReferenceEqualityComparer<ISSTNode>()));
        }

        public static string GetValue(this ISSTNode node, bool isUpdateCall = false)
        {
            return node.Accept(new ValueAsStringVisitor(), isUpdateCall);
        }

        public static string GetPrintValue(this ISSTNode node)
        {
            return node.Accept(new PrintVisitor(), null);
        }

        public static string GetOldValueString(this ISSTNode node)
        {
            return node.Accept(new OldValuePrintVisitor(), null);
        }

        public static ISSTNode GetOverwrittenInnerNode(this ISSTNode node, ISSTNode insertedNode, int position)
        {
            return node.Accept(new OverwritesNodeVisitor(position), insertedNode);
        }

        public static bool IsOverwrittenNode(this ISSTNode node, ISSTNode parent, int position)
        {
            return parent.Accept(new OverwritesNodeVisitor(position), node) != null;
        }

        public static string GetDiffPrinterValue(this ISSTNode node, List<IEditOperation> editscript, Settings settings)
        {
            if (node is SST)
            {
                return DiffPrinterValueForSST((SST) node);
            }
            if (node is LambdaExpression)
            {
                return DiffPrinterValueForLambdaExpression((LambdaExpression) node);
            }
            if (!IsNewDeclarationStyle(node))
            {
                var xamlPrintingContext = new XamlDiffPrintingContext(editscript, settings);
                node.Accept(new DiffPrintingVisitor(null), xamlPrintingContext);
                return xamlPrintingContext.ToString();
            }
            return node.GetValue();
        }

        private static string DiffPrinterValueForLambdaExpression(LambdaExpression node)
        {
            var xamlPrintingContext = new XamlSSTPrintingContext();
            xamlPrintingContext.ParameterList(node.Name.Parameters).Space().Text("=>");
            return xamlPrintingContext.ToString();
        }

        private static string DiffPrinterValueForSST(SST node)
        {
            var xamlPrintingContext = new XamlSSTPrintingContext();
            xamlPrintingContext.Type(node.EnclosingType);
            return xamlPrintingContext.ToString();
        }

        private static bool IsNewDeclarationStyle(ISSTNode node)
        {
            return node is MethodDeclaration || node is FieldDeclaration || node is ParameterDeclaration;
        }

        public static ISSTNode CloneNode(this ISSTNode node)
        {
            return node.Accept(new CloneVisitor(), null);
        }

        public static ISSTNode DeepCloneNode(this ISSTNode node)
        {
            var json = node.ToCompactJson();
            return json.ParseJsonTo<ISSTNode>();
        }

        public static void SafeInsert<T>(this IKaVEList<T> childrenList, int position, T node)
        {
            if (childrenList.Count <= position)
            {
                childrenList.Add(node);
            }
            else if (position < 0) // Failsafe if position calculation is wrong
            {
                childrenList.Insert(0, node);
            }
            else
            {
                childrenList.Insert(position, node);
            }
        }

        public static void ResetCache()
        {
            if (ParentCacheTree1 != null) ParentCacheTree1.Clear();
            if (ParentCacheTree2 != null) ParentCacheTree2.Clear();
        }
    }
}
﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Linq;
using KaVE.Commons.Model.Names;
using KaVE.Commons.Model.Names.CSharp;
using KaVE.Commons.Utils.Collections;

namespace SSTDiff.Util
{
    public static class StringSimilarity
    {
        public static int N = 2;

        public static double CalculateSimilarity(this string a, string b)
        {
            if (a.Equals(b)) return 1.0;

            var prefixA = GetPrefix(a);

            switch (prefixA)
            {
                case "CSharp.DelegateTypeName":
                    return CalculateSimilarity(DelegateTypeName.Get(RemovePrefix(a)),
                        DelegateTypeName.Get(RemovePrefix(b)));
                case "CSharp.EventName":
                    return CalculateSimilarity(EventName.Get(RemovePrefix(a)),
                        EventName.Get(RemovePrefix(b)));
                case "CSharp.FieldName":
                    return CalculateSimilarity(FieldName.Get(RemovePrefix(a)),
                        FieldName.Get(RemovePrefix(b)));
                case "CSharp.MethodName":
                    return CalculateSimilarity(MethodName.Get(RemovePrefix(a)),
                        MethodName.Get(RemovePrefix(b)));
                case "CSharp.PropertyName":
                    return CalculateSimilarity(PropertyName.Get(RemovePrefix(a)),
                        PropertyName.Get(RemovePrefix(b)));
                default:
                    return CalculateNGramsSimilarity(a, b);
            }
        }

        private static string GetPrefix(string s)
        {
            return s.Split(':')[0];
        }

        private static string RemovePrefix(string s)
        {
            // ReSharper disable once StringIndexOfIsCultureSpecific.1
            var removeDelegateTypeNamePrefix = s.Remove(0, s.IndexOf(":") + 1);
            return removeDelegateTypeNamePrefix;
        }

        private static double CalculateSimilarity(this IDelegateTypeName a, IDelegateTypeName b)
        {
            var sim = a.Name.CalculateNGramsSimilarity(b.Name);
            sim += a.ReturnType.Name.CalculateNGramsSimilarity(b.ReturnType.Name);

            // Parameter special case
            if (a.HasParameters || b.HasParameters)
            {
                if (!a.Parameters.Count.Equals(b.Parameters.Count)) return 0.0;
                var parameterCount = a.Parameters.Count;
                for (var i = 0; i < parameterCount; i++)
                {
                    sim += a.Parameters[i].Name.CalculateNGramsSimilarity(b.Parameters[i].Name);
                }
                return sim/(2 + parameterCount);
            }

            return sim/2;
        }

        private static double CalculateSimilarity(this IEventName a, IEventName b)
        {
            var sim = a.Name.CalculateNGramsSimilarity(b.Name);
            sim += a.HandlerType.Name.CalculateNGramsSimilarity(b.HandlerType.Name);

            return sim/2;
        }

        private static double CalculateSimilarity(this IFieldName a, IFieldName b)
        {
            var sim = a.Name.CalculateNGramsSimilarity(b.Name);
            sim += a.ValueType.Name.CalculateNGramsSimilarity(b.ValueType.Name);

            return sim/2;
        }

        private static double CalculateSimilarity(this IMethodName a, IMethodName b)
        {
            double sim;
            int averageCount = 0;
            if (a.IsConstructor && b.IsConstructor)
            {
                sim = a.DeclaringType.Name.CalculateNGramsSimilarity(b.DeclaringType.Name);
                averageCount++;
            }
            else
            {
                sim = a.Name.CalculateNGramsSimilarity(b.Name);
                sim += a.ReturnType.Name.CalculateNGramsSimilarity(b.ReturnType.Name);
                averageCount = 2;
            }

            // Parameter special case
            if (a.HasParameters || b.HasParameters)
            {
                if (!a.Parameters.Count.Equals(b.Parameters.Count)) return 0.0;
                var parameterCount = a.Parameters.Count;
                for (var i = 0; i < parameterCount; i++)
                {
                    sim += a.Parameters[i].Name.CalculateNGramsSimilarity(b.Parameters[i].Name);
                }
                return sim/(averageCount + parameterCount);
            }

            return sim/averageCount;
        }

        private static double CalculateSimilarity(this IPropertyName a, IPropertyName b)
        {
            var sim = a.Name.CalculateNGramsSimilarity(b.Name);
            sim += a.ValueType.Name.CalculateNGramsSimilarity(b.ValueType.Name);

            return sim/2;
        }

        internal static double CalculateNGramsSimilarity(this string a, string b)
        {
            if(a.Equals(b)) return 1.0;

            var gramsA = SlidingWindow(a);
            var gramsB = SlidingWindow(b);

            var gramsIntersection = gramsA.Intersect(gramsB);
            var gramsUnion = gramsA.Concat(gramsB);

            var gramsIntersectionCount = gramsIntersection.Count();
            var gramsUnionCount = gramsUnion.Count();

            double similiarity;

            if (BothStringsAreEmpty(gramsIntersectionCount, gramsUnionCount)) similiarity = 1.0;
            else similiarity = 2.0*gramsIntersectionCount/gramsUnionCount;

            return similiarity;
        }

        private static bool BothStringsAreEmpty(int gramsIntersectionCount, int gramsUnionCount)
        {
            return gramsIntersectionCount == 0 && gramsUnionCount == 0;
        }

        internal static KaVEHashSet<string> SlidingWindow(string s)
        {
            var resultSet = new KaVEHashSet<string>();

            var charArray = s.ToCharArray().ToList();

            for (var i = 0; i <= charArray.Count - N; i++)
            {
                var gram = charArray.GetRange(i, N);
                resultSet.Add(string.Concat(gram).ToLower());
            }

            return resultSet;
        }
    }
}
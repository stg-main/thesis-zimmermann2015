﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
using KaVE.Commons.Model.SSTs.Visitor;
using SSTDiff.Util;
using SSTDiff.Util.Visitor.EditOperations;

namespace SSTDiff.EditScript.EditOperations
{
    public interface IUpdate : IEditOperation
    {
        ISSTNode UpdatedNode { get; set; }
        ISSTNode NewNode { get; set; }
        string NewValue { get; set; }
        string OldValue { get; set; }
    }

    public class Update : IUpdate
    {
        public ISSTNode UpdatedNode { get; set; }
        public ISSTNode UpdatedNodeBackup { get; set; }

        public ISSTNode NewNode { get; set; }

        public string NewValue { get; set; }
        public string OldValue { get; set; }

        protected bool Equals(Update other)
        {
            return Equals(UpdatedNode, other.UpdatedNode) && Equals(NewNode, other.NewNode) && string.Equals(NewValue, other.NewValue) && string.Equals(OldValue, other.OldValue);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Update) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (UpdatedNode != null ? UpdatedNode.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (NewNode != null ? NewNode.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (NewValue != null ? NewValue.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (OldValue != null ? OldValue.GetHashCode() : 0);
                return hashCode;
            }
        }

        public void Apply(ISSTNode root, EditScriptAlgorithm editScriptAlgorithm)
        {
            // TODO: Test for Update.Apply
            UpdatedNode.Accept(new UpdateVisitor(), NewNode);
        }

        public EditOperationEnum EditOperation
        {
            get
            {
                return EditOperationEnum.Update;
            }
        }

        public bool ContainsNode(ISSTNode node)
        {
            return ReferenceEquals(node, UpdatedNode);
        }

        public override string ToString()
        {
            return string.Format("Update: [UpdatedNode: {0}, NewValue: {1}]", UpdatedNode.GetPrintValue(), NewValue);
        }
    }
}
﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
using KaVE.Commons.Model.SSTs.Visitor;
using SSTDiff.Util;
using SSTDiff.Util.Visitor.EditOperations;

namespace SSTDiff.EditScript.EditOperations
{
    public interface IDelete : IEditOperation
    {
        ISSTNode DeletedNode { get; set; }
        ISSTNode ParentNode { get; set; }
        int ChildPosition { get; set; }
    }

    public class Delete : IDelete
    {
        public ISSTNode DeletedNode { get; set; }
        public ISSTNode ParentNode { get; set; }
        public int ChildPosition { get; set; }

        protected bool Equals(Delete other)
        {
            return Equals(DeletedNode, other.DeletedNode) && Equals(ParentNode, other.ParentNode) && ChildPosition == other.ChildPosition;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Delete) obj);
        }

        public override int GetHashCode()
        {
            var hashCode = (DeletedNode != null ? DeletedNode.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ (ParentNode != null ? ParentNode.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ ChildPosition;
            return hashCode;
        }

        public void Apply(ISSTNode root, EditScriptAlgorithm editScriptAlgorithm)
        {
            // TODO: Test for Delete.Apply
            var parentDeletedNode = DeletedNode.GetParent(root,true, SSTNodeUtil.TreeSelection.Tree1);
            parentDeletedNode.Accept(new DeletionVisitor(), DeletedNode);
        }

        public bool ContainsNode(ISSTNode node)
        {
            return ReferenceEquals(node, DeletedNode);
        }

        public EditOperationEnum EditOperation
        {
            get
            {
                return EditOperationEnum.Delete;
            }
        }

        public override string ToString()
        {
            return string.Format("Delete : [DeletedNode: {0}, ParentNode: {1}, ChildPosition: {2}]", DeletedNode.GetPrintValue(),
                ParentNode.GetPrintValue(), ChildPosition);
        }
    }
}
﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using KaVE.Commons.Model.SSTs.Visitor;
using SSTDiff.Util;
using SSTDiff.Util.Visitor.EditOperations;

namespace SSTDiff.EditScript.EditOperations
{
    public interface IMove : IEditOperation
    {
        ISSTNode MovedNode { get; set; }
        ISSTNode ParentNode { get; set; }
        ISSTNode OldParentNode { get; set; }
        int ChildPosition { get; set; }
        int OldChildPosition { get; set; }
    }

    public class Move : IMove
    {
        public ISSTNode MovedNode { get; set; }

        public ISSTNode ParentNode { get; set; }

        public ISSTNode OldParentNode { get; set; }

        public int ChildPosition { get; set; }
        public int OldChildPosition { get; set; }

        public ISSTNode NewlyInsertedNode { get; set; }

        protected bool Equals(Move other)
        {
            return Equals(MovedNode, other.MovedNode) && Equals(ParentNode, other.ParentNode) &&
                   Equals(OldParentNode, other.OldParentNode) && ChildPosition == other.ChildPosition && OldChildPosition == other.OldChildPosition;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Move) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (MovedNode != null ? MovedNode.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (ParentNode != null ? ParentNode.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (OldParentNode != null ? OldParentNode.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ ChildPosition;
                hashCode = (hashCode*397) ^ OldChildPosition;
                return hashCode;
            }
        }

        public void Apply(ISSTNode root, EditScriptAlgorithm editScriptAlgorithm)
        {
            OldParentNode.Accept(new DeletionVisitor(), MovedNode);
            int childPosition;
            if (ReferenceEquals(OldParentNode, ParentNode) && ChildPosition > OldChildPosition)
            {
                childPosition = ChildPosition - 1;
            }
            else
            {
                childPosition = ChildPosition;

            }
            ParentNode.Accept(new InsertionVisitor(editScriptAlgorithm), new Insert {ChildPosition = childPosition, ParentNode = ParentNode, InsertedNode = MovedNode});

            // Update ParentNode Cache
            SSTNodeUtil.ParentCacheTree1[MovedNode] = ParentNode;
            // TODO: Test for Move.Apply  
        }

        public bool ContainsNode(ISSTNode node)
        {
            return ReferenceEquals(node, MovedNode) || ReferenceEquals(node, NewlyInsertedNode);
        }

        public EditOperationEnum EditOperation
        {
            get
            {
                return EditOperationEnum.Move;
            }
        }

        public override string ToString()
        {
            return string.Format("Move: [MovedNode: {0}, ParentNode: {1}, OldParentNode: {2}, ChildPosition: {3}, OldChildPosition: {4}]",
                MovedNode.GetPrintValue(), ParentNode.GetPrintValue(), OldParentNode.GetPrintValue(),
                ChildPosition, OldChildPosition);
        }
    }
}
﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
using KaVE.Commons.Model.SSTs.Visitor;
using SSTDiff.Util;
using SSTDiff.Util.Visitor.EditOperations;

namespace SSTDiff.EditScript.EditOperations
{
    public interface IInsert : IEditOperation
    {
        ISSTNode InsertedNode { get; set; }
        ISSTNode ParentNode { get; set; }
        int ChildPosition { get; set; }
    }

    public class Insert : IInsert
    {
        public ISSTNode InsertedNode { get; set; }

        public ISSTNode ParentNode { get; set; }

        public int ChildPosition { get; set; }

        protected bool Equals(Insert other)
        {
            return Equals(InsertedNode, other.InsertedNode) && Equals(ParentNode, other.ParentNode) &&
                   ChildPosition == other.ChildPosition;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((Insert) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (InsertedNode != null ? InsertedNode.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (ParentNode != null ? ParentNode.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ ChildPosition;
                return hashCode;
            }
        }

        public void Apply(ISSTNode root, EditScriptAlgorithm editScriptAlgorithm)
        {
            // Update ParentNode Cache
            SSTNodeUtil.ParentCacheTree1.Add(InsertedNode, ParentNode);

            // TODO: Test for Insert.Apply
            ParentNode.Accept(new InsertionVisitor(editScriptAlgorithm), this);
        }

        public EditOperationEnum EditOperation
        {
            get
            {
                return EditOperationEnum.Insert;
            }
        }

        public bool ContainsNode(ISSTNode node)
        {
            return ReferenceEquals(node, InsertedNode);
        }

        public override string ToString()
        {
            return string.Format("Insert : [InsertedNode: {0}, ParentNode: {1}, ChildPosition: {2}]", InsertedNode.GetPrintValue(),
                ParentNode.GetPrintValue(), ChildPosition);
        }
    }
}
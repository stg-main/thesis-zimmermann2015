﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using KaVE.Commons.Model.SSTs;
using KaVE.Commons.Model.SSTs.Declarations;
using KaVE.Commons.Model.SSTs.Impl;
using KaVE.Commons.Model.SSTs.Visitor;
using KaVE.Commons.Utils.Collections;
using KaVE.JetBrains.Annotations;
using SSTDiff.EditScript.EditOperations;
using SSTDiff.Matching;
using SSTDiff.Util;

namespace SSTDiff.EditScript
{
    public class EditScriptAlgorithm
    {
        private const int Up = 1;
        private const int Left = 2;
        private const int Diag = 3;

        public readonly HashSet<Match> Matches;

        public readonly Dictionary<ISSTNode, ISSTNode> LeftToRightMatch;
        public readonly Dictionary<ISSTNode, ISSTNode> RightToLeftMatch; 
        
        private Dictionary<ISSTNode, bool> _inOrderMapTree1;
        private Dictionary<ISSTNode, bool> _inOrderMapTree2;
        public ISSTNode RootT1;
        public ISSTNode RootT2;

        public readonly Dictionary<ISSTNode, ISSTNode> MoveClonesFromTree1;
        public readonly Dictionary<ISSTNode, ISSTNode> MoveClonesForReinsertion;
        private readonly Dictionary<ISSTNode, ISSTNode> _overrideNodeBackup;
        public KaVEList<IEditOperation> EditScript;

        public EditScriptAlgorithm(List<Match> matches, Dictionary<ISSTNode, bool> inOrderMapTree1 = null,
            Dictionary<ISSTNode, bool> inOrderMapTree2 = null)
        {
            Matches = new HashSet<Match>(ObjectReferenceEqualityComparer<Match>.Default);
            LeftToRightMatch = new Dictionary<ISSTNode, ISSTNode>(ObjectReferenceEqualityComparer<ISSTNode>.Default);
            RightToLeftMatch = new Dictionary<ISSTNode, ISSTNode>(ObjectReferenceEqualityComparer<ISSTNode>.Default);
            foreach (var match in matches)
            {
                Matches.Add(match);
                LeftToRightMatch.Add(match.NodeA,match.NodeB);
                RightToLeftMatch.Add(match.NodeB,match.NodeA);
            }

            _inOrderMapTree1 = inOrderMapTree1;
            _inOrderMapTree2 = inOrderMapTree2;
            MoveClonesFromTree1 = new Dictionary<ISSTNode, ISSTNode>(ObjectReferenceEqualityComparer<ISSTNode>.Default);
            MoveClonesForReinsertion = new Dictionary<ISSTNode, ISSTNode>(ObjectReferenceEqualityComparer<ISSTNode>.Default);
            _overrideNodeBackup = new Dictionary<ISSTNode, ISSTNode>(ObjectReferenceEqualityComparer<ISSTNode>.Default);
        }

        public List<IEditOperation> CreateEditScript(ISSTNode root1, ISSTNode root2)
        {
            RootT1 = root1;
            RootT2 = root2;
            _inOrderMapTree1 = _inOrderMapTree1 ?? SSTListUtil.CreateInOrderDictionary(root1);
            _inOrderMapTree2 = _inOrderMapTree2 ?? SSTListUtil.CreateInOrderDictionary(root2);

            EditScript = new KaVEList<IEditOperation>();

            var tree2Nodes = root2.GetBreadthFirstOrderNodes();

            foreach (var currentNode in tree2Nodes)
            {
                // Does not skip root node anymore!

                // Partners in T1
                ISSTNode partnerCurrentNode;
                RightToLeftMatch.TryGetValue(currentNode,out partnerCurrentNode);

                // Special case when both SSTs are equal
                if (currentNode is SST && partnerCurrentNode is SST && partnerCurrentNode.GetHashCode() == currentNode.GetHashCode())
                {
                    break;
                }

                var parentCurrentNode = currentNode.GetParent(root2,true, SSTNodeUtil.TreeSelection.Tree2);
                ISSTNode partnerParentCurrentNode = null;
                if (parentCurrentNode != null)
                {
                    RightToLeftMatch.TryGetValue(parentCurrentNode, out partnerParentCurrentNode);
                }
                
                if (partnerCurrentNode == null)
                {
                    /* Insert Phase */

                    var position = FindPosition(currentNode, root1, root2);

                    var clone = currentNode.CloneNode();

                    var overwrittenInnerNode = partnerParentCurrentNode.GetOverwrittenInnerNode(clone, position);
                    if (overwrittenInnerNode != null && !EditScript.OfType<Insert>().Any(operation => ReferenceEquals(operation.InsertedNode,partnerParentCurrentNode)))
                    {
                        _overrideNodeBackup.Add(clone, overwrittenInnerNode);
                    }

                    partnerCurrentNode = clone;

                    // caches parent node before first change
                    if (!MoveClonesFromTree1.ContainsKey(partnerParentCurrentNode))
                    {
                        MoveClonesFromTree1.Add(partnerParentCurrentNode, partnerParentCurrentNode.DeepCloneNode());
                    }

                    IEditOperation insert = new Insert
                    {
                        InsertedNode = clone,
                        ParentNode = partnerParentCurrentNode,
                        ChildPosition = position
                    };
                    
                    EditScript.Add(insert);
                    var match = Match.Create(clone, currentNode);
                    Matches.Add(match);
                    LeftToRightMatch.Add(match.NodeA, match.NodeB);
                    RightToLeftMatch.Add(match.NodeB, match.NodeA);

                    _inOrderMapTree1.Add(clone, true);

                    // Addition to regular algorithm - declares inserted node in order
                    MarkInOrder(currentNode,_inOrderMapTree2);
                    
                    insert.Apply(root1, this);
                }
                else
                {
                    var parentPartnerCurrentNode = partnerCurrentNode.GetParent(root1,true, SSTNodeUtil.TreeSelection.Tree1);

                    /* Update Phase */
                    var newValue = currentNode.GetValue(true);
                    var oldValue = partnerCurrentNode.GetValue(true);
                    var differentValues = !newValue.Equals(oldValue);
                    if (differentValues)
                    {
                        IEditOperation update = new Update
                        {
                            UpdatedNode = partnerCurrentNode,
                            UpdatedNodeBackup = partnerCurrentNode.DeepCloneNode(),
                            NewValue = newValue,
                            NewNode = currentNode,
                            OldValue = partnerCurrentNode.GetOldValueString()
                        };

                        EditScript.Add(update);
                        update.Apply(root1,this);
                    }

                    /* Move Phase */
                    if (parentCurrentNode != null && parentPartnerCurrentNode != null &&
                        !MatchingContainsNodes(parentPartnerCurrentNode, parentCurrentNode))
                    {
                        var position = FindPosition(currentNode, root1, root2);

                        if (!MoveClonesFromTree1.ContainsKey(parentPartnerCurrentNode))
                        {
                            MoveClonesFromTree1.Add(parentPartnerCurrentNode, parentPartnerCurrentNode.DeepCloneNode());
                        }

                        MoveClonesForReinsertion.Add(partnerCurrentNode, partnerCurrentNode.DeepCloneNode());

                        // Addition to regular algorithm - calculates old child position from cached tree 1 nodes (caches after first change)
                        var oldChildPosition = MoveClonesFromTree1[parentPartnerCurrentNode].GetChildren().IndexOf(partnerCurrentNode);
                        if (oldChildPosition < 0) oldChildPosition = parentPartnerCurrentNode.GetChildren().IndexOf(partnerCurrentNode);

                        IEditOperation move = new Move
                        {
                            ChildPosition = position,
                            ParentNode = partnerParentCurrentNode,
                            MovedNode = partnerCurrentNode,
                            OldParentNode = parentPartnerCurrentNode,
                            OldChildPosition = oldChildPosition
                        };

                        EditScript.Add(move);

                        // Addition to regular algorithm - declares moved nodes in order
                        MarkInOrder(partnerCurrentNode, _inOrderMapTree1);
                        MarkInOrder(currentNode, _inOrderMapTree2);


                        move.Apply(RootT1, this);
                    }
                }

                var partnerCurrentNodeIsNotALeaf = partnerCurrentNode.GetChildren().Count != 0;
                if (partnerCurrentNodeIsNotALeaf)
                    AlignChildren(partnerCurrentNode, currentNode, EditScript);
            }


            // Delete Phase
            var postOrderTree1 = root1.GetPostOrderNodes();

            foreach (var currentNode in postOrderTree1)
            {
                ISSTNode partnerCurrentNode;
                LeftToRightMatch.TryGetValue(currentNode, out partnerCurrentNode);
                if (partnerCurrentNode == null && !(currentNode is SST))
                {
                    var parentNode = currentNode.GetParent(root1, true, SSTNodeUtil.TreeSelection.Tree1);
                    var position = parentNode.GetChildren().FindIndex(node => ReferenceEquals(node, currentNode));

                    IEditOperation delete = new Delete
                    {
                        DeletedNode = currentNode,
                        ChildPosition = position,
                        ParentNode = parentNode
                    };

                    EditScript.Add(delete);

                    // Apply of delete deactivated - No deletion so T1 can be used as result of unified Edit Script print 
                    //delete.Apply(root1);
                }
                else if (_overrideNodeBackup.ContainsKey(currentNode))
                {
                    var deletedNode = _overrideNodeBackup[currentNode];
                    if(IsMovedNode(deletedNode)) continue;

                    var parentNode = currentNode.GetParent(root1, true, SSTNodeUtil.TreeSelection.Tree1);
                    var position = parentNode.GetChildren().FindIndex(node => ReferenceEquals(node, currentNode));

                    IEditOperation delete = new Delete
                    {
                        ChildPosition = position,
                        DeletedNode = deletedNode,
                        ParentNode = parentNode
                    };

                    EditScript.Add(delete);
                }
            }

            return EditScript;
        }

        private void AlignChildren(ISSTNode nodeT1, ISSTNode nodeT2, List<IEditOperation> editScript)
        {
            MarkChildrenOutOfOrder(nodeT1, _inOrderMapTree1);
            MarkChildrenOutOfOrder(nodeT2, _inOrderMapTree2);

            var childrenSequenceT1 = CreateChildrenWithPartnerSequence(nodeT1, nodeT2, LeftToRightMatch);
            var childrenSequenceT2 = CreateChildrenWithPartnerSequence(nodeT2, nodeT1, RightToLeftMatch);

            var lcs = CreateLongestCommonSubsequence(childrenSequenceT1,
                childrenSequenceT2);

            foreach (var nodePair in lcs)
            {
                MarkInOrder(nodePair.Item1, _inOrderMapTree1);
                MarkInOrder(nodePair.Item2, _inOrderMapTree2);
            }

            foreach (var a in childrenSequenceT1)
            {
                if (!IsInOrder(a, _inOrderMapTree1))
                {
                    foreach (var b in childrenSequenceT2)
                    {
                        if (!IsInOrder(b, _inOrderMapTree2) && MatchingContainsNodes(a,b))
                        {
                            var childPosition = FindPosition(b, RootT1, RootT2);

                            if (!MoveClonesFromTree1.ContainsKey(nodeT1))
                            {
                                MoveClonesFromTree1.Add(nodeT1, nodeT1.DeepCloneNode());
                            }

                            var oldChildPosition = MoveClonesFromTree1[nodeT1].GetChildren().IndexOf(a);

                            if (childPosition == oldChildPosition)
                            {
                                MarkInOrder(a, _inOrderMapTree1);
                                MarkInOrder(b, _inOrderMapTree2);
                                continue;
                            }

                            MoveClonesForReinsertion.Add(a, a.DeepCloneNode());

                            IEditOperation move = new Move
                            {
                                ChildPosition = childPosition,
                                MovedNode = a,
                                ParentNode = nodeT1,
                                OldParentNode = nodeT1,
                                OldChildPosition = oldChildPosition
                            };

                            editScript.Add(move);

                            move.Apply(RootT1,this);

                            MarkInOrder(a, _inOrderMapTree1);
                            MarkInOrder(b, _inOrderMapTree2);
                        }
                    }
                }
            }
        }

        public HashSet<Tuple<ISSTNode, ISSTNode>> CreateLongestCommonSubsequence(List<ISSTNode> leftSequence,
            List<ISSTNode> rightSequence)
        {
            var leftSize = leftSequence.Count;
            var rightSize = rightSequence.Count;

            var table = new int[leftSize + 1, rightSize + 1];
            var extractFlags = new int[leftSize + 1, rightSize + 1];

            for (var i = 1; i <= leftSize; i++)
            {
                for (var j = 1; j <= rightSize; j++)
                {
                    if (MatchingContainsNodes(leftSequence[i - 1], rightSequence[j - 1]))
                    {
                        table[i, j] = table[i - 1, j - 1] + 1;
                        extractFlags[i, j] = Diag;
                    }
                    else if (table[i - 1, j] >= table[i, j - 1])
                    {
                        table[i, j] = table[i - 1, j];
                        extractFlags[i, j] = Up;
                    }
                    else
                    {
                        table[i, j] = table[i, j - 1];
                        extractFlags[i, j] = Left;
                    }
                }
            }

            var result = ExtractLCS(extractFlags, leftSequence, rightSequence, leftSize, rightSize);

            return result;
        }

        private HashSet<Tuple<ISSTNode, ISSTNode>> ExtractLCS(int[,] extractFlags, List<ISSTNode> leftSequence,
            List<ISSTNode> rightSequence, int leftSize, int rightSize)
        {
            var result =
                new HashSet<Tuple<ISSTNode, ISSTNode>>(
                    ObjectReferenceEqualityComparer<Tuple<ISSTNode, ISSTNode>>.Default);

            var i = leftSize;
            var j = rightSize;

            while (i != 0 && j != 0)
            {
                switch (extractFlags[i, j])
                {
                    case Diag:
                        result.Add(Tuple.Create(leftSequence[i - 1], rightSequence[j - 1]));
                        i = i - 1;
                        j = j - 1;
                        continue;
                    case Up:
                        i = i - 1;
                        continue;
                    default:
                        j = j - 1;
                        continue;
                }
            }
            return result;
        }

        public void MarkInOrder(ISSTNode node, Dictionary<ISSTNode, bool> inOrderMapTree)
        {
            inOrderMapTree[node] = true;
        }

        public List<ISSTNode> CreateChildrenWithPartnerSequence(ISSTNode node, ISSTNode partnerNode, Dictionary<ISSTNode,ISSTNode> matching)
        {
            var result = new KaVEList<ISSTNode>();
            foreach (var sstNode in node.GetChildren())
            {
                ISSTNode partner;
                matching.TryGetValue(sstNode, out partner);
                if (partner != null &&
                    partnerNode.GetChildren().Contains(partner, new ObjectReferenceEqualityComparer<ISSTNode>()))
                    result.Add(sstNode);
            }

            return result;
        }

        public void MarkChildrenOutOfOrder(ISSTNode node, Dictionary<ISSTNode, bool> inOrderMapTree)
        {
            foreach (var child in node.GetChildren())
            {
                inOrderMapTree[child] = false;
            }
        }

        public int FindPosition(ISSTNode node, ISSTNode rootT1, ISSTNode rootT2)
        {
            var parentNode = node.GetParent(rootT2, true, SSTNodeUtil.TreeSelection.Tree2);

            List<ISSTNode> parentChildren;

            if (parentNode is ISST)
            {
                parentChildren = GetChildrenForSST((ISST) parentNode, node);
            }
            else
            {
                parentChildren = parentNode.GetChildren();
            }

            var rightMostSibling = GetRightMostSiblingThatIsInOrder(node, parentChildren, _inOrderMapTree2);

            if (rightMostSibling == null) return 0;

            ISSTNode partnerRightMostSibling;
            RightToLeftMatch.TryGetValue(rightMostSibling, out partnerRightMostSibling);

            var parentPartnerRightMostSibling = partnerRightMostSibling.GetParent(rootT1, true, SSTNodeUtil.TreeSelection.Tree1);
            List<ISSTNode> parentPartnerChildren;
            if (parentPartnerRightMostSibling is ISST)
            {
                parentPartnerChildren = GetChildrenForSST((ISST)parentPartnerRightMostSibling, node);
            }
            else
            {
                parentPartnerChildren = parentPartnerRightMostSibling.GetChildren();
            }

            
            // normally jumps over nodes that are not in order to calculate index - this is only working if deleted nodes / moves are deleted from tree1
            
            //int count = 0;
            //for (int i = 0; i < parentPartnerChildren.FindIndex(otherNode => ReferenceEquals(otherNode, partnerRightMostSibling)); i++)
            //{
            //    var child = parentPartnerChildren[i];
            //    if (IsInOrder(child,_inOrderMapTree1))
            //    {
            //        count++;
            //    }
            //}
            //return count + 1;
            
            return parentPartnerChildren.FindIndex(otherNode => ReferenceEquals(otherNode, partnerRightMostSibling)) + 1;
        }

        private List<ISSTNode> GetChildrenForSST(ISST sst, ISSTNode node)
        {
            var delegateDecl = node as IDelegateDeclaration;
            if (delegateDecl != null)
            {
                var delegateList = Lists.NewListFrom(sst.Delegates).Cast<ISSTNode>().ToList();
                return delegateList;
            }

            var eventDecl = node as IEventDeclaration;
            if (eventDecl != null)
            {
                var eventList = Lists.NewListFrom(sst.Events).Cast<ISSTNode>().ToList();
                return eventList;
            }

            var fieldDecl = node as IFieldDeclaration;
            if (fieldDecl != null)
            {
                var fieldList = Lists.NewListFrom(sst.Fields).Cast<ISSTNode>().ToList();
                return fieldList;
            }

            var propertyDecl = node as IPropertyDeclaration;
            if (propertyDecl != null)
            {
                var propertyList = Lists.NewListFrom(sst.Properties).Cast<ISSTNode>().ToList();
                return propertyList;
            }

            var methodDecl = node as IMethodDeclaration;
            if (methodDecl != null)
            {
                var methodList = Lists.NewListFrom(sst.Methods).Cast<ISSTNode>().ToList();
                return methodList;
            }

            return new List<ISSTNode>();
        }

        [CanBeNull]
        public ISSTNode GetRightMostSiblingThatIsInOrder(ISSTNode node, List<ISSTNode> parentChildren,
            Dictionary<ISSTNode, bool> inOrderMapTree)
        {
            var nodeIndex = parentChildren.FindIndex(otherNode => ReferenceEquals(otherNode, node));

            ISSTNode resultNode = null;
            for (var i = nodeIndex - 1; i >= 0; i--)
            {
                if (!IsInOrder(parentChildren[i], inOrderMapTree)) continue;

                var rightMostChild = parentChildren[i];


                // Addition to regular algorithm - Fail safe skips childs that have no match
                if(!RightToLeftMatch.ContainsKey(rightMostChild)) continue;
                resultNode = rightMostChild;
                break;
            }

            return resultNode;
        }

        public bool IsInOrder(ISSTNode node, Dictionary<ISSTNode, bool> inOrderTree)
        {
            return inOrderTree[node];
        }

        public bool MatchingContainsNodes(ISSTNode leftNode, ISSTNode rightNode)
        {
            ISSTNode leftToRightMatch;
            LeftToRightMatch.TryGetValue(leftNode,out leftToRightMatch);
            ISSTNode rightToLeftMatch;
            RightToLeftMatch.TryGetValue(rightNode,out rightToLeftMatch);

            return leftNode != null && rightNode != null &&
                   ReferenceEquals(leftToRightMatch, rightNode) &&
                   ReferenceEquals(rightToLeftMatch, leftNode);
        }

        private bool IsMovedNode(ISSTNode currentNode)
        {
            var moves = EditScript.OfType<Move>();
            return moves.Any(move => ReferenceEquals(move.MovedNode, currentNode));
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using KaVE.Commons.Model.SSTs.Visitor;
using SSTDiff.EditScript.EditOperations;
using SSTDiff.Util;

namespace SSTDiff.EditScript
{
    public static class EditScriptUtil
    {
        public static Delete FindOverridenDeleteForInsertedNode(this List<IEditOperation> editScript,
            Insert insert, ISSTNode insertedNode)
        {
            var deletions = editScript.OfType<Delete>().ToList();
            
            foreach (var deletion in deletions)
            {
                if (ReferenceEquals(insert.ParentNode, deletion.ParentNode) &&
                    insert.ChildPosition == deletion.ChildPosition &&
                    ReferenceEquals(insertedNode, insert.InsertedNode) &&
                    deletion.DeletedNode.IsOverwrittenNode(deletion.ParentNode, deletion.ChildPosition))
                {
                    return deletion;
                }
            }


            return null;
        }

        public static IEditOperation FindOverridenEditForMovedNode(this List<IEditOperation> editScript,
            Move move, ISSTNode movedNode)
        {
            foreach (var editOperation in editScript)
            {
                var insert = editOperation as Insert;
                var delete = editOperation as Delete;
                var otherMove = editOperation as Move;
                if (insert != null)
                {
                    if (IsOverwrittenEditOperationByMove(movedNode, insert.ParentNode, insert.ChildPosition, move, insert.InsertedNode))
                    {
                        return insert;
                    }
                }

                if (delete != null)
                {
                    if (IsOverwrittenEditOperationByMove(movedNode, delete.ParentNode, delete.ChildPosition, move, delete.DeletedNode))
                    {
                        return delete;
                    }
                }

                if (otherMove != null)
                {
                    if (IsOverwrittenEditOperationByMove(movedNode, otherMove.ParentNode, move.ChildPosition, move, otherMove.MovedNode))
                    {
                        if (!ReferenceEquals(move, otherMove))
                            return otherMove;
                    }
                }
            }
            return null;
        }

        private static bool IsOverwrittenEditOperationByMove(ISSTNode movedNode, ISSTNode parent, int childPosition,
            Move move, ISSTNode overridenNode)
        {
            return ReferenceEquals(parent, move.OldParentNode) &&
                   childPosition == move.ChildPosition &&
                   ReferenceEquals(movedNode, move.NewlyInsertedNode) && overridenNode.IsOverwrittenNode(parent, childPosition);
        }
    }
}
﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using KaVE.Commons.Model.SSTs.Impl;
using KaVE.Commons.Model.SSTs.Visitor;
using KaVE.Commons.Utils.Collections;
using SSTDiff.Util;
using SSTDiff.Util.Visitor.NodeUtil;

namespace SSTDiff.Matching
{
    public class MatchingAlgorithm
    {
        private readonly MatchUtil _matchUtil;
        private readonly Settings _settings;

        private ISSTNode _root1;
        private ISSTNode _root2;

        public MatchingAlgorithm(Settings settings = null)
        {
            _settings = settings ?? new Settings();
            SSTNodeUtil.ResetCache();
            _matchUtil = new MatchUtil(_settings);
        }

        public List<Match> CreateMatching(ISSTNode tree1, ISSTNode tree2)
        {
            _root1 = tree1;
            _root2 = tree2;
            _matchUtil.Root1 = _root1;
            _matchUtil.Root2 = _root2;
            var parentsDictionaryT1 =
                new Dictionary<ISSTNode, ISSTNode>(ObjectReferenceEqualityComparer<ISSTNode>.Default);
            var parentsDictionaryT2 =
                new Dictionary<ISSTNode, ISSTNode>(ObjectReferenceEqualityComparer<ISSTNode>.Default);
            _root1.Accept(new ParentDictionaryVisitor(), parentsDictionaryT1);
            _root2.Accept(new ParentDictionaryVisitor(), parentsDictionaryT2);

            SSTNodeUtil.ParentCacheTree1 = parentsDictionaryT1;
            SSTNodeUtil.ParentCacheTree2 = parentsDictionaryT2;

            var finalMatching = new KaVEList<Match>();

            // Marks all nodes in T1 and T2 as unmatched
            var unmatchedNodesTree1 = _root1.GetPostOrderNodes();
            var unmatchedNodesTree2 = _root2.GetPostOrderNodes();

            var tempMatching = HashCodeMatchingForSSTRootNodes(unmatchedNodesTree1, unmatchedNodesTree2);

            tempMatching.AddRange(_settings.ActivateMatchingWithParent
                ? MatchLeavesWithParentNames(unmatchedNodesTree1, unmatchedNodesTree2)
                : MatchLeaves(unmatchedNodesTree1, unmatchedNodesTree2));

            var sortedTempMatching = SortTempMatching(tempMatching);

            AddBestMatchesToFinalMatching(sortedTempMatching, finalMatching, unmatchedNodesTree1, unmatchedNodesTree2);

            RemoveLeavesFromUnmatchedList(unmatchedNodesTree1, unmatchedNodesTree2);

            if (_settings.ActivateMatchingWithParent)
                MatchUnmatchedNodesWithBestMatch(unmatchedNodesTree1, unmatchedNodesTree2, finalMatching);
            else MatchUnmatchedNodesWithFirstMatch(unmatchedNodesTree1, unmatchedNodesTree2, finalMatching);

            return finalMatching;
        }

        private void RemoveLeavesFromUnmatchedList(List<ISSTNode> unmatchedNodesTree1,
            List<ISSTNode> unmatchedNodesTree2)
        {
            unmatchedNodesTree1.RemoveAll(node => !(node is SST) && node.GetChildren().Count == 0);
            unmatchedNodesTree2.RemoveAll(node => !(node is SST) && node.GetChildren().Count == 0);
        }

        #region Improved Matching

        public List<Match> MatchLeavesWithParentNames(List<ISSTNode> unmatchedNodesTree1,
            List<ISSTNode> unmatchedNodesTree2)
        {
            var tempMatching = new KaVEList<Match>();

            var leavesTree1 = _root1.GetLeaves(unmatchedNodesTree1);
            var leavesTree2 = _root2.GetLeaves(unmatchedNodesTree2);

            foreach (var sstLeafTree1 in leavesTree1)
            {
                foreach (var sstLeafTree2 in leavesTree2)
                {
                    double similiarity;
                    if (_matchUtil.MatchCriterion1WithParents(sstLeafTree1, sstLeafTree2, out similiarity,
                        sstLeafTree1.GetParent(_root1, true, SSTNodeUtil.TreeSelection.Tree1),
                        sstLeafTree2.GetParent(_root2, true, SSTNodeUtil.TreeSelection.Tree2)))
                    {
                        tempMatching.Add(Match.Create(sstLeafTree1, sstLeafTree2, similiarity));
                    }
                }
            }

            return tempMatching;
        }

        private void MatchUnmatchedNodesWithBestMatch(List<ISSTNode> unmatchedNodesTree1,
            List<ISSTNode> unmatchedNodesTree2,
            List<Match> finalMatching)
        {
            while (unmatchedNodesTree1.Count != 0 && unmatchedNodesTree2.Count != 0)
            {
                var leftNode = unmatchedNodesTree1.First();

                var candidates = new Dictionary<double, List<ISSTNode>>();
                foreach (var nodeInTree2 in unmatchedNodesTree2)
                {
                    double sim;
                    var matchCheck = _matchUtil.MatchCriterion2WithParents(leftNode, nodeInTree2,
                        out sim, leftNode.GetParent(_root1, true, SSTNodeUtil.TreeSelection.Tree1),
                        nodeInTree2.GetParent(_root2, true, SSTNodeUtil.TreeSelection.Tree2), finalMatching);
                    if (matchCheck)
                    {
                        if (candidates.ContainsKey(sim))
                        {
                            candidates[sim].Add(nodeInTree2);
                        }
                        else
                        {
                            candidates.Add(sim, new List<ISSTNode> {nodeInTree2});
                        }
                    }
                }

                var maxPair = new Tuple<double, List<ISSTNode>>(0.0, null);
                foreach (var candidate in candidates)
                {
                    if (candidate.Key > maxPair.Item1)
                    {
                        maxPair = Tuple.Create(candidate.Key, candidate.Value);
                    }
                }

                ISSTNode rightNode = null;
                var rightNodeCandidateList = maxPair.Item2;
                if (rightNodeCandidateList != null)
                {
                    rightNode = rightNodeCandidateList.First();
                }

                if (rightNode != null && !maxPair.Item1.Equals(0.0))
                {
                    finalMatching.Add(Match.Create(leftNode, rightNode));
                    MarkNodeAsMatched(unmatchedNodesTree1, leftNode);
                    MarkNodeAsMatched(unmatchedNodesTree2, rightNode);
                }
                else
                {
                    unmatchedNodesTree1.Remove(leftNode);
                }
            }
        }

        private List<Match> HashCodeMatchingForSSTRootNodes(List<ISSTNode> unmatchedNodesTree1,
            List<ISSTNode> unmatchedNodesTree2)
        {
            var matching = new KaVEList<Match>();
            var sstRoot1 = _root1 as SST;
            var sstRoot2 = _root2 as SST;
            if (sstRoot1 != null && sstRoot2 != null)
            {
                // Root hashcode check
                if (sstRoot1.GetHashCode() == sstRoot2.GetHashCode() && _settings.ActivateEarlyHashcodeMatchingForSST)
                {
                    while (unmatchedNodesTree1.Count != 0)
                    {
                        matching.Add(Match.Create(unmatchedNodesTree1.First(), unmatchedNodesTree2.First(), 1.0));
                        unmatchedNodesTree1.RemoveAt(0);
                        unmatchedNodesTree2.RemoveAt(0);
                    }
                }
                else if (_settings.ActivateEarlyHashcodeMatchingForMethodDeclarations)
                {
                    // Check MethodDeclarations 
                    foreach (var methodDeclarationA in sstRoot1.Methods)
                    {
                        foreach (var methodDeclarationB in sstRoot2.Methods)
                        {
                            if (methodDeclarationA.GetHashCode() == methodDeclarationB.GetHashCode())
                            {
                                var methodANodes = methodDeclarationA.GetAllNodes();
                                var methodBNodes = methodDeclarationB.GetAllNodes();

                                var index = 0;
                                while (index < methodANodes.Count)
                                {
                                    var methodANode = methodANodes[index];
                                    var methodBNode = methodBNodes[index];
                                    matching.Add(Match.Create(methodANode, methodBNode, 1.0));
                                    MarkNodeAsMatched(unmatchedNodesTree1, methodANode);
                                    MarkNodeAsMatched(unmatchedNodesTree2, methodBNode);
                                    index++;
                                }
                            }
                            else if (MatchUtil.CompareMethodIdentifier(methodDeclarationA, methodDeclarationB))
                            {
                                var methodANodes = methodDeclarationA.GetBreadthFirstOrderNodes();
                                methodANodes.RemoveAll(node => !node.GetChildren().Any());
                                var methodBNodes = methodDeclarationB.GetBreadthFirstOrderNodes();
                                methodBNodes.RemoveAll(node => !node.GetChildren().Any());

                                while (methodANodes.Count != 0 && methodBNodes.Count != 0)
                                {
                                    var methodANode = methodANodes.First();
                                    var hashCodeEqualNodeB =
                                        methodBNodes.FirstOrDefault(
                                            methodBNode => methodANode.GetHashCode() == methodBNode.GetHashCode());
                                    if (hashCodeEqualNodeB != null)
                                    {
                                        var nodesA = methodANode.GetAllNodes();
                                        var nodesB = hashCodeEqualNodeB.GetAllNodes();

                                        var index = 0;
                                        while (index < nodesA.Count)
                                        {
                                            var nodeA = nodesA[index];
                                            var nodeB = nodesB[index];
                                            matching.Add(Match.Create(nodeA, nodeB, 1.0));
                                            MarkNodeAsMatched(unmatchedNodesTree1, nodeA);
                                            MarkNodeAsMatched(unmatchedNodesTree2, nodeB);
                                            methodANodes.Remove(nodeA);
                                            methodBNodes.Remove(nodeB);
                                            index++;
                                        }
                                    }
                                    else
                                    {
                                        methodANodes.Remove(methodANode);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return matching;
        }

        #endregion

        # region Change Distilling Matching

        public List<Match> MatchLeaves(List<ISSTNode> unmatchedNodesTree1, List<ISSTNode> unmatchedNodesTree2)
        {
            var tempMatching = new KaVEList<Match>();

            var leavesTree1 = _root1.GetLeaves(unmatchedNodesTree1);
            var leavesTree2 = _root2.GetLeaves(unmatchedNodesTree2);

            foreach (var sstLeafTree1 in leavesTree1)
            {
                foreach (var sstLeafTree2 in leavesTree2)
                {
                    double similiarity;
                    if (_matchUtil.MatchCriterion1(sstLeafTree1, sstLeafTree2, out similiarity))
                    {
                        tempMatching.Add(Match.Create(sstLeafTree1, sstLeafTree2, similiarity));
                    }
                }
            }

            return tempMatching;
        }

        private void MatchUnmatchedNodesWithFirstMatch(List<ISSTNode> unmatchedNodesTree1,
            List<ISSTNode> unmatchedNodesTree2,
            List<Match> finalMatching)
        {
            while (unmatchedNodesTree1.Count != 0)
            {
                double similarity;
                var leftNode = unmatchedNodesTree1.First();
                var rightNode =
                    unmatchedNodesTree2.FirstOrDefault(
                        nodeTree2 => _matchUtil.MatchCriterion2(leftNode, nodeTree2, finalMatching, out similarity));
                if (rightNode != null)
                {
                    finalMatching.Add(Match.Create(leftNode, rightNode));
                    MarkNodeAsMatched(unmatchedNodesTree1, leftNode);
                    MarkNodeAsMatched(unmatchedNodesTree2, rightNode);
                }
                else
                {
                    unmatchedNodesTree1.Remove(leftNode);
                }
            }
        }

        #endregion

        #region Util Methods

        private void AddBestMatchesToFinalMatching(List<Match> sortedTempMatching, KaVEList<Match> finalMatching,
            List<ISSTNode> unmatchedNodesTree1,
            List<ISSTNode> unmatchedNodesTree2)
        {
            while (sortedTempMatching.Count > 0)
            {
                var match = sortedTempMatching.First();
                finalMatching.Add(match);
                sortedTempMatching.RemoveAll(
                    otherMatch => otherMatch.ContainsNode(match.NodeA) || otherMatch.ContainsNode(match.NodeB));
                MarkNodeAsMatched(unmatchedNodesTree1, match.NodeA);
                MarkNodeAsMatched(unmatchedNodesTree2, match.NodeB);
            }
        }

        public void MarkNodeAsMatched(List<ISSTNode> nodeList, ISSTNode node)
        {
            nodeList.RemoveAll(otherNode => ReferenceEquals(otherNode, node));
        }

        public List<Match> SortTempMatching(List<Match> tempMatching)
        {
            return tempMatching.OrderByDescending(match => match.Similiarity).ToList();
        }

        #endregion
    }
}
﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using KaVE.Commons.Model.SSTs;
using KaVE.Commons.Model.SSTs.Declarations;
using KaVE.Commons.Model.SSTs.Impl.Declarations;
using KaVE.Commons.Model.SSTs.Impl.Statements;
using KaVE.Commons.Model.SSTs.Visitor;
using SSTDiff.Util;
using SSTDiff.Util.Visitor.NodeUtil;
using static System.String;

namespace SSTDiff.Matching
{
    public class MatchUtil
    {
        private readonly int _dynamicThesholdForSmallSubtrees;
        private readonly double _innerNodeSimilarityMeasure;
        private readonly double _innerNodeSimilaritySmallSubtreesMeasure;
        private readonly double _innerNodeSimilarityWeighting;
        private readonly double _parentStringSimilarityMeasure;
        private readonly double _stringSimilarityMeasure;

        public MatchUtil(Settings settings)
        {
            _stringSimilarityMeasure = settings.StringSimilarityMeasure;
            _innerNodeSimilarityMeasure = settings.InnerNodeSimilarityMeasure;
            _innerNodeSimilarityWeighting = settings.InnerNodeSimilarityWeighting;
            _innerNodeSimilaritySmallSubtreesMeasure = settings.InnerNodeSimilaritySmallSubtreesMeasure;
            _dynamicThesholdForSmallSubtrees = settings.DynamicThesholdForSmallSubtrees;
            _parentStringSimilarityMeasure = settings.ParentStringSimilarityMeasure;
        }

        public ISSTNode Root1 { get; set; }
        public ISSTNode Root2 { get; set; }

        public bool MatchCriterion1(ISSTNode x, ISSTNode y, out double similarity)
        {
            if (x.GetType() != y.GetType())
            {
                similarity = 0.0;
                return false;
            }

            var xValue = x.GetValue();
            var yValue = y.GetValue();

            similarity = xValue.CalculateSimilarity(yValue);

            return (similarity >= _stringSimilarityMeasure);
        }

        public bool MatchCriterion1WithParents(ISSTNode x, ISSTNode y, out double similarity, ISSTNode parentX,
            ISSTNode parentY)
        {
            if (x.GetType() != y.GetType())
            {
                similarity = 0.0;
                return false;
            }

            if (x is UnknownStatement && y is UnknownStatement)
            {
                similarity = 1.0;
                return true;
            }

            var xValue = x.GetValue();
            var yValue = y.GetValue();


            var parentSim = ParentSimilarity(parentX, parentY);

            var leafSim = xValue.CalculateSimilarity(yValue);
            similarity = (leafSim + parentSim)/2;
            return (leafSim >= _stringSimilarityMeasure) && (parentSim >= _parentStringSimilarityMeasure);
        }

        public bool MatchCriterion2(ISSTNode x, ISSTNode y, List<Match> matches, out double similarity)
        {
            // Root needs to be matched
            if (x is ISST && y is ISST)
            {
                similarity = 1.0;
                return true;
            }

            if (x.GetType() != y.GetType())
            {
                similarity = 0.0;
                return false;
            }

            if (x.GetHashCode() == y.GetHashCode())
            {
                similarity = 1.0;
                return true;
            }

            if (MethodDeclarationSpecialCase(x, y, out similarity)) return true;

            var innerNodeSimilarity = CalculateInnerNodeSimilarity(x, y, matches);

            if (innerNodeSimilarity >= _innerNodeSimilarityWeighting && x.GetType() == y.GetType() &&
                (x.GetValue() == Empty || y.GetValue() == Empty))
            {
                similarity = innerNodeSimilarity;
                return true;
            }

            var innerNodeSimilarityCheck = x.GetLeaves().Count > _dynamicThesholdForSmallSubtrees
                ? innerNodeSimilarity >= _innerNodeSimilarityMeasure
                : innerNodeSimilarity >= _innerNodeSimilaritySmallSubtreesMeasure;

            double criterion1Similarity;
            var matchCriterion1 = MatchCriterion1(x, y, out criterion1Similarity);
            similarity = (criterion1Similarity + innerNodeSimilarity)/2;
            return matchCriterion1 && innerNodeSimilarityCheck;
        }

        public bool MatchCriterion2WithParents(ISSTNode x, ISSTNode y, out double similarity, ISSTNode parentX,
            ISSTNode parentY, List<Match> matches)
        {
            // Root needs to be matched
            if (x is ISST && y is ISST)
            {
                similarity = 1.0;
                return true;
            }

            if (x.GetType() != y.GetType())
            {
                similarity = 0.0;
                return false;
            }

            if (MethodDeclarationSpecialCase(x, y, out similarity)) return true;

            var innerNodeSimilarity = x.GetHashCode() == y.GetHashCode()
                ? 1.0
                : CalculateInnerNodeSimilarity(x, y, matches);


            if (innerNodeSimilarity >= _innerNodeSimilarityWeighting && x.GetType() == y.GetType())
            {
                similarity = innerNodeSimilarity;
                return true;
            }

            var innerNodeSimilarityCheck = x.GetLeaves().Count > _dynamicThesholdForSmallSubtrees
                ? innerNodeSimilarity >= _innerNodeSimilarityMeasure
                : innerNodeSimilarity >= _innerNodeSimilaritySmallSubtreesMeasure;

            var parentSim = ParentSimilarity(parentX, parentY);
            var parentSimilarityCheck = parentSim > _parentStringSimilarityMeasure;
            
            double criterion1Similarity;
            var matchCriterion1 = MatchCriterion1WithParents(x, y, out criterion1Similarity, parentX, parentY);

            similarity = (criterion1Similarity + innerNodeSimilarity + parentSim)/3;
            return matchCriterion1 && innerNodeSimilarityCheck && parentSimilarityCheck;
        }

        public double ParentSimilarity(ISSTNode parentX, ISSTNode parentY)
        {
            double parentSim;

            if (parentX == null || parentY == null) return 1.0;

            if (parentX.GetType() != parentY.GetType())
            {
                parentSim = 0.0;
            }
            else if (MethodDeclarationSpecialCase(parentX, parentY, out parentSim)) return parentSim;
            else
            {
                var parentXValue = parentX.Accept(new NameVisitor(), null);
                var parentYValue = parentY.Accept(new NameVisitor(), null);
                if (parentXValue == Empty && parentYValue == Empty && IsSpecialCaseWithoutValue(parentX))
                {
                    var parentX2 = parentX.GetParent(Root1, true, SSTNodeUtil.TreeSelection.Tree1);
                    var parentY2 = parentY.GetParent(Root2, true, SSTNodeUtil.TreeSelection.Tree2);
                    parentXValue = parentX2.Accept(new NameVisitor(), null);
                    parentYValue = parentY2.Accept(new NameVisitor(), null);
                }
                parentSim = parentXValue.CalculateSimilarity(parentYValue);
            }
            return parentSim;
        }

        public bool IsSpecialCaseWithoutValue(ISSTNode parent)
        {
            return parent is ParameterDeclaration;
        }

        public bool MethodDeclarationSpecialCase(ISSTNode x, ISSTNode y, out double similarity)
        {
            var methodDeclX = x as MethodDeclaration;
            var methodDeclY = y as MethodDeclaration;
            if (methodDeclX != null && methodDeclY != null)
            {
                if (CompareMethodIdentifier(methodDeclX, methodDeclY) ||
                    methodDeclX.Body.GetHashCode() == methodDeclY.Body.GetHashCode())
                {
                    similarity = 1.0;
                    return true;
                }

                similarity = ("CSharp.MethodName:" + methodDeclX.Name).CalculateSimilarity("CSharp.MethodName:" + methodDeclY.Name);
                return similarity >= _parentStringSimilarityMeasure;
            }
            similarity = 0.0;
            return false;
        }

        public static bool CompareMethodIdentifier(IMethodDeclaration methodDeclX, IMethodDeclaration methodDeclY)
        {
            return methodDeclX.Name.Identifier.Equals(methodDeclY.Name.Identifier);
        }

        public double CalculateInnerNodeSimilarity(ISSTNode x, ISSTNode y, List<Match> matches)
        {
            var leavesInX = x.GetLeaves();
            var leavesInY = y.GetLeaves();

            var maxLeaves = Math.Max(leavesInX.Count, leavesInY.Count);
            var commonMatches =
                leavesInX.Select(leafX => matches.Find(m => m.ContainsNode(leafX)))
                    .Where(match => match != null)
                    .Count(match => leavesInY.Exists(match.ContainsNode));

            return commonMatches/(double) maxLeaves;
        }
    }
}
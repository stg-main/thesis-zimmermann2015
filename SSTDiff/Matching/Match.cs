﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
using KaVE.Commons.Model.SSTs.Visitor;

namespace SSTDiff.Matching
{
    public class Match
    {
        public ISSTNode NodeA { get; set; }
        public ISSTNode NodeB { get; set; }
        public double Similiarity { get; set; }

        public static Match Create(ISSTNode nodeA, ISSTNode nodeB, double similiarity)
        {
            return new Match
            {
                NodeA = nodeA,
                NodeB = nodeB,
                Similiarity = similiarity
            };
        }

        public static Match Create(ISSTNode nodeA, ISSTNode nodeB)
        {
            return new Match
            {
                NodeA = nodeA,
                NodeB = nodeB,
                Similiarity = double.NaN
            };
        }

        public bool ContainsNode(ISSTNode node)
        {
            return ReferenceEquals(NodeA, node) || ReferenceEquals(NodeB, node);
        }

        public bool ContainsNodes(ISSTNode nodeA, ISSTNode nodeB)
        {
            return ReferenceEquals(NodeA, nodeA) && ReferenceEquals(NodeB, nodeB) ||
                   ReferenceEquals(NodeA, nodeB) && ReferenceEquals(NodeB, nodeA);
        }

        public ISSTNode GetOppositeNode(ISSTNode node)
        {
            return ReferenceEquals(node, NodeA) ? NodeB : NodeA;
        }

        protected bool Equals(Match other)
        {
            return Equals(NodeA, other.NodeA) && Equals(NodeB, other.NodeB) && Similiarity.Equals(other.Similiarity);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Match) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (NodeA != null ? NodeA.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (NodeB != null ? NodeB.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ Similiarity.GetHashCode();
                return hashCode;
            }
        }

        public override string ToString()
        {
            return "[" + NodeA + ", " + NodeB + ", " + Similiarity + "]";
        }
    }
}
﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.ComponentModel;
using JetBrains.Annotations;

namespace SSTDiff
{
    public class Settings : INotifyPropertyChanged
    {
        private bool _activateWrappingDetection;

        private bool _activateUnwrappingDetection;

        private bool _activateMatchingWithParent = true;

        private bool _activateEarlyHashcodeMatchingForSST = true;

        private bool _activateEarlyHashcodeMatchingForMethodDeclarations = true;

        private bool _activateDifferentUpdateOperationDisplay = true;

        private bool _activateMoveOfBlockDetection;

        private bool _activateJoinInsertsAndDelete;

        private bool _onMethodLevel;
        private bool _filterMoves = true;

        private double _stringSimilarityMeasure = 0.6;
        private double _parentStringSimilarityMeasure = 0.4;
        private double _innerNodeSimilarityWeighting = 0.8;
        private double _innerNodeSimilaritySmallSubtreesMeasure = 0.3;
        private double _innerNodeSimilarityMeasure = 0.6;
        private int _dynamicThesholdForSmallSubtrees = 8;

        public event PropertyChangedEventHandler PropertyChanged;

        public bool ActivateWrappingDetection
        {
            get { return _activateWrappingDetection; }
            set
            {
                _activateWrappingDetection = value;
                OnPropertyChanged(nameof(ActivateWrappingDetection));
            }
        }

        public bool ActivateUnwrappingDetection
        {
            get { return _activateUnwrappingDetection; }
            set
            {
                _activateUnwrappingDetection = value;
                OnPropertyChanged(nameof(ActivateUnwrappingDetection));
            }
        }

        public bool ActivateMatchingWithParent
        {
            get { return _activateMatchingWithParent; }
            set
            {
                _activateMatchingWithParent = value; 
                OnPropertyChanged(nameof(ActivateMatchingWithParent));
            }
        }

        public bool ActivateEarlyHashcodeMatchingForSST
        {
            get { return _activateEarlyHashcodeMatchingForSST; }
            set
            {
                _activateEarlyHashcodeMatchingForSST = value;
                OnPropertyChanged(nameof(ActivateEarlyHashcodeMatchingForSST));
            }
        }

        public bool ActivateEarlyHashcodeMatchingForMethodDeclarations
        {
            get { return _activateEarlyHashcodeMatchingForMethodDeclarations; }
            set
            {
                _activateEarlyHashcodeMatchingForMethodDeclarations = value; 
                OnPropertyChanged(nameof(ActivateEarlyHashcodeMatchingForMethodDeclarations));
            }
        }

        public bool ActivateDifferentUpdateOperationDisplay
        {
            get { return _activateDifferentUpdateOperationDisplay; }
            set
            {
                _activateDifferentUpdateOperationDisplay = value;
                OnPropertyChanged(nameof(ActivateDifferentUpdateOperationDisplay));
            }
        }

        public bool ActivateMoveOfBlockDetection
        {
            get { return _activateMoveOfBlockDetection; }
            set
            {
                _activateMoveOfBlockDetection = value;
                OnPropertyChanged(nameof(ActivateMoveOfBlockDetection));
            }
        }

        public bool ActivateJoinInsertsAndDelete
        {
            get { return _activateJoinInsertsAndDelete; }
            set
            {
                _activateJoinInsertsAndDelete = value;
                OnPropertyChanged(nameof(ActivateJoinInsertsAndDelete));
            }
        }

        public int DynamicThesholdForSmallSubtrees
        {
            get { return _dynamicThesholdForSmallSubtrees; }
            set
            {
                _dynamicThesholdForSmallSubtrees = value;
                OnPropertyChanged(nameof(DynamicThesholdForSmallSubtrees));
            }
        }

        public double InnerNodeSimilarityMeasure
        {
            get { return _innerNodeSimilarityMeasure; }
            set
            {
                _innerNodeSimilarityMeasure = value;
                OnPropertyChanged(nameof(InnerNodeSimilarityMeasure));
            }
        }

        public double InnerNodeSimilarityWeighting
        {
            get { return _innerNodeSimilarityWeighting; }
            set
            {
                _innerNodeSimilarityWeighting = value;
                OnPropertyChanged(nameof(InnerNodeSimilarityWeighting));
            }
        }

        public double InnerNodeSimilaritySmallSubtreesMeasure
        {
            get { return _innerNodeSimilaritySmallSubtreesMeasure; }
            set
            {
                _innerNodeSimilaritySmallSubtreesMeasure = value;
                OnPropertyChanged(nameof(InnerNodeSimilaritySmallSubtreesMeasure));
            }
        }

        public double StringSimilarityMeasure
        {
            get { return _stringSimilarityMeasure; }
            set
            {
                _stringSimilarityMeasure = value;
                OnPropertyChanged(nameof(StringSimilarityMeasure));
            }
        }

        public double ParentStringSimilarityMeasure
        {
            get { return _parentStringSimilarityMeasure; }
            set
            {
                _parentStringSimilarityMeasure = value;
                OnPropertyChanged(nameof(ParentStringSimilarityMeasure));
            }
        }

        public bool OnMethodLevel
        {
            get { return _onMethodLevel; }
            set
            {
                _onMethodLevel = value; 
                OnPropertyChanged(nameof(OnMethodLevel));
            }
        }

        public bool FilterMoves
        {
            get { return _filterMoves; }
            set
            {
                _filterMoves = value;
                OnPropertyChanged(nameof(FilterMoves));
            }
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
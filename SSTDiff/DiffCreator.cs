﻿using System.Collections.Generic;
using System.Linq;
using KaVE.Commons.Model.SSTs.Declarations;
using KaVE.Commons.Model.SSTs.Impl;
using KaVE.Commons.Model.SSTs.Impl.Declarations;
using KaVE.Commons.Model.SSTs.Visitor;
using KaVE.Commons.Utils.Collections;
using SSTDiff.EditScript;
using SSTDiff.EditScript.EditOperations;
using SSTDiff.Matching;
using SSTDiff.Transformations;
using SSTDiff.Util;
using SSTDiff.Util.Visitor.EditOperations;

namespace SSTDiff
{
    public class DiffCreator
    {
        public List<IEditOperation> CreateFinalEditScript(Settings settings, ISSTNode leftTree, ISSTNode rightTree)
        {
            var usedSettings = settings ?? new Settings();
            var editScript = usedSettings.OnMethodLevel ? DiffOnMethodLevel(leftTree, rightTree, usedSettings) : CreateDiff(leftTree, rightTree, usedSettings);
            return editScript;
        }

        private List<IEditOperation> DiffOnMethodLevel(ISSTNode leftTree, ISSTNode rightTree, Settings usedSettings)
        {
            var leftSST = leftTree as SST;
            var rightSST = rightTree as SST;

            var methodDeclarationMatching = new List<Match>();

            var matchUtil = new MatchUtil(usedSettings);

            var editScript = new List<IEditOperation>();

            if (leftSST != null && rightSST != null)
            {
                var leftMethodDeclarations = leftSST.Methods;
                var rightMethodDeclarations = rightSST.Methods;

                MethodDeclarationMatcher(leftMethodDeclarations, rightMethodDeclarations, matchUtil,
                    methodDeclarationMatching);

                MethodDeclarationDiffer(leftMethodDeclarations, rightMethodDeclarations, usedSettings,
                    methodDeclarationMatching, editScript, leftSST, rightSST);
            }

            return editScript;
        }

        public static void MethodDeclarationDiffer(IKaVESet<IMethodDeclaration> leftMethodDeclarations,
            IKaVESet<IMethodDeclaration> rightMethodDeclarations, Settings usedSettings,
            List<Match> methodDeclarationMatching, List<IEditOperation> editScript, SST leftSST, SST rightSST)
        {
            foreach (var rightMethodDeclaration in rightMethodDeclarations)
            {
                var match = methodDeclarationMatching.Find(m => m.ContainsNode(rightMethodDeclaration));
                if (match != null)
                {
                    var methodA = (MethodDeclaration) match.NodeA;
                    var methodB = (MethodDeclaration) match.NodeB;
                    if (methodA.Body.GetHashCode() != methodB.Body.GetHashCode() || match.Similiarity < 1.0 || !MatchUtil.CompareMethodIdentifier(methodA, methodB))
                    {
                        editScript.AddRange(CreateDiff(methodA, methodB, usedSettings));
                    }
                }
                else
                {
                    var insert = new Insert
                    {
                        ChildPosition = rightSST.Methods.ToList().IndexOf(rightMethodDeclaration),
                        InsertedNode = rightMethodDeclaration,
                        ParentNode = leftSST
                    };
                    insert.ParentNode.Accept(new InsertionVisitor(new EditScriptAlgorithm(new List<Match>())), insert);
                    editScript.Add(insert);
                }
            }

            foreach (var leftMethodDeclaration in leftMethodDeclarations)
            {
                var match = methodDeclarationMatching.Find(m => m.ContainsNode(leftMethodDeclaration));
                if (match == null)
                {
                    var delete = new Delete
                    {
                        ChildPosition = leftSST.Methods.ToList().IndexOf(leftMethodDeclaration),
                        DeletedNode = leftMethodDeclaration.DeepCloneNode(),
                        ParentNode = leftSST
                    };
                    delete.ParentNode.Accept(new InsertionVisitor(new EditScriptAlgorithm(new List<Match>())), new Insert
                    {
                        ChildPosition = delete.ChildPosition,
                        InsertedNode = delete.DeletedNode,
                        ParentNode = leftSST
                    });
                    editScript.Add(delete);
                }
            }
        }

        public static void MethodDeclarationMatcher(IKaVESet<IMethodDeclaration> leftMethodDeclarations,
            IKaVESet<IMethodDeclaration> rightMethodDeclarations,
            MatchUtil matchUtil, List<Match> methodDeclarationMatching)
        {
            foreach (var leftMethodDeclaration in leftMethodDeclarations)
            {
                foreach (var rightMethodDeclaration in rightMethodDeclarations)
                {
                    double similarity;
                    if (matchUtil.MethodDeclarationSpecialCase(leftMethodDeclaration, rightMethodDeclaration,
                        out similarity))
                    {
                        if (
                            !methodDeclarationMatching.Exists(
                                match => match.ContainsNode(leftMethodDeclaration) || match.ContainsNode(rightMethodDeclaration)))
                        {
                            methodDeclarationMatching.Add(Match.Create(leftMethodDeclaration, rightMethodDeclaration, similarity));
                        }
                    }
                }
            }
        }

        private static
            List<IEditOperation> CreateDiff
            (ISSTNode leftTree, ISSTNode rightTree, Settings usedSettings)
        {
            var matchingAlgorithm = new MatchingAlgorithm(usedSettings);
            var matching = matchingAlgorithm.CreateMatching(leftTree, rightTree);

            var editScriptAlgorithm = new EditScriptAlgorithm(matching);
            var editScript = editScriptAlgorithm.CreateEditScript(leftTree, rightTree);

            var transformations = new List<ITransformation>
            {
                usedSettings.ActivateWrappingDetection ? new WrappingDetection() : null,
                usedSettings.ActivateUnwrappingDetection ? new UnwrappingDetection() : null,
                new DeletedNodesFromMovesInsertion(),
                usedSettings.ActivateMoveOfBlockDetection ? new MoveOfBlocksDetection() : null,
                usedSettings.ActivateJoinInsertsAndDelete ? new JoinInsertsAndDelete() : null,
                usedSettings.FilterMoves ? new FilterMoves() : null
            };
            transformations.ForEach(
                transform =>
                {
                    if (transform != null)
                        transform.ApplyTransformation(editScript, leftTree, editScriptAlgorithm);
                });
            return editScript;
        }
    }

}
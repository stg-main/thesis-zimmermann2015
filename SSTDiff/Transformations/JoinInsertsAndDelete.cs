﻿using System.Collections.Generic;
using System.Linq;
using KaVE.Commons.Model.SSTs.Visitor;
using SSTDiff.EditScript;
using SSTDiff.EditScript.EditOperations;
using SSTDiff.Util;

namespace SSTDiff.Transformations
{
    public class JoinInsertsAndDelete : ITransformation
    {
        public void ApplyTransformation(List<IEditOperation> editScript, ISSTNode tree, EditScriptAlgorithm editScriptAlgorithm)
        {
            var inserts = editScript.OfType<Insert>().ToList();
            var deletions = editScript.OfType<Delete>().ToList();

            var removalList = new List<IEditOperation>();

            foreach (var insert in inserts)
            {
                foreach (var deletion in deletions)
                {
                    if (ReferenceEquals(insert.ParentNode, deletion.ParentNode) &&
                        insert.ChildPosition == deletion.ChildPosition)
                    {
                        editScript.Add(CreateReplacementUpdateOperation(insert,deletion));
                        removalList.Add(insert);
                        removalList.Add(deletion);
                    }
                }
            }

            removalList.ForEach(editOperation => editScript.Remove(editOperation));
        }

        private static Update CreateReplacementUpdateOperation(Insert insert, Delete deletion)
        {
            return new Update
            {
                NewNode = insert.InsertedNode,
                NewValue = insert.InsertedNode.GetValue(),
                OldValue = deletion.DeletedNode.GetValue(),
                UpdatedNode = insert.InsertedNode,
                UpdatedNodeBackup = deletion.DeletedNode
            };
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KaVE.Commons.Model.SSTs.Expressions.Simple;
using KaVE.Commons.Model.SSTs.References;
using KaVE.Commons.Model.SSTs.Visitor;
using SSTDiff.EditScript;
using SSTDiff.EditScript.EditOperations;

namespace SSTDiff.Transformations
{
    class FilterMoves : ITransformation
    {
        public void ApplyTransformation(List<IEditOperation> editScript, ISSTNode tree, EditScriptAlgorithm editScriptAlgorithm)
        {
            var moves = editScript.OfType<Move>().ToList();

            foreach (var move in moves)
            {
                if (IsMoveOnDollarVariable(move)) editScript.Remove(move);
            }
        }

        private bool IsMoveOnDollarVariable(Move move)
        {
            var varRef = move.MovedNode as IVariableReference;
            if (varRef != null)
            {
                return varRef.Identifier.StartsWith("$");
            }

            var refExpr = move.MovedNode as IReferenceExpression;
            if (refExpr != null)
            {
                var varRefChild = refExpr.Reference as IVariableReference;
                if (varRefChild != null)
                {
                    return varRefChild.Identifier.StartsWith("$");
                }
            }
            return false;
        }

    }
}

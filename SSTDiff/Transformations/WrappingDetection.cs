﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;
using System.Linq;
using KaVE.Commons.Model.SSTs;
using KaVE.Commons.Model.SSTs.Visitor;
using SSTDiff.EditScript;
using SSTDiff.EditScript.EditOperations;
using SSTDiff.Util;

namespace SSTDiff.Transformations
{
    public class WrappingDetection : ITransformation
    {
        public void ApplyTransformation(List<IEditOperation> editScript, ISSTNode tree, EditScriptAlgorithm editScriptAlgorithm)
        {
            var moves = editScript.OfType<Move>().ToList();
            var inserts = editScript.OfType<Insert>().ToList();
            foreach (var move in moves)
            {
                if (!(move.MovedNode is IStatement)) return;
                var parentList = new List<ISSTNode>();
                var parent = move.ParentNode.GetParent(tree, false);
                while (parent != null)
                {
                    parentList.Add(parent);
                    parent = parent.GetParent(tree, true, SSTNodeUtil.TreeSelection.Tree1);
                }
                if (parentList.Any(p => ReferenceEquals(p, move.OldParentNode)))
                {
                    if (inserts.Exists(insert => ReferenceEquals(insert.ParentNode, move.OldParentNode))) editScript.Remove(move);
                }
            }
        }
    }
}
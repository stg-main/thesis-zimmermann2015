﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;
using System.Linq;
using KaVE.Commons.Model.SSTs.Visitor;
using SSTDiff.EditScript;
using SSTDiff.EditScript.EditOperations;
using SSTDiff.Util.Visitor.EditOperations;

namespace SSTDiff.Transformations
{
    public class DeletedNodesFromMovesInsertion : ITransformation
    {
        public void ApplyTransformation(List<IEditOperation> editScript, ISSTNode tree, EditScriptAlgorithm editScriptAlgorithm)
        {
            var moves = editScript.OfType<Move>().ToList();
            if (!moves.Any()) return;
            foreach (var move in moves)
            {
                var oldPosition = move.OldChildPosition;
                var cloneOfMovedNode = editScriptAlgorithm.MoveClonesForReinsertion[move.MovedNode];
                move.NewlyInsertedNode = cloneOfMovedNode;
                move.OldParentNode.Accept(new InsertionVisitor(editScriptAlgorithm),
                    new Insert
                    {
                        ChildPosition = oldPosition,
                        InsertedNode = cloneOfMovedNode,
                        ParentNode = move.OldParentNode
                    });
            }
        }
    }
}
﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;
using System.Linq;
using KaVE.Commons.Model.SSTs;
using KaVE.Commons.Model.SSTs.Impl.Blocks;
using KaVE.Commons.Model.SSTs.Impl.References;
using KaVE.Commons.Model.SSTs.Visitor;
using KaVE.Commons.Utils.Collections;
using SSTDiff.EditScript;
using SSTDiff.EditScript.EditOperations;
using SSTDiff.Util;
using SSTDiff.Util.Visitor.EditOperations;

namespace SSTDiff.Transformations
{
    public class MoveOfBlocksDetection : ITransformation
    {
        public void ApplyTransformation(List<IEditOperation> editScript, ISSTNode tree, EditScriptAlgorithm editScriptAlgorithm)
        {
            var moves = editScript.OfType<Move>();
            var movesWithSameParents =
                new Dictionary<ISSTNode, List<Move>>(ObjectReferenceEqualityComparer<ISSTNode>.Default);

            foreach (var move in moves)
            {
                if (!(move.MovedNode is IStatement)) continue;

                if (movesWithSameParents.ContainsKey(move.ParentNode))
                {
                    movesWithSameParents[move.ParentNode].Add(move);
                }
                else
                {
                    movesWithSameParents.Add(move.ParentNode, new KaVEList<Move> {move});
                }
            }

            RemoveSingleMovesForParent(movesWithSameParents);

            foreach (var movesWithSameParent in movesWithSameParents)
            {
                var moveList = movesWithSameParent.Value;
                var maxBlock = GetMaximumBlock(moveList);
                if (maxBlock.Count > 1)
                {
                    editScript.RemoveAll(
                        edit => maxBlock.Contains(edit, ObjectReferenceEqualityComparer<IEditOperation>.Default));
                    var deletionVisitor = new DeletionVisitor();
                    foreach (var move in maxBlock)
                    {
                        movesWithSameParent.Key.Accept(deletionVisitor, move.MovedNode);
                        move.OldParentNode.Accept(deletionVisitor, move.NewlyInsertedNode);
                    }
                    var replacement = CreateReplacementBlock(maxBlock);
                    var replacementNewlyInserted = CreateReplacementBlock(maxBlock);
                    var childPosition = maxBlock.Min(move => move.ChildPosition);
                    var oldChildPosition = maxBlock.Min(move => move.OldChildPosition);
                    var oldParentNode = maxBlock[0].OldParentNode;
                    editScript.Add(CreateReplacementMove(replacement, replacementNewlyInserted, movesWithSameParent.Key, oldParentNode,
                        childPosition, oldChildPosition));
                    movesWithSameParent.Key.Accept(new InsertionVisitor(editScriptAlgorithm), new Insert{ChildPosition = childPosition, InsertedNode = replacement, ParentNode = movesWithSameParent.Key});
                    oldParentNode.Accept(new InsertionVisitor(editScriptAlgorithm), new Insert {ChildPosition = oldChildPosition, InsertedNode = replacementNewlyInserted, ParentNode = oldParentNode});
                }
            }
        }

        private List<Move> GetMaximumBlock(List<Move> moveList)
        {
            var movesWithSameOldParent =
                new Dictionary<ISSTNode, List<Move>>(ObjectReferenceEqualityComparer<ISSTNode>.Default);
            foreach (var move in moveList)
            {
                if (movesWithSameOldParent.ContainsKey(move.OldParentNode))
                {
                    movesWithSameOldParent[move.OldParentNode].Add(move);
                }
                else
                {
                    movesWithSameOldParent.Add(move.OldParentNode, new KaVEList<Move> { move });
                }
            }

            List<Move> maxMoveList = new KaVEList<Move>();
            foreach (var pair in movesWithSameOldParent)
            {
                if (pair.Value.Count > maxMoveList.Count) maxMoveList = pair.Value;
            }

            List<Move> finalList = new KaVEList<Move>();
            foreach (var move in maxMoveList)
            {
                if (finalList.Count == 0)
                {
                    finalList.Add(move);
                    continue;
                }

                var differenceBetweenChilds = move.ChildPosition - finalList.Last().ChildPosition;
                if(differenceBetweenChilds == 1) finalList.Add(move);
            }
            return finalList;
        }

        private IEditOperation CreateReplacementMove(ISSTNode replacement, ISSTNode newlyInsertedNode, ISSTNode parentNode, ISSTNode oldParentNode,
            int childPosition, int oldChildPosition)
        {
            return new Move
            {
                ChildPosition = childPosition,
                MovedNode = replacement,
                NewlyInsertedNode = newlyInsertedNode,
                OldChildPosition = oldChildPosition,
                OldParentNode = oldParentNode,
                ParentNode = parentNode
            };
        }

        private ISSTNode CreateReplacementBlock(List<Move> moveList)
        {
            return new UsingBlock
            {
                Body = Lists.NewListFrom(moveList.Select(move => move.MovedNode).Cast<IStatement>())
            };
        }

        private static void RemoveSingleMovesForParent(Dictionary<ISSTNode, List<Move>> movesWithSamePartners)
        {
            foreach (var keyValuePair in movesWithSamePartners.Where(keyValuePair => keyValuePair.Value.Count < 2).ToList())
            {
                movesWithSamePartners.Remove(keyValuePair.Key);
            }
        }
    }
}
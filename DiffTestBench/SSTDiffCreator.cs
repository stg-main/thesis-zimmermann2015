﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KaVE.Commons.Model.SSTs.Impl;
using KaVE.Commons.Utils.Collections;
using SSTDiff.EditScript;
using SSTDiff.EditScript.EditOperations;
using SSTDiff.Matching;

namespace DiffTestBench
{
    public class SSTDiffCreator : IDiffCreator
    {
        public string CreateDiff(Dictionary<Tuple<string, SST>, Tuple<string, SST>> sstDictionary)
        {
            var editScriptDictionary = new Dictionary<Tuple<string, string>, List<IEditOperation>>();
            var matchingAlgorithm = new MatchingAlgorithm();
            foreach (var keyValuePair in sstDictionary)
            {
                var leftRoot = keyValuePair.Key.Item2;
                var rightRoot = keyValuePair.Value.Item2;

                var matching = matchingAlgorithm.CreateMatching(leftRoot, rightRoot);
                var editScriptCreator = new EditScriptAlgorithm(matching);
                var editScript = editScriptCreator.CreateEditScript(leftRoot, rightRoot);

                // TODO: add settings and transformations here

                editScriptDictionary.Add(Tuple.Create(keyValuePair.Key.Item1, keyValuePair.Value.Item1), editScript);
            }
            
            return CreateDiffOutput(editScriptDictionary);
        }

        private string CreateDiffOutput(Dictionary<Tuple<string, string>, List<IEditOperation>> editScriptDictionary)
        {
            var sb = new StringBuilder();
            foreach (var keyValuePair in editScriptDictionary)
            {
                sb.AppendLine(string.Format("\"{0}\" \"{1}\"", keyValuePair.Key.Item1, keyValuePair.Key.Item2));
                sb.AppendLine();
                sb.AppendLine(CreateEditOperationsString(keyValuePair.Value));
                sb.AppendLine();
            }
            return sb.ToString();
        }

        private string CreateEditOperationsString(List<IEditOperation> editOperations)
        {
            var sb = new StringBuilder();

            var insertOperations = Lists.NewListFrom(editOperations.Where(editOperation => editOperation is Insert));
            var deleteOperations = Lists.NewListFrom(editOperations.Where(editOperation => editOperation is Delete));
            var moveOperations = Lists.NewListFrom(editOperations.Where(editOperation => editOperation is Move));
            var updateOperations = Lists.NewListFrom(editOperations.Where(editOperation => editOperation is Update));

            sb.AppendLine("Update Operations :");
            sb.AppendLine(updateOperations.ToString());
            sb.AppendLine();
            sb.AppendLine("Insert Operations :");
            sb.AppendLine(insertOperations.ToString());
            sb.AppendLine();
            sb.AppendLine("Delete Operations: ");
            sb.AppendLine(deleteOperations.ToString());
            sb.AppendLine();
            sb.AppendLine("Move Operations :");
            sb.AppendLine(moveOperations.ToString());
            sb.AppendLine();

            return sb.ToString();
        }
    }
}
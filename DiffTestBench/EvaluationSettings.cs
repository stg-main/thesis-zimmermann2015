﻿namespace DiffTestBench
{
    public class EvaluationSettings
    {
        public bool EvaluationMode { get; set; }

        public int EvaluationAmount { get; set; }
        
        public int UserEvaluationAmount { get; set; }

        public int DataSetNumber { get; set; }

        public int DefaultSeed { get; set; }

        public string Path { get; set; }
        
    }
}
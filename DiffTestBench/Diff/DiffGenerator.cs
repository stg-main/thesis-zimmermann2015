﻿using System;
using System.Collections.Generic;
using KaVE.Commons.Model.SSTs.Visitor;
using SSTDiff;
using SSTDiff.EditScript.EditOperations;

namespace DiffTestBench.Diff
{
    public class DiffGenerator
    {
        public static bool GenerateDiff(Settings settings, ISSTNode leftTree, ISSTNode rightTree, out string output, out List<IEditOperation> outputEditscript)
        {
            try
            {
                var diffCreator = new DiffCreator();
                var editScript = diffCreator.CreateFinalEditScript(settings, leftTree, rightTree);
                
                output = editScript.Count == 0 ? "0" : editScript.Count.ToString();
                outputEditscript = editScript;
                return true;
            }
            catch (Exception e)
            {
                output = e.ToString();
                outputEditscript = new List<IEditOperation>();
                return false;
            }
        }
    }
}
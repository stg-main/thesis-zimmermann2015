﻿using System;
using System.Collections.Generic;
using System.Linq;
using KaVE.Commons.Model.SSTs;
using KaVE.Commons.Model.SSTs.Impl.Declarations;
using KaVE.Commons.Model.SSTs.Visitor;
using KaVE.Commons.Utils.Collections;
using KaVE.Commons.Utils.SSTPrinter;
using SSTDiff;
using SSTDiff.EditScript.EditOperations;
using SSTDiff.Matching;
using SSTDiff.Util;
using SSTDiff.Util.DiffPrinter;

namespace DiffTestBench.Diff
{
    public class XamlPrinter
    {
        public static string PrintDiff(ISSTNode leftTree, ISSTNode rightTree, Settings settings = null)
        {
            List<IEditOperation> editScript;
            string output;
            var success = DiffGenerator.GenerateDiff(settings, leftTree, rightTree, out output, out editScript);
            if (!success) return "Error while calculating Diff" + Environment.NewLine + output;

            if (editScript.Count == 0)
            {
                return "SSTs are equal";
            }

            var visitor = new DiffPrintingVisitor(new Dictionary<Match, bool>());
            var context = new XamlDiffPrintingContext(editScript, settings);
            leftTree.Accept(visitor, context);
            var contextXaml = context.ToString();
            return contextXaml.Insert(0,
                string.Format("Edit Script Count: {0}{1}", editScript.Count, Environment.NewLine));
        }

        public static Tuple<string, string, string> PrintOnlyChanges(ISSTNode left, ISSTNode right,
            Settings settings = null)
        {
            var leftTree = (ISST) left;
            var rightTree = (ISST) right;
            var leftTreeBackup = (ISST) left.DeepCloneNode();

            var methodDeclarationMatchingForDiff = new List<Match>();
            var methodDeclarationMatchingForBackup = new List<Match>();
            DiffCreator.MethodDeclarationMatcher(leftTree.Methods, rightTree.Methods, new MatchUtil(settings),
                methodDeclarationMatchingForDiff);
            DiffCreator.MethodDeclarationMatcher(leftTreeBackup.Methods, rightTree.Methods, new MatchUtil(settings),
                methodDeclarationMatchingForBackup);

            var changedMethodsDictionaryForDiff = CreateChangedMethodsDictionary(methodDeclarationMatchingForDiff);
            var changedMethodsDictionaryForBackup = CreateChangedMethodsDictionary(methodDeclarationMatchingForBackup);


            List<IEditOperation> editScript;
            string output;
            var success = DiffGenerator.GenerateDiff(settings, leftTree, rightTree, out output, out editScript);
            
            UpdateChangedMethodsDictionary(changedMethodsDictionaryForDiff, editScript);
            UpdateChangedMethodsDictionary(changedMethodsDictionaryForBackup, editScript);

            var leftTreePrint = PrintOnlyChangesForSST(leftTreeBackup, changedMethodsDictionaryForBackup);
            var rightTreePrint = PrintOnlyChangesForSST(rightTree, changedMethodsDictionaryForDiff);
            
            if (!success)
                return Tuple.Create(leftTreePrint, rightTreePrint,
                    "Error while calculating Diff" + Environment.NewLine + output);

            if (editScript.Count == 0)
            {
                return Tuple.Create(leftTreePrint, rightTreePrint, "SSTs are equal");
            }
            
            var visitor = new DiffPrintingVisitor(changedMethodsDictionaryForDiff);
            var context = new XamlDiffPrintingContext(editScript, settings);
            leftTree.Accept(visitor, context);
            var diffXaml = context.ToString();
            diffXaml = diffXaml.Insert(0,
                string.Format("Edit Script Count: {0}{1}", editScript.Count, Environment.NewLine));
            return Tuple.Create(leftTreePrint, rightTreePrint, diffXaml);
        }

        public static string PrintSST(ISSTNode tree)
        {
            var visitor = new SSTPrintingVisitor();
            var context = new XamlSSTPrintingContext();
            tree.Accept(visitor, context);
            return context.ToString();
        }

        private static void UpdateChangedMethodsDictionary(Dictionary<Match, bool> changedMethodsDictionary, List<IEditOperation> editscript)
        {
            var keysToChange = new List<Match>();
            foreach (var keyValuePair in changedMethodsDictionary)
            {
                if (editscript.Exists(editOperation => editOperation.ContainsNode(keyValuePair.Key.NodeA)))
                {
                    keysToChange.Add(keyValuePair.Key);
                }
            }
            foreach (var match in keysToChange)
            {
                changedMethodsDictionary[match] = true;
            }
        }

        private static string PrintOnlyChangesForSST(ISST sst, Dictionary<Match, bool> methodDeclarationMatching)
        {
            var visitor = new SSTPrintingVisitor();
            var sstPrintingContext = new XamlSSTPrintingContext();

            OpenClassDeclaration(sst, sstPrintingContext);

            AppendMemberDeclarationGroup(sstPrintingContext, visitor, sst.Delegates);
            AppendMemberDeclarationGroup(sstPrintingContext, visitor, sst.Events);
            AppendMemberDeclarationGroup(sstPrintingContext, visitor, sst.Fields);
            AppendMemberDeclarationGroup(sstPrintingContext, visitor, sst.Properties);
            AppendMethodDeclarationGroup(sstPrintingContext, visitor, sst.Methods, methodDeclarationMatching,
                2, 1);

            CloseClassDeclaration(sstPrintingContext);

            return sstPrintingContext.ToString();
        }

        private static void OpenClassDeclaration(ISST sst, SSTPrintingContext c)
        {
            c.Indentation();

            if (sst.EnclosingType.IsInterfaceType)
            {
                c.Keyword("interface");
            }
            else if (sst.EnclosingType.IsEnumType)
            {
                c.Keyword("enum");
            }
            else if (sst.EnclosingType.IsStructType)
            {
                c.Keyword("struct");
            }
            else
            {
                c.Keyword("class");
            }

            c.Space().Type(sst.EnclosingType);

            if (c.TypeShape != null && c.TypeShape.TypeHierarchy.HasSupertypes)
            {
                c.Text(" : ");

                if (c.TypeShape.TypeHierarchy.HasSuperclass && c.TypeShape.TypeHierarchy.Extends != null)
                {
                    c.Type(c.TypeShape.TypeHierarchy.Extends.Element);

                    if (c.TypeShape.TypeHierarchy.IsImplementingInterfaces)
                    {
                        c.Text(", ");
                    }
                }

                foreach (var i in c.TypeShape.TypeHierarchy.Implements)
                {
                    c.Type(i.Element);

                    if (!ReferenceEquals(i, c.TypeShape.TypeHierarchy.Implements.Last()))
                    {
                        c.Text(", ");
                    }
                }
            }

            c.NewLine()
                .Indentation().Text("{").NewLine();

            c.IndentationLevel++;
        }

        private static void CloseClassDeclaration(SSTPrintingContext c)
        {
            c.IndentationLevel--;

            c.Indentation().Text("}");
        }

        private static void AppendMemberDeclarationGroup(SSTPrintingContext c,
            ISSTNodeVisitor<SSTPrintingContext> visitor,
            IEnumerable<IMemberDeclaration> nodeGroup,
            int inBetweenNewLineCount = 1,
            int trailingNewLineCount = 2)
        {
            var sstNodes = nodeGroup.ToList();
            foreach (var node in sstNodes)
            {
                node.Accept(visitor, c);

                var newLinesNeeded = !ReferenceEquals(node, sstNodes.Last())
                    ? inBetweenNewLineCount
                    : trailingNewLineCount;

                for (var i = 0; i < newLinesNeeded; i++)
                {
                    c.NewLine();
                }
            }
        }

        private static void AppendMethodDeclarationGroup(SSTPrintingContext c,
            SSTPrintingVisitor visitor,
            IEnumerable<IMemberDeclaration> nodeGroup,
            Dictionary<Match, bool> methodMatches,
            int inBetweenNewLineCount = 1,
            int trailingNewLineCount = 2)
        {
            var sstNodes = nodeGroup.ToList();
            var dotsTriggered = false;
            foreach (var node in sstNodes)
            {
                var matchPair = methodMatches.FirstOrDefault(pair => pair.Key.ContainsNode(node));
                if (matchPair.Key != null)
                {
                    if (matchPair.Value)
                    {
                        AppendMethodToContext(c, visitor, inBetweenNewLineCount, trailingNewLineCount, node, sstNodes);
                        dotsTriggered = false;
                    }
                    else if(!dotsTriggered)
                    {
                        AppendDots(c);
                        dotsTriggered = true;
                    }
                }
                else
                {
                    AppendMethodToContext(c,visitor,inBetweenNewLineCount,trailingNewLineCount,node,sstNodes);
                }
            }
        }

        private static void AppendDots(SSTPrintingContext sstPrintingContext)
        {
            sstPrintingContext.Indentation().Text("<Span FontSize=\"14\" Foreground=\"DarkSlateGray\">[...]</Span>").NewLine().NewLine();
        }

        private static void AppendMethodToContext(SSTPrintingContext c, SSTPrintingVisitor visitor, int inBetweenNewLineCount,
            int trailingNewLineCount, IMemberDeclaration node, List<IMemberDeclaration> sstNodes)
        {
            node.Accept(visitor, c);

            var newLinesNeeded = !ReferenceEquals(node, sstNodes.Last())
                ? inBetweenNewLineCount
                : trailingNewLineCount;

            for (var i = 0; i < newLinesNeeded; i++)
            {
                c.NewLine();
            }
        }

        private static Dictionary<Match, bool> CreateChangedMethodsDictionary(List<Match> methodMatching)
        {
            var result = new Dictionary<Match, bool>();
            foreach (var match in methodMatching)
            {
                var methodA = (MethodDeclaration) match.NodeA;
                var methodB = (MethodDeclaration) match.NodeB;
                if (methodA.Body.GetHashCode() != methodB.Body.GetHashCode() || match.Similiarity < 1.0 ||
                    !MatchUtil.CompareMethodIdentifier(methodA, methodB) ||
                    methodA.GetValue() != methodB.GetValue())
                {
                    result.Add(match, true);
                }
                else
                {
                    result.Add(match, false);
                }
            }
            return result;
        }
    }
}
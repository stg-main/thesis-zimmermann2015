﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using DiffTestBench.Import;
using KaVE.Commons.Model.Events.CompletionEvents;
using KaVE.Commons.Utils.Json;
using SSTDiff;

namespace DiffTestBench
{
    public enum Modes
    {
        OldDiffTestBench,
        ErrorCheck,
        Evaluation,
        Cleanup,
        CreateEvaluationData
    }

    public class Program
    {
        public const string RootDir = @"C:\Diff Tests\";
        public const string ADir = @"A\";
        public const string BDir = @"B\";

        public static Modes Mode = Modes.ErrorCheck;

        protected static Dictionary<string, Settings> SettingsToEvaluate = new Dictionary<string, Settings>
        {
            {
                "Wrapping-Unwrapping Detection",
                new Settings
                {
                    ActivateWrappingDetection = true,
                    ActivateUnwrappingDetection = true
                }
            },
            {
                "Block Move Detection",
                new Settings
                {
                    ActivateMoveOfBlockDetection = true
                }
            },
            {
                "Join Insertion-Deletions",
                new Settings
                {
                    ActivateJoinInsertsAndDelete = true
                }
            }
        };

        private static void Main(string[] args)
        {
            Console.WriteLine("Select Mode:");
            Console.WriteLine("0 - Old Diff Test Bench");
            Console.WriteLine("1 - Error Check");
            Console.WriteLine("2 - Evaluation");
            Console.WriteLine("3 - Cleanup");
            Console.WriteLine("4 - Create Evaluation Data");
            
            string mode = Console.ReadLine();
            int r;
            if(int.TryParse(mode,out r) && r > 4)
            {
                Console.WriteLine("Wrong mode selected");
            }

            switch ((Modes) r)
            {
                case Modes.OldDiffTestBench:
                    OldDiffTestBench();
                    break;
                case Modes.ErrorCheck:
                    ErrorCheck();
                    break;
                case Modes.Evaluation:
                    Evaluation();
                    break;
                case Modes.Cleanup:
                    Cleanup();
                    break;
                case Modes.CreateEvaluationData:
                    CreateEvaluationData();
                    break;
            }

        }

        private static void CreateEvaluationData()
        {
            var evaluationDataCreator = new EvaluationDataCreator();
            evaluationDataCreator.CreateData();
        }

        private static void Cleanup()
        {
            Console.WriteLine("Input directoy path");
            string path = Console.ReadLine();
            if (!string.IsNullOrEmpty(path) && Directory.Exists(path))
            {
                var fileList = Directory.EnumerateFiles(path).Where(filePath => filePath.EndsWith(".json"));
                PerformCleanup(fileList.ToList());
            }
        }

        private static void PerformCleanup(List<string> fileList)
        {
            int index = 0;
            foreach (var filePath in fileList)
            {
                Console.WriteLine(index + " of " + fileList.Count);
                var text = File.ReadAllText(filePath);
                var completionEvent = text.ParseJsonTo<CompletionEvent>();
                if(completionEvent.Context2.IsDefault() || completionEvent.TerminatedState != TerminationState.Applied) File.Delete(filePath);
                index++;
            }
        }

        private static void OldDiffTestBench()
        {
            var sstReader = new SSTReader(RootDir, ADir, BDir);
            var sstDictionary = sstReader.ReadSsts();

            var textDiffCreator = new TextDiffCreator(RootDir);

            var textDiffOut = textDiffCreator.CreateDiff(sstDictionary);
            var sstDiffOut = new SSTDiffCreator().CreateDiff(sstDictionary);

            WriteOutput(textDiffOut, "TextDiffOut.txt");
            WriteOutput(sstDiffOut, "SSTDiffOut.txt");
        }

        public static void WriteOutput(string output, string fileName)
        {
            File.WriteAllText(Path.Combine(RootDir, fileName), output);
        }

        private static void ErrorCheck()
        {
            Console.WriteLine("Choose heuristics to check for errors");
            var settings = SettingSelection();
            var contextDictionary = ReadContexts();

            var errorChecker = new ErrorChecker(contextDictionary, settings);
            string output = errorChecker.ProcessErrorCheck();
            var activatedSettingsString = GetSelectedSettingsAsNumbersString(settings);
            WriteOutput(output, "ErrorOutput" + activatedSettingsString + ".txt");
        }

        private static void Evaluation()
        {
            Console.WriteLine("Evaluate all heuristics one by one? (y/n)");
            var yesNoInput = Console.ReadLine();
            if (yesNoInput != null && yesNoInput.Equals("y"))
            {
                EvaluateAllSettingsOneByOne();
            }
            else
            {
                Console.WriteLine("Choose reference settings");
                var referenceSettings = SettingSelection();
                Console.WriteLine("Choose heuristics to evaluate");
                var settings = SettingSelection();
                var contextDictionary = ReadContexts();

                var output = EvaluateSettings(contextDictionary, referenceSettings, settings);
                var activatedSettingsString = GetSelectedSettingsAsNumbersString(settings);
                WriteOutput(output, "EvaluationOutput" + activatedSettingsString + ".csv");
                SaveContextDictionary(contextDictionary);
            }

        }

        private static void EvaluateAllSettingsOneByOne()
        {
            var contextDictionary = ReadContexts();
            
            foreach (var settingPair in SettingsToEvaluate)
            {
                var settings = settingPair.Value;
                var referenceSettings = new Settings
                {
                    ActivateEarlyHashcodeMatchingForMethodDeclarations = true,
                    ActivateEarlyHashcodeMatchingForSST = true,
                    ActivateMatchingWithParent = true
                };

                var output = EvaluateSettings(contextDictionary, referenceSettings, settings);
                WriteOutput(output, "EvaluationOutput" + settingPair.Key + ".csv");
            }
            SaveContextDictionary(contextDictionary);
        }

        private static void SaveContextDictionary(Dictionary<string, List<Tuple<Context, DateTime?>>> contextDictionary)
        {
            Console.WriteLine("Save context dictionary (y or n)?");
            var answer = Console.ReadLine();
            if (!string.IsNullOrEmpty(answer) && answer.Equals("y"))
            {
                var contextDictionaryAsJson = contextDictionary.ToCompactJson();
                File.WriteAllText(Path.Combine(RootDir, "contextDictionary.json"), contextDictionaryAsJson);
            }
        }
        
        private static string EvaluateSettings(Dictionary<string, List<Tuple<Context, DateTime?>>> contextDictionary, Settings referenceSettings, Settings settingsToEvaluate)
        {
            var evaluation = new Evaluation(contextDictionary, referenceSettings, settingsToEvaluate);
            var output = evaluation.ProcessEvaluation();
            output = output.Insert(0,
                string.Format("Evaluated Settings: {0}{1}{2}", Environment.NewLine, GetSelectedSettingsAsString(settingsToEvaluate),
                    Environment.NewLine));
            output = output.Insert(0,
                string.Format("Reference Settings: {0}{1}{2}", Environment.NewLine, GetSelectedSettingsAsString(referenceSettings),
                    Environment.NewLine));
            return output;
        }

        private static Settings SettingSelection()
        {
            var settings = new Settings();
            var settingType = typeof(Settings);
            var settingProperties = settingType.GetProperties().Where(property => property.PropertyType == typeof(bool)).ToList();
            Console.WriteLine("Setting selection:");
            for (int i = 0; i < settingProperties.Count(); i++)
            {
                Console.WriteLine("{0} - {1}",i,settingProperties[i].Name);
            }
            Console.WriteLine("Choose which heuristics should be activated by typing in the number and seperate multiple with a comma");
            var input = Console.ReadLine();
            if (!string.IsNullOrEmpty(input))
            {
                var selectedSettings = input.Split(',').Select(int.Parse);
                foreach (var selectedSetting in selectedSettings)
                {
                    if(selectedSetting < settingProperties.Count) settingProperties[selectedSetting].SetValue(settings,true);
                }
            }
            return settings;
        }

        public static string GetSelectedSettingsAsString(Settings settings)
        {
            var settingType = typeof(Settings);
            var settingProperties = settingType.GetProperties().Where(property => property.PropertyType == typeof(bool)).ToList();
            var sb = new StringBuilder();
            foreach (var settingProperty in settingProperties)
            {
                if ((bool) settingProperty.GetValue(settings)) sb.AppendLine(settingProperty.Name);
            }

            return sb.ToString();
        }

        public static string GetSelectedSettingsAsNumbersString(Settings settings)
        {
            var settingProperties =
                typeof (Settings).GetProperties().Where(property => property.PropertyType == typeof (bool)).ToList();
            var activatedSettingsString = "";
            for (int i = 0; i < settingProperties.Count; i++)
            {
                if ((bool) settingProperties[i].GetValue(settings)) activatedSettingsString += i;
            }
            return activatedSettingsString;
        }

        private static Dictionary<string, List<Tuple<Context, DateTime?>>> ReadContexts()
        {
            Dictionary<string, List<Tuple<Context, DateTime?>>> contextDictionary;
            var pathToContext = Path.Combine(RootDir, "contextDictionary.json");
            if (File.Exists(pathToContext))
            {
                var text = File.ReadAllText(pathToContext);
                contextDictionary = text.ParseJsonTo<Dictionary<string, List<Tuple<Context, DateTime?>>>>();
                return contextDictionary;
            }
            var contextReader = new ContextReader();
            var zips = Directory.EnumerateFiles(RootDir).Where(file => file.EndsWith(".zip"));
            Console.WriteLine("Now reading contexts from Zips - this can take a while");
            contextReader.ReadContextsFromAllZipFiles(zips);
            contextDictionary = contextReader.ContextDictionary;
            return contextDictionary;
        }
    }
}
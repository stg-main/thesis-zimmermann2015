﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DiffTestBench.Diff;
using DiffTestBench.Import;
using KaVE.Commons.Model.Events.CompletionEvents;
using KaVE.Commons.Utils.Json;
using SSTDiff;
using SSTDiff.EditScript.EditOperations;
using SSTDiff.Util;

namespace DiffTestBench
{
    public class EvaluationDataCreator
    {
        private const int TriesTillSearchUnqiueDiffBreaks = 10000;

        private List<Tuple<string, Context, DateTime>> _contextDictionary;
        private EvaluationSettings _evaluationSettings;
        private Settings _referenceSettings;

        protected Dictionary<string, Settings> SettingsToEvaluate = new Dictionary<string, Settings>
        {
            {
                "Wrapping-Unwrapping Detection",
                new Settings
                {
                    ActivateWrappingDetection = true,
                    ActivateUnwrappingDetection = true
                }
            },
            {
                "Block Move Detection",
                new Settings
                {
                    ActivateMoveOfBlockDetection = true
                }
            },
            {
                "Join Insertion-Deletions",
                new Settings
                {
                    ActivateJoinInsertsAndDelete = true
                }
            }
        };

        public void CreateData()
        {
            var pathToSettings = Path.Combine(Program.RootDir, "EvaluationSettings");
            _evaluationSettings = File.ReadAllText(pathToSettings).ParseJsonTo<EvaluationSettings>();

            ImportContexts();

            Console.WriteLine("Contexts read");
            
            _referenceSettings = new Settings
            {
                ActivateEarlyHashcodeMatchingForMethodDeclarations = true,
                ActivateEarlyHashcodeMatchingForSST = true,
                ActivateMatchingWithParent = true
            };

            var random = new Random(_evaluationSettings.DefaultSeed);

            // Completeness
            var diffsToEvaluate = GenerateDiffToEvaluate(random);
            SaveDiffList(diffsToEvaluate, "Completeness");

            Console.WriteLine("Finished Completeness Diff List");

            foreach (var settings in SettingsToEvaluate)
            {
                diffsToEvaluate = GenerateDiffToEvaluate(random, settings.Value);
                SaveDiffList(diffsToEvaluate, settings.Key);
                Console.WriteLine("Finished " + settings.Key);
            }
        }

        private void SaveDiffList(
            List<Tuple<Tuple<string, Context, DateTime>, Tuple<string, Context, DateTime>>> diffList, string fileName)
        {
            var diffListAsJson = diffList.ToCompactJson();
            File.WriteAllText(Path.Combine(_evaluationSettings.Path, fileName + ".json"), diffListAsJson);
        }

        private List<Tuple<Tuple<string, Context, DateTime>, Tuple<string, Context, DateTime>>> GenerateDiffToEvaluate(
            Random random, Settings settingsToEvaluate = null)
        {
            var diffsToEvaluate =
                new HashSet<Tuple<Tuple<string, Context, DateTime>, Tuple<string, Context, DateTime>>>();
            for (var i = 0; i < _evaluationSettings.EvaluationAmount; i++)
            {
                Tuple<Tuple<string, Context, DateTime>, Tuple<string, Context, DateTime>> tupleToAdd;
                if (!TryGetUniqueDiffTuple(random, diffsToEvaluate, out tupleToAdd, settingsToEvaluate)) return null;
                diffsToEvaluate.Add(tupleToAdd);
            }

            return diffsToEvaluate.ToList();
        }

        private bool TryGetUniqueDiffTuple(Random random,
            HashSet<Tuple<Tuple<string, Context, DateTime>, Tuple<string, Context, DateTime>>> diffsToEvaluate,
            out Tuple<Tuple<string, Context, DateTime>, Tuple<string, Context, DateTime>> tupleToAdd,
            Settings settingsToEvaluate = null)
        {
            var diffFound = false;
            var tries = 0;
            Tuple<Tuple<string, Context, DateTime>, Tuple<string, Context, DateTime>> tempTuple;
            do
            {
                if (tries > TriesTillSearchUnqiueDiffBreaks)
                {
                    tupleToAdd = null;
                    return false;
                }
                var index = random.Next(1, _contextDictionary.Count - 2);

                // diff is linear -> either compares previous or next context

                var contextTuple = _contextDictionary[index];
                var nextContextTuple = _contextDictionary[index + 1];
                var previousContextTuple = _contextDictionary[index - 1];

                tempTuple = nextContextTuple.Item2.SST.EnclosingType.Equals(contextTuple.Item2.SST.EnclosingType)
                    ? Tuple.Create(contextTuple, nextContextTuple)
                    : Tuple.Create(previousContextTuple, contextTuple);


                if (tempTuple.Item1.Item2.SST.GetHashCode() != tempTuple.Item2.Item2.SST.GetHashCode() &&
                    !diffsToEvaluate.Contains(tempTuple))
                {
                    if (settingsToEvaluate == null) diffFound = true;
                    else
                    {
                        string diffOutputOriginal;
                        string diffOutputEvaluate;
                        List<IEditOperation> referenceEditscript;
                        List<IEditOperation> improvedEditscript;
                        var successReference = DiffGenerator.GenerateDiff(_referenceSettings,
                            tempTuple.Item1.Item2.SST.DeepCloneNode(), tempTuple.Item2.Item2.SST.DeepCloneNode(),
                            out diffOutputOriginal, out referenceEditscript);
                        var successEvaluate = DiffGenerator.GenerateDiff(settingsToEvaluate,
                            tempTuple.Item1.Item2.SST.DeepCloneNode(), tempTuple.Item2.Item2.SST.DeepCloneNode(),
                            out diffOutputEvaluate, out improvedEditscript);
                        if (successReference && successEvaluate)
                        {
                            if (improvedEditscript.Count < referenceEditscript.Count) diffFound = true;
                        }
                    }
                }

                tries++;
            } while (!diffFound);
            tupleToAdd = tempTuple;
            return true;
        }

        private void ImportContexts()
        {
            if (Directory.Exists(_evaluationSettings.Path))
            {
                var contextReader = new ContextReader();
                var zips = Directory.EnumerateFiles(_evaluationSettings.Path).Where(file => file.EndsWith(".zip"));
                contextReader.ReadContextsFromAllZipFiles(zips);
                var contextDictionary = contextReader.ContextDictionary;
                TransformContextDictionary(contextDictionary);
            }
        }

        private void TransformContextDictionary(Dictionary<string, List<Tuple<Context, DateTime?>>> contextDictionary)
        {
            _contextDictionary = new List<Tuple<string, Context, DateTime>>();
            foreach (var keyValuePair in contextDictionary)
            {
                var contextList =
                    keyValuePair.Value.Where(pair => pair.Item2.HasValue)
                        .Select(pair => Tuple.Create(keyValuePair.Key, pair.Item1, pair.Item2.Value))
                        .ToList();
                // only adds contextList with more than one because else no diffs are possible
                if (contextList.Count > 1) _contextDictionary.AddRange(contextList);
            }
        }
    }
}
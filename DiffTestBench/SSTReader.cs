/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using KaVE.Commons.Model.SSTs.Impl;
using KaVE.Commons.Utils.Json;

namespace DiffTestBench
{
    public class SSTReader
    {
        private readonly string _aDir;
        private readonly string _bDir;
        private readonly string _rootDir;

        public SSTReader(string rootDir, string aDir, string bDir)
        {
            _rootDir = rootDir;
            _aDir = aDir;
            _bDir = bDir;
        }

        public Dictionary<Tuple<string, SST>, Tuple<string, SST>> ReadSsts()
        {
            var jsonFilesA = GetJsonFileList(Path.Combine(_rootDir, _aDir));
            var jsonFilesB = GetJsonFileList(Path.Combine(_rootDir, _bDir));

            var sstsDictionary = new Dictionary<Tuple<string, SST>, Tuple<string, SST>>();

            var sstFilePaths = new List<Tuple<string, string>>();

            foreach (var jsonFileFromA in jsonFilesA)
            {
                var jsonFileFromB = jsonFilesB.FirstOrDefault(sstPath =>
                {
                    var fileName = Path.GetFileName(sstPath);
                    return fileName != null && fileName.Equals(Path.GetFileName(jsonFileFromA));
                });
                if (!string.Empty.Equals(jsonFileFromB))
                    sstFilePaths.Add(Tuple.Create(jsonFileFromA, jsonFileFromB));
            }

            foreach (var filePair in sstFilePaths)
            {
                var sstA = ReadAndParseSST(filePair.Item1);
                var sstB = ReadAndParseSST(filePair.Item2);
                sstsDictionary.Add(Tuple.Create(filePair.Item1, sstA),
                    Tuple.Create(filePair.Item2, sstB));
            }

            return sstsDictionary;
        }

        public static List<string> GetJsonFileList(string directoryPath)
        {
            var files = Directory.GetFiles(directoryPath);
            var jsonFiles = files.Where(file => file.EndsWith(".json")).ToList();
            return jsonFiles;
        }

        public static SST ReadAndParseSST(string filePath)
        {
            var sstAsJson = File.ReadAllText(filePath);
            var sst = sstAsJson.ParseJsonTo<SST>();
            return sst;
        }
    }
}
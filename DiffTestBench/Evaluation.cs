﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DiffTestBench.Diff;
using KaVE.Commons.Model.Events.CompletionEvents;
using SSTDiff;
using SSTDiff.EditScript.EditOperations;
using SSTDiff.Util;

namespace DiffTestBench
{
    public class Evaluation
    {
        private readonly Dictionary<string, List<Tuple<Context, DateTime?>>> _contextDictionary;
        private readonly Settings _defaultSettings;
        private readonly Settings _settingsToEvaluate;

        public Evaluation(Dictionary<string, List<Tuple<Context, DateTime?>>> contextDictionary,
            Settings defaultSettings, Settings settingsToEvaluate)
        {
            _contextDictionary = contextDictionary;
            _settingsToEvaluate = settingsToEvaluate;
            _defaultSettings = defaultSettings;
        }

        public string ProcessEvaluation()
        {
            var data = ObtainEvaluationData();
            return AnalyzeData(data);
        }

        public Dictionary<string, Tuple<List<IEditOperation>, List<IEditOperation>>> ObtainEvaluationData()
        {
            var numberOfClasses = GetNumberOfClasses();
            var keyValuePairs = _contextDictionary.ToList();
            var data = new Dictionary<string, Tuple<List<IEditOperation>, List<IEditOperation>>>();
            for (var i = 0; i < keyValuePairs.Count; i++)
            {
                Console.WriteLine("Class {0} of {1}", i + 1, numberOfClasses);
                var pair = keyValuePairs[i];
                var contextList =
                    pair.Value.Where(tuple => tuple.Item2.HasValue).OrderBy(tuple => tuple.Item2.Value.Ticks).ToList();
                var index = 0;
                while (index < contextList.Count - 1)
                {
                    string diffOutputOriginal;
                    string diffOutputEvaluate;
                    List<IEditOperation> originalEditscript;
                    List<IEditOperation> improvedEditscript;
                    Console.WriteLine("Diff {0} of {1}", index + 1, contextList.Count - 1);
                    var successOriginal = DiffGenerator.GenerateDiff(_defaultSettings,
                        contextList[index].Item1.SST.DeepCloneNode(), contextList[index + 1].Item1.SST.DeepCloneNode(),
                        out diffOutputOriginal, out originalEditscript);
                    var successEvaluate = DiffGenerator.GenerateDiff(_settingsToEvaluate,
                        contextList[index].Item1.SST.DeepCloneNode(), contextList[index + 1].Item1.SST.DeepCloneNode(),
                        out diffOutputEvaluate, out improvedEditscript);
                    if (successOriginal && successEvaluate)
                    {
                        data.Add(
                            string.Format("{0} {1}/{2}", pair.Key, contextList[index].Item2?.ToString("G"),
                                contextList[index + 1].Item2?.ToString("G")), Tuple.Create(originalEditscript, improvedEditscript));
                    }
                    index++;
                }
            }
            return data;
        }

        public string AnalyzeData(Dictionary<string, Tuple<List<IEditOperation>, List<IEditOperation>>> data)
        {
            var sb = new StringBuilder();
            sb.AppendLine("Evaluation Data:");
            sb.AppendLine(
                "DiffPair;OriginalDiffEditScriptCount;ImprovedDiffEditScriptCount;InsertOriginal;InsertImproved;DeleteOriginal;DeleteImproved;MoveOriginal;MoveImproved;UpdateOriginal;UpdateImproved");
            var countOriginalEqualImproved = 0;
            var countOriginalBetter = 0;
            var countImprovedBetter = 0;
            var countAllDiffs = 0;
            foreach (var keyPair in data)
            {
                var originalEditscript = keyPair.Value.Item1;
                var improvedEditscript = keyPair.Value.Item2;
                var originalInsertsCount = originalEditscript.OfType<Insert>().Count();
                var improvedInsertsCount = improvedEditscript.OfType<Insert>().Count();
                var originalDeletesCount = originalEditscript.OfType<Delete>().Count();
                var improvedDeletesCount = improvedEditscript.OfType<Delete>().Count();
                var originalMovesCount = originalEditscript.OfType<Move>().Count();
                var improvedMovesCount = improvedEditscript.OfType<Move>().Count();
                var originalUpdatesCount = originalEditscript.OfType<Update>().Count();
                var improvedUpdatesCount = improvedEditscript.OfType<Update>().Count();
                sb.AppendFormat("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10}", keyPair.Key, originalEditscript.Count,
                    improvedEditscript.Count, originalInsertsCount, improvedInsertsCount, originalDeletesCount,
                    improvedDeletesCount, originalMovesCount, improvedMovesCount, originalUpdatesCount,
                    improvedUpdatesCount);
                sb.AppendLine();
                var originalEditScriptCount = originalEditscript.Count;
                var improvedEditScriptCount = improvedEditscript.Count;
                countAllDiffs++;
                if (originalEditScriptCount == improvedEditScriptCount)
                {
                    countOriginalEqualImproved++;
                }
                else if (originalEditScriptCount < improvedEditScriptCount)
                {
                    countOriginalBetter++;
                }
                else
                {
                    countImprovedBetter++;
                }
            }
            sb.AppendLine();
            sb.AppendLine("Analysis:");
            sb.AppendFormat("Number of Diffs processed {0}", countAllDiffs);
            sb.AppendLine();
            sb.AppendFormat("Number of Diffs where EditScript of Original is equal to Improved {0}",
                countOriginalEqualImproved);
            sb.AppendLine();
            sb.AppendFormat("Number of Diffs where EditScript of Original is better than Improved {0}",
                countOriginalBetter);
            sb.AppendLine();
            sb.AppendFormat("Number of Diffs where EditScript of Improved is better than Original {0}",
                countImprovedBetter);
            sb.AppendLine();

            return sb.ToString();
        }

        private int GetNumberOfClasses()
        {
            return _contextDictionary.Count;
        }
    }
}
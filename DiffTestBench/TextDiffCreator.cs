﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using KaVE.Commons.Model.SSTs;
using KaVE.Commons.Model.SSTs.Impl;
using KaVE.Commons.Utils.SSTPrinter;

namespace DiffTestBench
{
    public class TextDiffCreator : IDiffCreator
    {
        private const string PrintedSstFileExtension = ".printed.txt";
        private static readonly SSTPrintingVisitor SstPrinter = new SSTPrintingVisitor();
        private readonly string _rootDir;

        public TextDiffCreator(string rootDir)
        {
            _rootDir = rootDir;
        }

        public string CreateDiff(Dictionary<Tuple<string, SST>, Tuple<string, SST>> sstDictionary)
        {
            var printedSstPairs = PrintSsts(sstDictionary);
            var printedSstFilePaths = WritePrintedSsts(printedSstPairs);
            var diffOut = CreateDiffOutput(printedSstFilePaths);
            return diffOut;
        }

        private Dictionary<Tuple<string, string>, Tuple<string, string>> PrintSsts(
            Dictionary<Tuple<string, SST>, Tuple<string, SST>> sstDictionary)
        {
            var result = new Dictionary<Tuple<string, string>, Tuple<string, string>>();
            foreach (var sstPair in sstDictionary)
            {
                var printedSstA = PrintSst(sstPair.Key.Item2);
                var printedSstB = PrintSst(sstPair.Value.Item2);
                result.Add(Tuple.Create(sstPair.Key.Item1, printedSstA), Tuple.Create(sstPair.Value.Item1, printedSstB));
            }
            return result;
        }

        private string PrintSst(ISST sst)
        {
            var sstPrintingContext = new SSTPrintingContext();
            SstPrinter.Visit(sst, sstPrintingContext);
            return sstPrintingContext.ToString();
        }

        private List<Tuple<string, string>> WritePrintedSsts(
            Dictionary<Tuple<string, string>, Tuple<string, string>> printedSstPairs)
        {
            var printedSstFilePaths = new List<Tuple<string, string>>();
            foreach (var printedSstPair in printedSstPairs)
            {
                var pathA = printedSstPair.Key.Item1 + PrintedSstFileExtension;
                var pathB = printedSstPair.Value.Item1 + PrintedSstFileExtension;
                WritePrintedSst(pathA, printedSstPair.Key.Item2);
                WritePrintedSst(pathB, printedSstPair.Value.Item2);
                printedSstFilePaths.Add(Tuple.Create(pathA, pathB));
            }
            return printedSstFilePaths;
        }

        private void WritePrintedSst(string filePath, string printedSst)
        {
            File.WriteAllText(filePath, printedSst);
        }

        private string CreateDiffOutput(List<Tuple<string, string>> printedSstFilePathPairs)
        {
            var sb = new StringBuilder();

            foreach (var printedSstFilePathPair in printedSstFilePathPairs)
            {
                var printedSstFilePaths = string.Format("\"{0}\" \"{1}\"", printedSstFilePathPair.Item1,
                    printedSstFilePathPair.Item2);
                sb.AppendLine(printedSstFilePaths);
                sb.AppendLine();
                var output = StartDiffProcess(printedSstFilePaths);
                sb.AppendLine(string.Empty.Equals(output) ? "Files are equal" : output);
                sb.AppendLine();
            }

            return sb.ToString();
        }

        private string StartDiffProcess(string printedSstFilePaths)
        {
            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = Path.Combine(_rootDir, "diff.exe"),
                    Arguments = printedSstFilePaths,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true,
                    WorkingDirectory = _rootDir
                }
            };
            process.Start();
            var output = process.StandardOutput;
            return output.ReadToEnd();
        }
    }
}
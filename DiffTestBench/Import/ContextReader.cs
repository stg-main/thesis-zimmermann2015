﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ICSharpCode.SharpZipLib.Zip;
using KaVE.Commons.Model.Events;
using KaVE.Commons.Model.Events.CompletionEvents;
using KaVE.Commons.Utils.Json;

namespace DiffTestBench.Import
{
    public class ContextReader
    {
        public const string RootDir = @"C:\Diff Tests\";

        public List<string> ZipFiles;

        private readonly Dictionary<string,List<Tuple<Context, DateTime?>>> _zipNameContextDictionary;

        public Dictionary<string,List<Tuple<Context,DateTime?>>> ContextDictionary;

        public ContextReader()
        {
            ZipFiles = new List<string>();
            _zipNameContextDictionary = new Dictionary<string, List<Tuple<Context,DateTime?>>>();
            ContextDictionary = new Dictionary<string, List<Tuple<Context, DateTime?>>>();
        }

        public void ReadContextsFromAllZipFiles(IEnumerable<string> selectedZips)
        {
            ZipFiles.AddRange(selectedZips);
            ZipFiles.ForEach(ReadContextFromZipFile);
            GenerateFinalContextDictionary();
        }

        public void ReadContextsFromSaveFile(string filePath)
        {
            var text = File.ReadAllText(filePath);
            ContextDictionary = text.ParseJsonTo<Dictionary<string, List<Tuple<Context, DateTime?>>>>();
        }

        public static IEnumerable<string> FindZipFiles(string folderPath)
        {
            return Directory.GetFiles(folderPath).Where(path => path.EndsWith(".zip"));
        }

        public void ReadContextFromZipFile(string pathToZipFile)
        {
            var zipName = Path.GetFileNameWithoutExtension(pathToZipFile);
            using (var zipInputStream = new ZipFile(pathToZipFile))
            {
                var strings = ReadAllContentsAsStrings(zipInputStream);
                var events = GetAllEventsFromStrings(strings);
                var contexts = GetContexts(events);
                if (zipName != null) _zipNameContextDictionary.Add(zipName, contexts);
            }
        }

        public IEnumerable<IDEEvent> GetAllEventsFromStrings(IEnumerable<string> strings)
        {
            return strings.Select(str => str.ParseJsonTo<IDEEvent>());
        }

        public IEnumerable<string> ReadAllContentsAsStrings(ZipFile zipFile)
        {
            foreach (ZipEntry entry in zipFile)
            {
                using (var reader = new StreamReader(zipFile.GetInputStream(entry)))
                {
                    yield return reader.ReadToEnd();
                }
            }
        }

        public List<Tuple<Context,DateTime?>> GetContexts(IEnumerable<IDEEvent> events)
        {
            var completionEvents = events.Where(ideEvent => ideEvent is CompletionEvent).Cast<CompletionEvent>().ToList();
            completionEvents = FilterCompletionEvents(completionEvents);
            return completionEvents.Select(completionEvent => Tuple.Create(completionEvent.Context2,completionEvent.TriggeredAt)).ToList();
        }

        private List<CompletionEvent> FilterCompletionEvents(List<CompletionEvent> completionEvents)
        {
            return completionEvents.Where(completionEvent => !completionEvent.Context2.IsDefault()).ToList();
        }

        public void GenerateFinalContextDictionary()
        {
            foreach (var zipNameContextPair in _zipNameContextDictionary)
            {
                foreach (var tuple in zipNameContextPair.Value)
                {
                    var name = string.Format("{0} {1}", zipNameContextPair.Key, tuple.Item1.SST.EnclosingType.Name);
                    if(ContextDictionary.ContainsKey(name)) ContextDictionary[name].Add(tuple);
                    else ContextDictionary.Add(name, new List<Tuple<Context, DateTime?>> { tuple });
                }
            }

            FilterLowDeltaTimes();
        }

        private void FilterLowDeltaTimes()
        {
            var newContextDictionary = new Dictionary<string, List<Tuple<Context, DateTime?>>>();
            foreach (var contextKeyValuePair in ContextDictionary)
            {
                var newContextList = new List<Tuple<Context, DateTime?>>();
                var currentBlock = new List<Tuple<Context,DateTime?>>();
                for (var index = 0; index < contextKeyValuePair.Value.Count; index++)
                {
                    var contextTuple = contextKeyValuePair.Value[index];
                    if (currentBlock.Count == 0)
                    {
                        currentBlock.Add(contextTuple);
                        continue;
                    }
                    
                    // ReSharper disable once PossibleInvalidOperationException
                    
                    var diffInTicks = (currentBlock.Last().Item2 - contextTuple.Item2).Value.Ticks;
                    var diff = TimeSpan.FromTicks(Math.Abs(diffInTicks));

                    if (index == contextKeyValuePair.Value.Count - 1)
                    {
                        newContextList.Add(currentBlock.First());
                        if (diff > TimeSpan.FromMinutes(1.0))
                        {
                            if (currentBlock.Count > 1)  newContextList.Add(currentBlock.Last());
                            newContextList.Add(contextTuple);
                        }
                        else
                        {
                            newContextList.Add(contextTuple);
                        }
                    }
                    else if (diff > TimeSpan.FromMinutes(1.0) )
                    {
                        if (currentBlock.Count > 1)
                        {
                            newContextList.Add(currentBlock.First());
                            newContextList.Add(currentBlock.Last());
                        }
                        else
                        {
                            newContextList.Add(currentBlock.First());
                        }
                        currentBlock.Clear();
                    }
                    else
                    {
                        currentBlock.Add(contextTuple);
                    }
                }
                newContextDictionary.Add(contextKeyValuePair.Key,newContextList);
            }
            ContextDictionary = newContextDictionary;
        }
    }
}
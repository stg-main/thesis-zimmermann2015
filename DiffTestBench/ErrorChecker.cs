﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DiffTestBench.Diff;
using KaVE.Commons.Model.Events.CompletionEvents;
using SSTDiff;
using SSTDiff.EditScript.EditOperations;
using SSTDiff.Util;

namespace DiffTestBench
{
    public class ErrorChecker
    {
        private readonly Dictionary<string, List<Tuple<Context, DateTime?>>> _contextDictionary;
        private readonly Settings _settings;

        public ErrorChecker(Dictionary<string, List<Tuple<Context, DateTime?>>> contextDictionary, Settings settings)
        {
            _contextDictionary = contextDictionary;
            _settings = settings;
        }

        public string ProcessErrorCheck()
        {
            var sb = new StringBuilder();
            var numberOfClasses = GetNumberOfClasses();
            var errorCount = 0;
            var diffCount = 0;
            var keyValuePairs = _contextDictionary.ToList();
            for (int i = 0; i < keyValuePairs.Count; i++)
            {
                Console.WriteLine("Class {0} of {1}",i+1,numberOfClasses);
                var pair = keyValuePairs[i];
                var contextList = pair.Value.Where(tuple => tuple.Item2.HasValue).OrderBy(tuple => tuple.Item2.Value.Ticks).ToList();
                int index = 0;
                while (index < contextList.Count - 1)
                {
                    string diffOutput;
                    List<IEditOperation> editscript;
                    Console.WriteLine("Diff {0} of {1}", index + 1, contextList.Count - 1);
                    var success = DiffGenerator.GenerateDiff(_settings, contextList[index].Item1.SST.DeepCloneNode(), contextList[index+1].Item1.SST.DeepCloneNode(),out diffOutput, out editscript);
                    diffCount++;
                    if (!success)
                    {
                        sb.AppendFormat("Class {0} Context {1} / {2} Output: {3}", pair.Key, contextList[index].Item2?.ToString("G"),
                            contextList[index + 1].Item2?.ToString("G"), diffOutput);
                        sb.AppendLine();
                        errorCount++;
                    }
                    index++;
                }
                // other direction - deactivated
                //index = contextList.Count - 1;
                //while (index > 0)
                //{
                //    Console.WriteLine("Context {0} of {1}", index + 1, contextList.Count - 1", contextList.Count, index + 1);
                //    var diffOutput = DiffGenerator.GenerateDiff(_settings, contextList[index].Item1.SST, contextList[index - 1].Item1.SST);
                //    sb.AppendFormat("Class {0} Context {1} / {2} Output: {3}", pair.Key, contextList[index].Item2, contextList[index - 1].Item2, diffOutput);
                //    sb.AppendLine();
                //    index--;
                //}
            }
            sb.Insert(0, string.Format("Error Count: {0} in {1} proccessed Diffs{2}", errorCount, diffCount, Environment.NewLine));
            return sb.ToString();
        }

        private int GetNumberOfClasses()
        {
            return _contextDictionary.Count;
        }
    }
}
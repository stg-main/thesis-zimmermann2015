﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using KaVE.Commons.Model.SSTs.Impl.Declarations;
using KaVE.Commons.Model.SSTs.Impl.References;
using KaVE.Commons.Model.SSTs.Impl.Statements;
using NUnit.Framework;
using SSTDiff.EditScript.EditOperations;
using SSTDiff.Util;

namespace SSTDiff.Tests.EditScript.EditOperations
{
    public class DeleteTest
    {
        [Test]
        public void EqualTest()
        {
            var node1 = new UnknownReference();
            var node2 = new UnknownReference();

            var delete1 = new Delete { DeletedNode = node1, ParentNode = node2, ChildPosition = 2 };
            var delete2 = new Delete { DeletedNode = node1, ParentNode = node2, ChildPosition = 2 };
            var delete3 = new Delete { DeletedNode = node1, ParentNode = node2, ChildPosition = 3 };
            var delete4 = new Delete { DeletedNode = node1, ParentNode = new UnknownStatement(), ChildPosition = 2 };
            var delete5 = new Delete { DeletedNode = new UnknownStatement(), ParentNode = node2, ChildPosition = 2 };

            var differentObject = "Test";

            Assert.False(delete1.Equals(null));
            // ReSharper disable once EqualExpressionComparison
            Assert.True(delete1.Equals(delete1));
            Assert.True(delete1.Equals(delete2));
            Assert.False(delete1.Equals(delete3));
            Assert.False(delete1.Equals(delete4));
            Assert.False(delete1.Equals(delete5));
            // ReSharper disable once SuspiciousTypeConversion.Global
            Assert.False(delete1.Equals(differentObject));
        }

        [Test]
        public void GetHashCodeTest()
        {
            var node1 = new UnknownReference();
            var node2 = new UnknownReference();

            var delete1 = new Delete { DeletedNode = node1, ParentNode = node2, ChildPosition = 2 };
            var delete2 = new Delete { DeletedNode = node1, ParentNode = node2, ChildPosition = 2 };
            var delete3 = new Delete { DeletedNode = node2, ParentNode = node1, ChildPosition = 3 };

            Assert.AreEqual(delete1.GetHashCode(), delete2.GetHashCode());
            Assert.AreNotEqual(delete1.GetHashCode(), delete3.GetHashCode());
        }

        [Test]
        public void ContainsNodeTest()
        {
            var node1 = new UnknownStatement();
            var node2 = new UnknownStatement();

            var delete = new Delete {ChildPosition = 2, DeletedNode = node1, ParentNode = new MethodDeclaration()};

            Assert.True(delete.ContainsNode(node1));
            Assert.False(delete.ContainsNode(node2));
        }

        [Test]
        public void EditOperationPropertyTest()
        {
            Assert.AreEqual(new Delete().EditOperation, EditOperationEnum.Delete);
        }
        
        [Test]
        public void ToStringTest()
        {
            var node1 = new UnknownReference();
            var node2 = new UnknownReference();

            var delete = new Delete{ DeletedNode = node1, ParentNode = node2, ChildPosition = 2 };

            var expected = string.Format("Delete : [DeletedNode: {0}, ParentNode: {1}, ChildPosition: {2}]", node1.GetPrintValue(),
                node2.GetPrintValue(), 2);

            Assert.AreEqual(expected, delete.ToString());
        }
    }
}
﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using KaVE.Commons.Model.SSTs.Impl.Declarations;
using KaVE.Commons.Model.SSTs.Impl.References;
using KaVE.Commons.Model.SSTs.Impl.Statements;
using NUnit.Framework;
using SSTDiff.EditScript.EditOperations;
using SSTDiff.Util;

namespace SSTDiff.Tests.EditScript.EditOperations
{
    public class InsertTest
    {
        [Test]
        public void EqualTest()
        {
            var node1 = new UnknownReference();
            var node2 = new UnknownReference();

            var insert1 = new Insert {InsertedNode = node1, ParentNode = node2, ChildPosition = 2};
            var insert2 = new Insert {InsertedNode = node1, ParentNode = node2, ChildPosition = 2};
            var insert3 = new Insert {InsertedNode = node1, ParentNode = node2, ChildPosition = 3};
            var insert4 = new Insert {InsertedNode = node1, ParentNode = new UnknownStatement(), ChildPosition = 2};
            var insert5 = new Insert {InsertedNode = new UnknownStatement(), ParentNode = node2, ChildPosition = 2};

            var differentObject = "Test";

            Assert.False(insert1.Equals(null));
            // ReSharper disable once EqualExpressionComparison
            Assert.True(insert1.Equals(insert1));
            Assert.True(insert1.Equals(insert2));
            Assert.False(insert1.Equals(insert3));
            Assert.False(insert1.Equals(insert4));
            Assert.False(insert1.Equals(insert5));
            // ReSharper disable once SuspiciousTypeConversion.Global
            Assert.False(insert1.Equals(differentObject));
        }

        [Test]
        public void GetHashCodeTest()
        {
            var node1 = new UnknownReference();
            var node2 = new UnknownReference();

            var insert1 = new Insert {InsertedNode = node1, ParentNode = node2, ChildPosition = 2};
            var insert2 = new Insert {InsertedNode = node1, ParentNode = node2, ChildPosition = 2};
            var insert3 = new Insert {InsertedNode = node2, ParentNode = node1, ChildPosition = 3};

            Assert.AreEqual(insert1.GetHashCode(), insert2.GetHashCode());
            Assert.AreNotEqual(insert1.GetHashCode(), insert3.GetHashCode());
        }

        [Test]
        public void ContainsNodeTest()
        {
            var node1 = new UnknownStatement();
            var node2 = new UnknownStatement();

            var insert = new Insert { ChildPosition = 2, InsertedNode = node1, ParentNode = new MethodDeclaration() };

            Assert.True(insert.ContainsNode(node1));
            Assert.False(insert.ContainsNode(node2));
        }

        [Test]
        public void EditOperationPropertyTest()
        {
            Assert.AreEqual(new Insert().EditOperation, EditOperationEnum.Insert);
        }

        [Test]
        public void ToStringTest()
        {
            var node1 = new UnknownReference();
            var node2 = new UnknownReference();

            var insert = new Insert {InsertedNode = node1, ParentNode = node2, ChildPosition = 2};

            var expected = string.Format("Insert : [InsertedNode: {0}, ParentNode: {1}, ChildPosition: {2}]", node1.GetPrintValue(),
                node2.GetPrintValue(), 2);

            Assert.AreEqual(expected, insert.ToString());
        }
    }
}
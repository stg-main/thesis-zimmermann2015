﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using KaVE.Commons.Model.SSTs.Impl.Declarations;
using KaVE.Commons.Model.SSTs.Impl.References;
using KaVE.Commons.Model.SSTs.Impl.Statements;
using NUnit.Framework;
using SSTDiff.EditScript.EditOperations;
using SSTDiff.Util;

namespace SSTDiff.Tests.EditScript.EditOperations
{
    public class MoveTest
    {
        [Test]
        public void EqualTest()
        {
            var node1 = new UnknownReference();
            var node2 = new UnknownReference();
            var node3 = new UnknownReference();

            var move1 = new Move {MovedNode = node1, ParentNode = node2, OldParentNode = node3, ChildPosition = 2};
            var move2 = new Move {MovedNode = node1, ParentNode = node2, OldParentNode = node3, ChildPosition = 2};
            var move3 = new Move {MovedNode = node1, ParentNode = node2, OldParentNode = node3, ChildPosition = 3};
            var move4 = new Move { MovedNode = node1, ParentNode = new UnknownStatement(), OldParentNode = node3, ChildPosition = 2 };
            var move5 = new Move { MovedNode = new UnknownStatement(), ParentNode = node2, OldParentNode = node3, ChildPosition = 2 };
            var move6 = new Move { MovedNode = node1, ParentNode = node2, OldParentNode = new UnknownStatement(), ChildPosition = 2 };

            var differentObject = "Test";

            Assert.False(move1.Equals(null));
            // ReSharper disable once EqualExpressionComparison
            Assert.True(move1.Equals(move1));
            Assert.True(move1.Equals(move2));
            Assert.False(move1.Equals(move3));
            Assert.False(move1.Equals(move4));
            Assert.False(move1.Equals(move5));
            Assert.False(move1.Equals(move6));
            // ReSharper disable once SuspiciousTypeConversion.Global
            Assert.False(move1.Equals(differentObject));
        }

        [Test]
        public void GetHashCodeTest()
        {
            var node1 = new UnknownReference();
            var node2 = new UnknownReference();
            var node3 = new UnknownReference();

            var move1 = new Move { MovedNode = node1, ParentNode = node2, OldParentNode = node3, ChildPosition = 2 };
            var move2 = new Move { MovedNode = node1, ParentNode = node2, OldParentNode = node3, ChildPosition = 2 };
            var move3 = new Move { MovedNode = node2, ParentNode = node3, OldParentNode = node1, ChildPosition = 3 };

            Assert.AreEqual(move1.GetHashCode(), move2.GetHashCode());
            Assert.AreNotEqual(move1.GetHashCode(), move3.GetHashCode());
        }

        [Test]
        public void ContainsNodeTest()
        {
            var node1 = new UnknownStatement();
            var node2 = new UnknownStatement();

            var move = new Move { ChildPosition = 2, MovedNode = node1, ParentNode = new MethodDeclaration() };

            Assert.True(move.ContainsNode(node1));
            Assert.False(move.ContainsNode(node2));
        }

        [Test]
        public void EditOperationPropertyTest()
        {
            Assert.AreEqual(new Move().EditOperation, EditOperationEnum.Move);
        }

        [Test]
        public void ToStringTest()
        {
            var node1 = new UnknownReference();
            var node2 = new UnknownReference();
            var node3 = new UnknownReference();

            var move = new Move {MovedNode = node1, ParentNode = node2, OldParentNode = node3, ChildPosition = 2};

            var expected =
                string.Format("Move: [MovedNode: {0}, ParentNode: {1}, OldParentNode: {2}, ChildPosition: {3}, OldChildPosition: {4}]",
                    node1.GetPrintValue(), node2.GetPrintValue(), node3.GetPrintValue(), 2, 0);

            Assert.AreEqual(expected, move.ToString());
        }
    }
}
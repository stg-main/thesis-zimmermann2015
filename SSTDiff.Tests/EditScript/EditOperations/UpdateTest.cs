﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using KaVE.Commons.Model.SSTs.Impl.References;
using KaVE.Commons.Model.SSTs.Impl.Statements;
using NUnit.Framework;
using SSTDiff.EditScript.EditOperations;
using SSTDiff.Util;

namespace SSTDiff.Tests.EditScript.EditOperations
{
    public class UpdateTest
    {
        [Test]
        public void EqualTest()
        {
            var node1 = new UnknownReference();
            var node2 = new UnknownReference();

            var update1 = new Update { UpdatedNode = node1, NewValue= "foo", OldValue = "bar",NewNode = node2 };
            var update2 = new Update { UpdatedNode = node1, NewValue = "foo", OldValue = "bar", NewNode = node2 };
            var update3 = new Update { UpdatedNode = node1, NewValue = "bar", OldValue = "bar", NewNode = node2 };
            var update4 = new Update { UpdatedNode = node1, NewValue = "foo", OldValue = "bar", NewNode = new UnknownStatement() };
            var update5 = new Update { UpdatedNode = new UnknownStatement(), NewValue = "foo", OldValue = "bar", NewNode = node2 };
            var update6 = new Update { UpdatedNode = node1, NewValue = "foo", OldValue = "foo", NewNode = node2 };

            var differentObject = "Test";

            Assert.False(update1.Equals(null));
            // ReSharper disable once EqualExpressionComparison
            Assert.True(update1.Equals(update1));
            Assert.True(update1.Equals(update2));
            Assert.False(update1.Equals(update3));
            Assert.False(update1.Equals(update4));
            Assert.False(update1.Equals(update5));
            Assert.False(update1.Equals(update6));
            // ReSharper disable once SuspiciousTypeConversion.Global
            Assert.False(update1.Equals(differentObject));
        }

        [Test]
        public void GetHashCodeTest()
        {
            var node1 = new UnknownReference();
            var node2 = new UnknownReference();

            var update1 = new Update { UpdatedNode = node1, NewValue = "foo", OldValue = "bar", NewNode = node2 };
            var update2 = new Update { UpdatedNode = node1, NewValue = "foo", OldValue = "bar", NewNode = node2 };
            var update3 = new Update { UpdatedNode = node2, NewValue = "bar", OldValue = "foo", NewNode = node1 };

            Assert.AreEqual(update1.GetHashCode(), update2.GetHashCode());
            Assert.AreNotEqual(update1.GetHashCode(), update3.GetHashCode());
        }

        [Test]
        public void ContainsNodeTest()
        {
            var node1 = new UnknownStatement();
            var node2 = new UnknownStatement();

            var update = new Update { UpdatedNode = node1};

            Assert.True(update.ContainsNode(node1));
            Assert.False(update.ContainsNode(node2));
        }

        [Test]
        public void EditOperationPropertyTest()
        {
            Assert.AreEqual(new Update().EditOperation, EditOperationEnum.Update);
        }

        [Test]
        public void ToStringTest()
        {
            var node1 = new UnknownReference();

            var update = new Update {UpdatedNode = node1, NewValue = "foo"};

            var expected = string.Format("Update: [UpdatedNode: {0}, NewValue: {1}]", node1.GetPrintValue(), "foo");

            Assert.AreEqual(expected, update.ToString());
        }
    }
}
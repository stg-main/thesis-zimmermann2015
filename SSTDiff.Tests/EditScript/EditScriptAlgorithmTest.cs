﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System;
using System.Collections.Generic;
using DiffTestBench;
using KaVE.Commons.Model.Names.CSharp;
using KaVE.Commons.Model.SSTs.Impl;
using KaVE.Commons.Model.SSTs.Impl.Declarations;
using KaVE.Commons.Model.SSTs.Impl.Expressions.Simple;
using KaVE.Commons.Model.SSTs.Impl.References;
using KaVE.Commons.Model.SSTs.Impl.Statements;
using KaVE.Commons.Model.SSTs.Visitor;
using NUnit.Framework;
using SSTDiff.EditScript;
using SSTDiff.EditScript.EditOperations;
using SSTDiff.Matching;
using SSTDiff.Util;

namespace SSTDiff.Tests.EditScript
{
    public class EditScriptAlgorithmTest
    {
        private EditScriptAlgorithm _uut;

        [SetUp]
        public void Setup()
        {
            _uut = new EditScriptAlgorithm(new List<Match>());
        }

        #region Editscript Tests

        [Test]
        public void EmptyEditScriptWithEqualSST()
        {
            var sst = SSTReader.ReadAndParseSST("Util\\Test.json");
            var matches = new MatchingAlgorithm().CreateMatching(sst, sst);

            var uut = new EditScriptAlgorithm(matches);

            CollectionAssert.IsEmpty(uut.CreateEditScript(sst, sst));
        }

        [Test]
        public void EditScriptInsertDeleteTest()
        {
            var varRef = new VariableReference {Identifier = "foo"};

            var fieldNameA1 =
                FieldName.Get("CSharp.FieldName:[System.Int32, mscore, 4.0.0.0] [MyClass, MyAssembly, 1.2.3.4]" +
                              ".Apple");
            var fieldDeclA1 = new FieldDeclaration
            {
                Name =
                    fieldNameA1
            };

            var expectedFieldDeclA2 = new FieldDeclaration
            {
                Name =
                    FieldName.Get("CSharp.FieldName:[System.Int32, mscore, 4.0.0.0] [MyClass, MyAssembly, 1.2.3.4]" +
                                  ".Banana")
            };

            var fieldDeclA2 = new FieldDeclaration
            {
                Name =
                    FieldName.Get("CSharp.FieldName:[System.Int32, mscore, 4.0.0.0] [MyClass, MyAssembly, 1.2.3.4]" +
                                  ".Banana")
            };

            var fieldDeclB1 = new FieldDeclaration
            {
                Name =
                    FieldName.Get(
                        "CSharp.FieldName:[System.Int32, mscore, 4.0.0.0] [OtherClass, OtherAssembly, 1.2.3.4]" +
                        ".Peach")
            };

            var assignment = new Assignment
            {
                Expression = new ReferenceExpression(),
                Reference = varRef
            };

            var methodDeclarationA = new MethodDeclaration
            {
                Body =
                {
                    assignment
                }
            };


            var methodDeclarationB = new MethodDeclaration
            {
                Body =
                {
                    new UnknownStatement(),
                    assignment,
                    new UnknownStatement()
                }
            };

            var sstRootLeft = new SST
            {
                Fields =
                {
                    fieldDeclA1,
                    fieldDeclA2
                },
                Methods =
                {
                    methodDeclarationA
                }
            };

            var sstRootRight = new SST
            {
                Fields =
                {
                    fieldDeclB1
                },
                Methods =
                {
                    methodDeclarationB
                }
            };

            var matching = new MatchingAlgorithm(new Settings
            {
                ActivateMatchingWithParent = false
            }).CreateMatching(sstRootLeft, sstRootRight);

            var uut = new EditScriptAlgorithm(matching);

            var actualEditScript = uut.CreateEditScript(sstRootLeft, sstRootRight);


            var expectedEditScript = new List<IEditOperation>
            {
                new Insert {ChildPosition = 1, InsertedNode = fieldDeclB1.FieldName, ParentNode = fieldDeclA1},
                new Delete
                {
                    ChildPosition = 1,
                    DeletedNode = new SimpleName {Name = fieldNameA1.Name},
                    ParentNode = fieldDeclA1
                },
                new Insert {ChildPosition = 2, InsertedNode = new UnknownStatement(), ParentNode = methodDeclarationA},
                new Insert {ChildPosition = 4, InsertedNode = new UnknownStatement(), ParentNode = methodDeclarationA},
                new Delete {ChildPosition = 1, DeletedNode = fieldDeclA2.FieldName, ParentNode = fieldDeclA2},
                new Delete {ChildPosition = 0, DeletedNode = fieldDeclA2.ValueType, ParentNode = fieldDeclA2},
                new Delete {ChildPosition = 1, DeletedNode = expectedFieldDeclA2, ParentNode = sstRootLeft}
            };
            CollectionAssert.AreEquivalent(expectedEditScript, actualEditScript);
        }

        [Test]
        public void EditScriptUpdateTest()
        {
            var varRefA = new VariableReference {Identifier = "foo"};
            var varRefB = new VariableReference {Identifier = "foob"};

            var methodNameA =
                MethodName.Get(
                    "[System.Void, mscorlib, 4.0.0.0] [SSTDiff.Util.StringSimilarity, SSTDiff].Fruits()");

            var methodNameB =
                MethodName.Get(
                    "[System.Void, mscorlib, 4.0.0.0] [SSTDiff.Util.StringSimilarity, SSTDiff].Fruit()");


            var assignmentA = new Assignment
            {
                Expression = new ReferenceExpression(),
                Reference = varRefA
            };

            var assignmentB = new Assignment
            {
                Expression = new ReferenceExpression(),
                Reference = varRefB
            };

            var fieldDeclA1 = new FieldDeclaration
            {
                Name =
                    FieldName.Get("[System.Int32, mscore, 4.0.0.0] [MyClass, MyAssembly, 1.2.3.4]" +
                                  ".Apple")
            };

            var fieldDeclB1 = new FieldDeclaration
            {
                Name =
                    FieldName.Get(
                        "[System.Int32, mscore, 4.0.0.0] [MyClass, MyAssembly, 1.2.3.4]" +
                        ".Apples")
            };


            var methodDeclarationA = new MethodDeclaration
            {
                Name = methodNameA,
                Body =
                {
                    assignmentA
                }
            };

            var methodDeclarationB = new MethodDeclaration
            {
                Name = methodNameB,
                Body =
                {
                    assignmentB
                }
            };

            var sstRootLeft = new SST
            {
                Fields =
                {
                    fieldDeclA1
                },
                Methods =
                {
                    methodDeclarationA
                }
            };

            var sstRootRight = new SST
            {
                Fields =
                {
                    fieldDeclB1
                },
                Methods =
                {
                    methodDeclarationB
                }
            };

            var matching = new MatchingAlgorithm().CreateMatching(sstRootLeft, sstRootRight);

            var uut = new EditScriptAlgorithm(matching);

            var actualEditScript = uut.CreateEditScript(sstRootLeft, sstRootRight);


            var expectedEditScript = new List<IEditOperation>
            {
                new Update
                {
                    UpdatedNode = fieldDeclA1.FieldName,
                    NewValue = fieldDeclB1.FieldName.GetValue(),
                    OldValue = "Apple",
                    NewNode = fieldDeclB1.FieldName
                },
                new Update {UpdatedNode = varRefA, NewValue = varRefB.GetValue(), OldValue = "foo", NewNode = varRefB},
                new Update
                {
                    UpdatedNode = methodDeclarationA.MethodName,
                    NewValue = "Fruit",
                    OldValue = "Fruits",
                    NewNode = methodDeclarationB.MethodName
                }
            };

            CollectionAssert.AreEquivalent(expectedEditScript, actualEditScript);
        }

        [Test]
        public void MoveWithDifferentParents()
        {
            var varRef = new VariableReference {Identifier = "foo"};

            var varDecl = new VariableDeclaration {Reference = varRef, Type = TestNameFactory.GetAnonymousTypeName()};
            var anonymousMethodName = TestNameFactory.GetAnonymousMethodName();

            var methodDeclarationA1 = new MethodDeclaration
            {
                Name = MethodName.UnknownName,
                Body =
                {
                    new UnknownStatement(),
                    varDecl
                }
            };

            var methodDeclarationA2 = new MethodDeclaration
            {
                Name = anonymousMethodName,
                Body =
                {
                    new UnknownStatement()
                }
            };

            var methodDeclarationB1 = new MethodDeclaration
            {
                Name = MethodName.UnknownName,
                Body =
                {
                    new UnknownStatement()
                }
            };


            var methodDeclarationB2 = new MethodDeclaration
            {
                Name = anonymousMethodName,
                Body =
                {
                    new UnknownStatement(),
                    varDecl
                }
            };

            var sstRootLeft = new SST
            {
                Methods =
                {
                    methodDeclarationA1,
                    methodDeclarationA2
                }
            };

            var sstRootRight = new SST
            {
                Methods =
                {
                    methodDeclarationB1,
                    methodDeclarationB2
                }
            };

            var matching = new MatchingAlgorithm().CreateMatching(sstRootLeft, sstRootRight);

            var uut = new EditScriptAlgorithm(matching);

            var actualEditScript = uut.CreateEditScript(sstRootLeft, sstRootRight);

            var expectedEditScript = new List<IEditOperation>
            {
                new Move
                {
                    ChildPosition = 3,
                    OldChildPosition = 3,
                    MovedNode = varDecl,
                    ParentNode = methodDeclarationA2,
                    OldParentNode = methodDeclarationA1
                }
            };

            CollectionAssert.AreEquivalent(expectedEditScript, actualEditScript);
        }

        [Test]
        public void MoveChildren()
        {
            var varRef1 = new VariableReference {Identifier = "foo"};
            var varRef2 = new VariableReference {Identifier = "bar"};
            var varRef3 = new VariableReference {Identifier = "foobar"};
            var varRef4 = new VariableReference {Identifier = "barfoo"};

            var anonymousTypeName1 = TestNameFactory.GetAnonymousTypeName();
            var anonymousTypeName2 = TestNameFactory.GetAnonymousTypeName();
            var anonymousTypeName3 = TestNameFactory.GetAnonymousTypeName();
            var anonymousTypeName4 = TestNameFactory.GetAnonymousTypeName();

            var variableDeclA1 = new VariableDeclaration {Reference = varRef1, Type = anonymousTypeName1};
            var variableDeclA2 = new VariableDeclaration {Reference = varRef2, Type = anonymousTypeName2};
            var variableDeclA3 = new VariableDeclaration {Reference = varRef3, Type = anonymousTypeName3};
            var variableDeclA4 = new VariableDeclaration {Reference = varRef4, Type = anonymousTypeName4};

            var variableDeclB1 = new VariableDeclaration {Reference = varRef1, Type = anonymousTypeName1};
            var variableDeclB2 = new VariableDeclaration {Reference = varRef2, Type = anonymousTypeName2};
            var variableDeclB3 = new VariableDeclaration {Reference = varRef3, Type = anonymousTypeName3};
            var variableDeclB4 = new VariableDeclaration {Reference = varRef4, Type = anonymousTypeName4};

            var methodDeclarationA = new MethodDeclaration
            {
                Body =
                {
                    variableDeclA1,
                    variableDeclA2,
                    variableDeclA3,
                    variableDeclA4
                }
            };


            var methodDeclarationB = new MethodDeclaration
            {
                Body =
                {
                    variableDeclB2,
                    variableDeclB4,
                    variableDeclB3,
                    variableDeclB1
                }
            };

            var matching = new MatchingAlgorithm().CreateMatching(methodDeclarationA, methodDeclarationB);

            var uut = new EditScriptAlgorithm(matching);

            var actualEditScript = uut.CreateEditScript(methodDeclarationA, methodDeclarationB);

            var expectedEditScript = new List<IEditOperation>
            {
                new Move
                {
                    ChildPosition = 5,
                    OldChildPosition = 2,
                    MovedNode = variableDeclA1,
                    ParentNode = methodDeclarationA,
                    OldParentNode = methodDeclarationA
                },
                new Move
                {
                    ChildPosition = 3,
                    OldChildPosition = 5,
                    MovedNode = variableDeclA4,
                    ParentNode = methodDeclarationA,
                    OldParentNode = methodDeclarationA
                }
            };

            CollectionAssert.AreEquivalent(expectedEditScript, actualEditScript);
        }

        #endregion

        #region Algorithm Method Tests

        [Test]
        public void SimpleCreateLongestCommonSubsequenceTest()
        {
            var variableDeclA1 = new VariableDeclaration();
            var variableDeclA2 = new VariableDeclaration();
            var variableDeclA3 = new VariableDeclaration();

            var variableDeclB1 = new VariableDeclaration();
            var variableDeclB2 = new VariableDeclaration();
            var variableDeclB3 = new VariableDeclaration();

            var matches = new List<Match>
            {
                Match.Create(variableDeclA1, variableDeclB1),
                Match.Create(variableDeclA2, variableDeclB2),
                Match.Create(variableDeclA3, variableDeclB3),
                Match.Create(new UnknownStatement(), new UnknownStatement())
            };

            var leftChildSequence = new List<ISSTNode>
            {
                variableDeclA1,
                variableDeclA3,
                new UnknownStatement(),
                variableDeclA2
            };

            var rightChildSequence = new List<ISSTNode>
            {
                variableDeclB1,
                variableDeclB2,
                new UnknownStatement(),
                variableDeclB3
            };

            var uut = new EditScriptAlgorithm(matches);

            var expectedLcs =
                new HashSet<Tuple<ISSTNode, ISSTNode>>(
                    ObjectReferenceEqualityComparer<Tuple<ISSTNode, ISSTNode>>.Default)
                {
                    Tuple.Create((ISSTNode) variableDeclA1, (ISSTNode) variableDeclB1),
                    Tuple.Create((ISSTNode) variableDeclA2, (ISSTNode) variableDeclB2)
                };

            var actual = uut.CreateLongestCommonSubsequence(leftChildSequence, rightChildSequence);

            CollectionAssert.AreEqual(expectedLcs, actual);
        }

        [Test]
        public void CreateChildrenWithPartnerSequenceTest()
        {
            var variableDeclA1 = new VariableDeclaration();
            var variableDeclA2 = new VariableDeclaration();
            var variableDeclA3 = new VariableDeclaration();

            var variableDeclB1 = new VariableDeclaration();
            var variableDeclB2 = new VariableDeclaration();
            var variableDeclB3 = new VariableDeclaration();

            var matches = new List<Match>
            {
                Match.Create(variableDeclA1, variableDeclB1),
                Match.Create(variableDeclA2, variableDeclB2),
                Match.Create(new UnknownStatement(), new UnknownStatement())
            };

            var methodDeclA = new MethodDeclaration
            {
                Body =
                {
                    variableDeclA1,
                    variableDeclA2,
                    variableDeclA3,
                    new UnknownStatement()
                }
            };

            var methodDeclB = new MethodDeclaration
            {
                Body =
                {
                    variableDeclB1,
                    variableDeclB2,
                    variableDeclB3,
                    new UnknownStatement()
                }
            };

            var uut = new EditScriptAlgorithm(matches);

            var expectedChildrenListA = new List<ISSTNode>
            {
                variableDeclA1,
                variableDeclA2
            };

            var expectedChildrenListB = new List<ISSTNode>
            {
                variableDeclB1,
                variableDeclB2
            };

            var leftToRightMatch = new Dictionary<ISSTNode, ISSTNode>(ObjectReferenceEqualityComparer<ISSTNode>.Default);
            var rightToLeftMatch = new Dictionary<ISSTNode, ISSTNode>(ObjectReferenceEqualityComparer<ISSTNode>.Default);
            foreach (var match in matches)
            {
                leftToRightMatch.Add(match.NodeA, match.NodeB);
                rightToLeftMatch.Add(match.NodeB, match.NodeA);
            }
            var actualChildrenListA = uut.CreateChildrenWithPartnerSequence(methodDeclA, methodDeclB, leftToRightMatch);
            var actualChildrenListB = uut.CreateChildrenWithPartnerSequence(methodDeclB, methodDeclA, rightToLeftMatch);

            CollectionItemsAreReferenceEqual(expectedChildrenListA, actualChildrenListA);

            CollectionItemsAreReferenceEqual(expectedChildrenListB, actualChildrenListB);
        }

        private static void CollectionItemsAreReferenceEqual(List<ISSTNode> expected, List<ISSTNode> actual)
        {
            if (expected.Count != actual.Count) Assert.Fail("Collections have different lengths");

            for (var i = 0; i < expected.Count; i++)
            {
                if (!ReferenceEquals(expected[i], actual[i])) Assert.Fail("One Item is not ReferenceEqual");
            }
        }

        [Test]
        public void MarkInOrderTest()
        {
            var variableDecl = new VariableDeclaration();

            var inOrderMap = new Dictionary<ISSTNode, bool>
            {
                {variableDecl, false}
            };

            _uut.MarkInOrder(variableDecl, inOrderMap);

            Assert.AreEqual(true, inOrderMap[variableDecl]);
        }

        [Test]
        public void MarkChildrenOutOfOrderTest()
        {
            var variableDecl = new VariableDeclaration();
            var variableDecl2 = new VariableDeclaration();
            var variableDecl3 = new VariableDeclaration();
            var variableDecl4 = new VariableDeclaration();

            var inOrderMap = new Dictionary<ISSTNode, bool>(ObjectReferenceEqualityComparer<ISSTNode>.Default)
            {
                {variableDecl, true},
                {variableDecl2, false},
                {variableDecl3, true},
                {variableDecl4, true}
            };

            var methodDecl = new MethodDeclaration
            {
                Body =
                {
                    variableDecl,
                    variableDecl2,
                    variableDecl3,
                    variableDecl4
                }
            };

            _uut.MarkChildrenOutOfOrder(methodDecl, inOrderMap);

            foreach (var keyValuePair in inOrderMap)
            {
                Assert.AreEqual(false, keyValuePair.Value);
            }
        }

        [Test]
        public void GetRightMostSiblingThatIsInOrder()
        {
            var variableDecl = new VariableDeclaration();
            var variableDecl2 = new VariableDeclaration();
            var variableDecl3 = new VariableDeclaration();
            var variableDecl4 = new VariableDeclaration();

            var inOrderMap = new Dictionary<ISSTNode, bool>(ObjectReferenceEqualityComparer<ISSTNode>.Default)
            {
                {variableDecl, true},
                {variableDecl2, false},
                {variableDecl3, true},
                {variableDecl4, true}
            };

            var childrenList = new List<ISSTNode>
            {
                variableDecl,
                variableDecl2,
                variableDecl3,
                variableDecl4
            };

            var uut =
                new EditScriptAlgorithm(
                    new List<Match>
                    {
                        Match.Create(variableDecl3, variableDecl3),
                        Match.Create(variableDecl4, variableDecl4)
                    }, inOrderMap, inOrderMap);

            Assert.IsTrue(ReferenceEquals(variableDecl3,
                uut.GetRightMostSiblingThatIsInOrder(variableDecl4, childrenList, inOrderMap)));
        }

        [Test]
        public void IsInOrderTest()
        {
            var varRefA = new VariableReference {Identifier = "VariableA"};
            var varRefB = new VariableReference {Identifier = "VariableB"};
            var inOrderTree = new Dictionary<ISSTNode, bool>
            {
                {varRefA, true},
                {varRefB, false}
            };

            Assert.AreEqual(true, _uut.IsInOrder(varRefA, inOrderTree));
            Assert.AreEqual(false, _uut.IsInOrder(varRefB, inOrderTree));
        }

        [Test]
        public void FindPositionTest()
        {
            var variableDecl = new VariableDeclaration();
            var variableDecl2 = new VariableDeclaration();
            var variableDecl3 = new VariableDeclaration();
            var variableDecl4 = new VariableDeclaration();

            var methodName = TestNameFactory.GetAnonymousMethodName();
            var simpleMethodName = new SimpleName {Name = methodName.Name};
            var returnType = new TypeReference {TypeName = methodName.ReturnType};

            var inOrderMap = new Dictionary<ISSTNode, bool>(ObjectReferenceEqualityComparer<ISSTNode>.Default)
            {
                {variableDecl, true},
                {variableDecl2, true},
                {variableDecl3, true},
                {variableDecl4, true},
                {simpleMethodName, true},
                {returnType, true}
            };

            var methodDeclA = new MethodDeclaration
            {
                MethodName = simpleMethodName,
                ReturnType = returnType,
                Body =
                {
                    variableDecl,
                    variableDecl4
                }
            };

            var methodDeclB = new MethodDeclaration
            {
                MethodName = simpleMethodName,
                ReturnType = returnType,
                Body =
                {
                    variableDecl2,
                    variableDecl,
                    variableDecl4,
                    variableDecl3
                }
            };

            var uut =
                new EditScriptAlgorithm(
                    new List<Match>
                    {
                        Match.Create(variableDecl, variableDecl),
                        Match.Create(variableDecl4, variableDecl4),
                        Match.Create(methodDeclA.MethodName, methodDeclA.MethodName),
                        Match.Create(methodDeclA.ReturnType, methodDeclB.ReturnType)
                    }, inOrderMap, inOrderMap);

            Assert.AreEqual(2, uut.FindPosition(variableDecl2, methodDeclA, methodDeclB));
            Assert.AreEqual(4, uut.FindPosition(variableDecl3, methodDeclA, methodDeclB));
        }

        [Test]
        public void MatchingContainsNodesTest()
        {
            var variableDecl = new VariableDeclaration();
            var variableDecl2 = new VariableDeclaration();
            var variableDecl3 = new VariableDeclaration();
            var variableDecl4 = new VariableDeclaration();

            var methodName = TestNameFactory.GetAnonymousMethodName();
            var simpleMethodNameA = new SimpleName {Name = methodName.Name};
            var simpleMethodNameB = new SimpleName {Name = methodName.Name};
            var returnTypeA = new TypeReference {TypeName = methodName.ReturnType};
            var returnTypeB = new TypeReference {TypeName = methodName.ReturnType};

            var methodDeclA = new MethodDeclaration
            {
                MethodName = simpleMethodNameA,
                ReturnType = returnTypeA,
                Body =
                {
                    variableDecl,
                    variableDecl2
                }
            };

            var methodDeclB = new MethodDeclaration
            {
                MethodName = simpleMethodNameB,
                ReturnType = returnTypeB,
                Body =
                {
                    variableDecl3,
                    variableDecl4
                }
            };

            var matching = new List<Match>
            {
                Match.Create(variableDecl, variableDecl3),
                Match.Create(variableDecl2, variableDecl4),
                Match.Create(methodDeclA.MethodName, methodDeclB.MethodName),
                Match.Create(methodDeclA.ReturnType, methodDeclB.ReturnType)
            };

            var editScriptAlgorithm = new EditScriptAlgorithm(matching);

            Assert.True(editScriptAlgorithm.MatchingContainsNodes(variableDecl, variableDecl3));
            Assert.True(editScriptAlgorithm.MatchingContainsNodes(variableDecl2, variableDecl4));
            Assert.True(editScriptAlgorithm.MatchingContainsNodes(simpleMethodNameA, simpleMethodNameB));
            Assert.True(editScriptAlgorithm.MatchingContainsNodes(returnTypeA, returnTypeB));
            Assert.False(editScriptAlgorithm.MatchingContainsNodes(variableDecl3, variableDecl));
            Assert.False(editScriptAlgorithm.MatchingContainsNodes(variableDecl, variableDecl));
            Assert.False(editScriptAlgorithm.MatchingContainsNodes(variableDecl2, variableDecl2));
        }

        #endregion
    }
}
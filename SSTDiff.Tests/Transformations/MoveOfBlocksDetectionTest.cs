﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;
using KaVE.Commons.Model.SSTs.Impl.Blocks;
using KaVE.Commons.Model.SSTs.Impl.Declarations;
using KaVE.Commons.Model.SSTs.Impl.Expressions.Simple;
using KaVE.Commons.Model.SSTs.Impl.Statements;
using NUnit.Framework;
using SSTDiff.EditScript;
using SSTDiff.EditScript.EditOperations;
using SSTDiff.Matching;
using SSTDiff.Transformations;

namespace SSTDiff.Tests.Transformations
{
    public class MoveOfBlocksDetectionTest
    {
        [Test]
        public void DetectsMoveOfBlockCorrectly()
        {
            var assignment = new Assignment();
            var unknownStatement = new UnknownStatement();
            var variableDecl = new VariableDeclaration();
            var whileLoop = new WhileLoop
            {
                Body =
                {
                    variableDecl,
                    assignment,
                    unknownStatement
                }
            };
            
            var methodDeclA = new MethodDeclaration
            {
                Body =
                {
                    variableDecl,
                    assignment,
                    unknownStatement
                }
            };
            var methodDeclB = new MethodDeclaration
            {
                Body =
                {
                    whileLoop
                }
            };

            var settings = new Settings
            {
                ActivateMatchingWithParent = true
            };

            var matching = new MatchingAlgorithm(settings).CreateMatching(methodDeclA,methodDeclB);
            var editScriptAlgorithm = new EditScriptAlgorithm(matching);
            var actualEditScript = editScriptAlgorithm.CreateEditScript(methodDeclA,methodDeclB);

            new DeletedNodesFromMovesInsertion().ApplyTransformation(actualEditScript,methodDeclA, editScriptAlgorithm);
            new MoveOfBlocksDetection().ApplyTransformation(actualEditScript,methodDeclA, editScriptAlgorithm);


            var expectedBlock = new UsingBlock
            {
                Body =
                {
                    variableDecl,
                    assignment,
                    unknownStatement
                }
            };

            var expectedWhileLoop = new WhileLoop
            {
                Body =
                {
                    expectedBlock
                }
            };

            var expectedOldMethodDecl = new MethodDeclaration
            {
                Body =
                {
                    expectedBlock,
                    expectedWhileLoop
                }
            };

            var expectedEditScript = new List<IEditOperation>
            {
                new Insert{ChildPosition = 2, InsertedNode = expectedWhileLoop, ParentNode = methodDeclA},
                new Insert{ChildPosition = 0, InsertedNode = new UnknownExpression(), ParentNode = expectedWhileLoop},
                new Move{ChildPosition = 1, MovedNode = expectedBlock, ParentNode = expectedWhileLoop, OldParentNode = expectedOldMethodDecl, OldChildPosition = 2}
            };

            CollectionAssert.AreEquivalent(expectedEditScript, actualEditScript);
        }
    }
}
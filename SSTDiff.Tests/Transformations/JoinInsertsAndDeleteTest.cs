﻿using System.Collections.Generic;
using KaVE.Commons.Model.SSTs.Impl.Blocks;
using KaVE.Commons.Model.SSTs.Impl.Expressions.Simple;
using KaVE.Commons.Model.SSTs.Impl.Statements;
using NUnit.Framework;
using SSTDiff.EditScript;
using SSTDiff.EditScript.EditOperations;
using SSTDiff.Matching;
using SSTDiff.Transformations;

namespace SSTDiff.Tests.Transformations
{
    public class JoinInsertsAndDeleteTest
    {
        [Test]
        public void CollapsesConditionsCorrectly()
        {
            var constantValueExpressionA = new ConstantValueExpression { Value = "true"};
            var ifElseBlockA = new IfElseBlock
            {
                Condition = constantValueExpressionA,
                Then = 
                {
                    new UnknownStatement()
                }
            };

            var constantValueExpressionB = new ConstantValueExpression {Value = "false"};
            var ifElseBlockB = new IfElseBlock
            {
                Condition = constantValueExpressionB,
                Then =
                {
                    new UnknownStatement()
                }
            };

            var matching = new MatchingAlgorithm().CreateMatching(ifElseBlockA, ifElseBlockB);
            var editScriptAlgorithm = new EditScriptAlgorithm(matching);
            var actualEditScript = editScriptAlgorithm.CreateEditScript(ifElseBlockA, ifElseBlockB);

            new JoinInsertsAndDelete().ApplyTransformation(actualEditScript,ifElseBlockA, editScriptAlgorithm);

            var expectedEditScript = new List<IEditOperation>
            {
                new Update
                {
                    NewNode = constantValueExpressionB,
                    NewValue = "false",
                    OldValue = "true",
                    UpdatedNode = constantValueExpressionB
                }
            };

            CollectionAssert.AreEquivalent(expectedEditScript, actualEditScript);
        }
    }
}
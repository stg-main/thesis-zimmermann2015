﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;
using KaVE.Commons.Model.SSTs.Impl.Blocks;
using KaVE.Commons.Model.SSTs.Impl.Declarations;
using KaVE.Commons.Model.SSTs.Impl.Expressions.Simple;
using KaVE.Commons.Model.SSTs.Impl.References;
using KaVE.Commons.Model.SSTs.Impl.Statements;
using NUnit.Framework;
using SSTDiff.EditScript;
using SSTDiff.EditScript.EditOperations;
using SSTDiff.Matching;
using SSTDiff.Transformations;

namespace SSTDiff.Tests.Transformations
{
    public class WrappingDetectionTest
    {
        [Test]
        public void IgnoresWrappingMoves()
        {
            var varRef = new VariableReference { Identifier = "foo" };

            var assignment = new Assignment
            {
                Expression = new ReferenceExpression(),
                Reference = varRef
            };

            var constantValueExpression = new ConstantValueExpression();
            var constantValueExpression2 = new ConstantValueExpression();

            var ifElseBlock = new IfElseBlock
            {
                Condition = constantValueExpression,
                Then =
                {
                    assignment
                }
            };

            var ifElseBlockRecursive = new IfElseBlock
            {
                Condition = constantValueExpression2,
                Then =
                {
                    ifElseBlock
                }
            };

            var methodDeclarationA = new MethodDeclaration
            {
                Body =
                {
                    assignment
                }
            };

            var methodDeclarationB = new MethodDeclaration
            {
                Body =
                {
                    ifElseBlockRecursive
                }
            };

            var matching = new MatchingAlgorithm().CreateMatching(methodDeclarationA, methodDeclarationB);

            var uut = new EditScriptAlgorithm(matching);

            var actualEditScript = uut.CreateEditScript(methodDeclarationA, methodDeclarationB);

            var expectedEditScript = new List<IEditOperation>
            {
                new Insert {ChildPosition = 2, InsertedNode = ifElseBlockRecursive, ParentNode = methodDeclarationA},
                new Insert {ChildPosition = 1, InsertedNode = ifElseBlock, ParentNode = ifElseBlockRecursive},
                new Insert {ChildPosition = 0, InsertedNode = constantValueExpression, ParentNode = ifElseBlock},
                new Insert {ChildPosition = 0, InsertedNode = constantValueExpression, ParentNode = ifElseBlockRecursive},
            };

            new WrappingDetection().ApplyTransformation(actualEditScript, methodDeclarationA, uut);

            CollectionAssert.AreEquivalent(expectedEditScript, actualEditScript);
        }

        [Test]
        public void IgnoresWrappingInNonInsertedNodes()
        {
            var unknownStatement = new UnknownStatement();

            var constantValueExpression = new ConstantValueExpression();

            var ifElseBlockT1 = new IfElseBlock
            {
                Condition = constantValueExpression
            };

            var ifElseBlockT2 = new IfElseBlock
            {
                Condition = constantValueExpression,
                Then =
                {
                    unknownStatement
                }
            };

            var methodDeclarationA = new MethodDeclaration
            {
                Body =
                {
                    unknownStatement,
                    ifElseBlockT1
                }
            };

            var methodDeclarationB = new MethodDeclaration
            {
                Body =
                {
                    ifElseBlockT2
                }
            };

            var matching = new MatchingAlgorithm().CreateMatching(methodDeclarationA, methodDeclarationB);

            var uut = new EditScriptAlgorithm(matching);

            var actualEditScript = uut.CreateEditScript(methodDeclarationA, methodDeclarationB);

            var expectedEditScript = new List<IEditOperation>
            {
                new Move{ChildPosition = 1, OldChildPosition = 2, MovedNode = unknownStatement, OldParentNode = methodDeclarationA, ParentNode = ifElseBlockT1}
            };

            new WrappingDetection().ApplyTransformation(actualEditScript, methodDeclarationA, uut);

            CollectionAssert.AreEquivalent(expectedEditScript, actualEditScript);
        } 
    }
}
﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using KaVE.Commons.Model.Names.CSharp;
using KaVE.Commons.Model.SSTs;
using KaVE.Commons.Model.SSTs.Impl;
using KaVE.Commons.Model.SSTs.Impl.Declarations;
using KaVE.Commons.Model.SSTs.Impl.Expressions.Simple;
using KaVE.Commons.Model.SSTs.Impl.References;
using KaVE.Commons.Model.SSTs.Impl.Statements;
using NUnit.Framework;
using SSTDiff.Util;

namespace SSTDiff.Tests.Util
{
    public class SSTNodeUtilTest
    {
        [Test]
        public void GetParentTest()
        {
            var varRefA = new VariableReference {Identifier = "VariableA"};
            var fieldDeclA = new FieldDeclaration
            {
                Name = FieldName.Get("[System.Int32, mscore, 4.0.0.0] [MyClass, MyAssembly, 1.2.3.4]" + ".fieldA")
            };

            var referenceExpression = new ReferenceExpression();

            var assignment = new Assignment
            {
                Expression = referenceExpression,
                Reference = varRefA
            };

            var methodDeclaration = new MethodDeclaration
            {
                Body =
                {
                    assignment
                }
            };
            var assignmentClone = (IStatement) assignment.CloneNode();

            var methodDeclaration2 = new MethodDeclaration
            {
                Body =
                {
                    assignmentClone,
                    new UnknownStatement()
                }
            };

            var sstRoot = new SST
            {
                Fields =
                {
                    fieldDeclA
                },
                Methods =
                {
                    methodDeclaration2,
                    methodDeclaration
                }
            };

            Assert.AreEqual(sstRoot, fieldDeclA.GetParent(sstRoot,false));
            Assert.AreEqual(sstRoot, methodDeclaration.GetParent(sstRoot, false));
            Assert.AreEqual(methodDeclaration, assignment.GetParent(sstRoot, false));
            Assert.AreEqual(methodDeclaration, assignment.GetParent(sstRoot, false));
            Assert.AreEqual(assignment, referenceExpression.GetParent(sstRoot, false));
            Assert.AreEqual(assignment, varRefA.GetParent(sstRoot, false));
            Assert.AreEqual(referenceExpression, referenceExpression.Reference.GetParent(sstRoot, false));
        }

        [Test]
        public void GetValue()
        {
            var identifier = "FooBar";
            var varRef = new VariableReference {Identifier = identifier};

            Assert.AreEqual(identifier, varRef.GetValue());
        }

        [Test]
        public void CloneNodeTest()
        {
            var varRef = new VariableReference {Identifier = "VariableA"};
            var clone = varRef.CloneNode();

            Assert.AreEqual(varRef, clone);
            Assert.False(ReferenceEquals(varRef, clone));
        }

    }
}
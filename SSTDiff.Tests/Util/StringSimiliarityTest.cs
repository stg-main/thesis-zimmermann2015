﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
using System;
using System.Diagnostics;
using KaVE.Commons.Utils.Collections;
using NUnit.Framework;
using SSTDiff.Util;

namespace SSTDiff.Tests.Util
{
    public class StringSimiliarityTest
    {
        [SetUp]
        public void Setup()
        {
            StringSimilarity.N = 2;
        }

        [Test]
        public void SlidingWindowTest()
        {
            StringSimilarity.N = 3;

            var testString = "vertical";
            var actualSet = StringSimilarity.SlidingWindow(testString);

            var expectedSet = new KaVEHashSet<string>
            {
                "ver",
                "ert",
                "rti",
                "tic",
                "ica",
                "cal"
            };

            CollectionAssert.AreEqual(expectedSet, actualSet);
        }

        [Test]
        public void TwoGramsTest()
        {
            var s1 = "verticalDrawAction";
            var s2 = "drawVerticalAction";

            var actual = s1.CalculateNGramsSimilarity(s2);
            var actualRounded = Math.Round(actual, 2);
            Assert.AreEqual(0.88, actualRounded);
        }

        [Test]
        public void ThreeGramsTest()
        {
            StringSimilarity.N = 3;

            var s1 = "verticalDrawAction";
            var s2 = "drawVerticalAction";

            var actual = s1.CalculateNGramsSimilarity(s2);
            var actualRounded = Math.Round(actual, 2);
            Assert.AreEqual(0.75, actualRounded);
        }

        [Test]
        public void FourGramsTest()
        {
            StringSimilarity.N = 4;

            var s1 = "verticalDrawAction";
            var s2 = "drawVerticalAction";

            var actual = s1.CalculateNGramsSimilarity(s2);
            var actualRounded = Math.Round(actual, 2);
            Assert.AreEqual(0.6, actualRounded);
        }

        [Test]
        public void MethodNameSimilarity()
        {
            var a = "CSharp.MethodName:" + TestNameFactory.GetAnonymousMethodName();
            var b = "CSharp.MethodName:" + TestNameFactory.GetAnonymousMethodName();

            var sim = a.CalculateSimilarity(b);

            Assert.Greater(sim, 0.6);
        }

        [Test]
        public void DifferentLongMethodNameSimilarity()
        {
            var a =
                "CSharp.MethodName:[System.Collections.Generic.List`1[[T -&gt; SSTDiff.Matching.Match, SSTDiff]], mscorlib, 4.0.0.0] [SSTDiff.Matching.MatchingAlgorithm, SSTDiff].CreateMatching([i:KaVE.Commons.Model.SSTs.Visitor.ISSTNode, KaVE.Commons, 0.915.0.0] tree1, [i:KaVE.Commons.Model.SSTs.Visitor.ISSTNode, KaVE.Commons, 0.915.0.0] tree2)";
            var b =
                "CSharp.MethodName:static [System.Collections.Generic.List`1[[T -&gt; i:KaVE.Commons.Model.SSTs.Visitor.ISSTNode, KaVE.Commons, 0.915.0.0]], mscorlib, 4.0.0.0] [SSTDiff.Util.SSTListUtil, SSTDiff].GetPostOrderNodes([i:KaVE.Commons.Model.SSTs.Visitor.ISSTNode, KaVE.Commons, 0.915.0.0] root)";

            var sim = a.CalculateSimilarity(b);

            Assert.Less(sim, 0.6);
            Debug.Write(sim);
        }

        [Test]
        public void DifferentLongMethodNamesWithSameParameterNumberSimilarity()
        {
            var a =
                "CSharp.MethodName:[System.Void, mscorlib, 4.0.0.0] [SSTDiff.Util.StringSimilarity, SSTDiff].Fruits([System.String, mscorlib, 4.0.0.0] apple, [System.String, mscorlib, 4.0.0.0] peach)";

            var b =
                "CSharp.MethodName:[System.Void, mscorlib, 4.0.0.0] [SSTDiff.Util.StringSimilarity, SSTDiff].Fruits([System.String, mscorlib, 4.0.0.0] pear, [System.String, mscorlib, 4.0.0.0] orange)";

            var sim = a.CalculateSimilarity(b);

            Assert.Less(sim, 0.6);
            Debug.Write(sim);
        }

        [Test]
        public void DifferentLongFieldNameSimilarity()
        {
            var a =
                "CSharp.FieldName:[KaVE.VS.FeedbackGenerator.Settings.ExportSettingsSuite.ExportSettings, KaVE.VS.FeedbackGenerator] [KaVE.VS.FeedbackGenerator.UserControls.UserProfile.UserProfileContext, KaVE.VS.FeedbackGenerator]._exportSettings";
            var b =
                "CSharp.FieldName:[i:KaVE.Commons.Utils.IRandomizationUtils, KaVE.Commons] [KaVE.VS.FeedbackGenerator.UserControls.UserProfile.UserProfileContext, KaVE.VS.FeedbackGenerator]._rnd";
            var sim = a.CalculateSimilarity(b);

            Assert.Less(sim, 0.6);
            Debug.Write(sim);
        }

        [Test]
        public void DifferentLongPropertyNameSimilarity()
        {
            var a =
                "CSharp.PropertyName:get [System.Boolean, mscorlib, 4.0.0.0] [KaVE.VS.FeedbackGenerator.UserControls.UserProfile.UserProfileContext, KaVE.VS.FeedbackGenerator].IsDatev()";
            var b =
                "CSharp.PropertyName:set get [System.String, mscorlib, 4.0.0.0] [KaVE.VS.FeedbackGenerator.UserControls.UserProfile.UserProfileContext, KaVE.VS.FeedbackGenerator].ProfileId()";

            var sim = a.CalculateSimilarity(b);

            Assert.Less(sim, 0.6);
            Debug.Write(sim);
        }

        [Test]
        public void DifferentLongTypeNameSimilarity()
        {
            var a = "CSharp.InterfaceTypeName:i:KaVE.Commons.Utils.IRandomizationUtils, KaVE.Commons";
            var b = "CSharp.TypeName:System.ComponentModel.PropertyChangedEventArgs, System, 4.0.0.0";

            var sim = a.CalculateSimilarity(b);

            Assert.Less(sim, 0.6);
            Debug.Write(sim);
        }

        [Test]
        public void DifferentLongEventNameSimilarity()
        {
            var a =
                "CSharp.EventName:[d:[System.Void, mscorlib, 4.0.0.0] [SSTDiff.Matching.Match+SubscriptionEventHandler, SSTDiff].([System.Object, mscorlib, 4.0.0.0] sender, [System.EventArgs, mscorlib, 4.0.0.0] e)] [SSTDiff.Matching.Match, SSTDiff].Changed";
            var b =
                "CSharp.EventName:[d:[System.Void, mscorlib, 4.0.0.0] [System.EventHandler, mscorlib, 4.0.0.0].([System.Object, mscorlib, 4.0.0.0] sender, [System.EventArgs, mscorlib, 4.0.0.0] e)] [SSTDiff.Matching.Match, SSTDiff].Handler";

            var sim = a.CalculateSimilarity(b);

            Assert.Less(sim, 0.6);
            Debug.Write(sim);
        }

        [Test]
        public void DifferentLongDelegateTypeNamesWithNoParameters()
        {
            var a =
                "CSharp.DelegateTypeName:d:[System.Void, mscorlib, 4.0.0.0] [SSTDiff.Matching.Match+ChangedEventHandler, SSTDiff].()";
            var b =
                "CSharp.DelegateTypeName:d:[System.Void, mscorlib, 4.0.0.0] [SSTDiff.Matching.Match+ProcessNumberDelegate, SSTDiff].()";
            var sim = a.CalculateSimilarity(b);

            Assert.Less(sim, 0.6);
            Debug.Write(sim);
        }

        [Test]
        public void DifferentLongDelegateTypeNameWithDifferentNumberOfParametersSimilarity()
        {
            var a =
                "CSharp.DelegateTypeName:d:[System.Void, mscorlib, 4.0.0.0] [SSTDiff.Matching.Match+ChangedEventHandler, SSTDiff].([System.Object, mscorlib, 4.0.0.0] sender, [System.EventArgs, mscorlib, 4.0.0.0] e)";
            var b =
                "CSharp.DelegateTypeName:d:[System.Void, mscorlib, 4.0.0.0] [SSTDiff.Matching.Match+ProcessNumberDelegate, SSTDiff].([System.Int32, mscorlib, 4.0.0.0] number)";
            var sim = a.CalculateSimilarity(b);

            Assert.Less(sim, 0.6);
            Debug.Write(sim);
        }

        [Test]
        public void DifferentLongDelegateTypeNameWithSameNumberOfParametersSimilarity()
        {
            var a =
                "CSharp.DelegateTypeName:d:[System.Void, mscorlib, 4.0.0.0] [SSTDiff.Matching.Match+ProcessNumberDelegate, SSTDiff].([System.Int32, mscorlib, 4.0.0.0] number)";
            var b =
                "CSharp.DelegateTypeName:d:[System.Void, mscorlib, 4.0.0.0] [SSTDiff.Matching.Match+ProcessTextDelegate, SSTDiff].([System.String, mscorlib, 4.0.0.0] text)";

            var sim = a.CalculateSimilarity(b);

            Assert.Less(sim, 0.6);
            Debug.Write(sim);
        }

        [Test]
        public void ShortStringSimilarity()
        {
            var a = "foobar";
            var b = "barfoo";

            var sim = a.CalculateSimilarity(b);

            Assert.Greater(sim, 0.6);
        }
    }
}
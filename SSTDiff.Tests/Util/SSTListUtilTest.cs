﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
using DiffTestBench;
using NUnit.Framework;
using SSTDiff.Util;

namespace SSTDiff.Tests.Util
{
    internal class SSTListUtilTest
    {
        [Test]
        public void GetLeavesTest()
        {
            var sst = SSTReader.ReadAndParseSST("Util\\Test.json");
            var leavesList = sst.GetLeaves();
            Assert.AreEqual(18, leavesList.Count);
        }

        [Test]
        public void GetNodesTest()
        {
            var sst = SSTReader.ReadAndParseSST("Util\\Test.json");
            var nodesList = sst.GetAllNodes();
            Assert.AreEqual(36, nodesList.Count);
        }

        [Test]
        public void GetPostOrderNodesTest()
        {
            var sst = SSTReader.ReadAndParseSST("Util\\Test.json");
            var nodesList = sst.GetPostOrderNodes();
            Assert.AreEqual(36, nodesList.Count);
        }

        [Test]
        public void GetBreadthFirstNodesTest()
        {
            var sst = SSTReader.ReadAndParseSST("Util\\Test.json");
            var nodesList = sst.GetBreadthFirstOrderNodes();
            Assert.AreEqual(36, nodesList.Count);
        }

        [Test]
        public void CreateInOrderDictionary()
        {
            var sst = SSTReader.ReadAndParseSST("Util\\Test.json");

            var dictionary = SSTListUtil.CreateInOrderDictionary(sst);

            Assert.AreEqual(36, dictionary.Count);

            foreach (var keyPair in dictionary)
            {
                Assert.AreEqual(false, keyPair.Value);
            }
        }
    }
}
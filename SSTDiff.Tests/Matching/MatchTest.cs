﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
using KaVE.Commons.Model.SSTs.Impl.References;
using KaVE.Commons.Model.SSTs.Impl.Statements;
using NUnit.Framework;
using SSTDiff.Matching;

namespace SSTDiff.Tests.Matching
{
    public class MatchTest
    {
        [Test]
        public void CreateTest()
        {
            var node1 = new UnknownReference();
            var node2 = new UnknownReference();

            var expectedMatch = new Match
            {
                NodeA = node1,
                NodeB = node2,
                Similiarity = 1.0
            };

            var actualMatch = Match.Create(node1, node2, 1.0);

            Assert.AreEqual(expectedMatch, actualMatch);
        }

        [Test]
        public void CreateWithoutSimilarityTest()
        {
            var node1 = new UnknownReference();
            var node2 = new UnknownReference();

            var expectedMatch = new Match
            {
                NodeA = node1,
                NodeB = node2,
                Similiarity = double.NaN
            };

            var actualMatch = Match.Create(node1, node2);

            Assert.AreEqual(expectedMatch, actualMatch);
        }

        [Test]
        public void EqualTest()
        {
            var node1 = new UnknownReference();
            var node2 = new UnknownReference();

            var baseMatch = Match.Create(node1, node2, 0.8);
            var match2 = Match.Create(node1, node2, 0.8);
            var match3 = Match.Create(node1, node2, 0.6);
            var match4 = Match.Create(node1, new UnknownStatement(), 0.8);
            var match5 = Match.Create(new UnknownStatement(), node2, 0.8);

            var differentObject = "Test";

            Assert.False(baseMatch.Equals(null));
            // ReSharper disable once EqualExpressionComparison
            Assert.True(baseMatch.Equals(baseMatch));
            Assert.True(baseMatch.Equals(match2));
            Assert.False(baseMatch.Equals(match3));
            Assert.False(baseMatch.Equals(match4));
            Assert.False(baseMatch.Equals(match5));
            // ReSharper disable once SuspiciousTypeConversion.Global
            Assert.False(baseMatch.Equals(differentObject));
        }

        [Test]
        public void GetHashCodeTest()
        {
            var node1 = new UnknownReference();
            var node2 = new UnknownReference();

            var match1 = Match.Create(node1, node2, 0.8);
            var match2 = Match.Create(node1, node2, 0.8);
            var match3 = Match.Create(node1, node2, 0.6);

            Assert.AreEqual(match1.GetHashCode(), match2.GetHashCode());
            Assert.AreNotEqual(match1.GetHashCode(), match3.GetHashCode());
        }

        [Test]
        public void ToStringTest()
        {
            var node1 = new UnknownReference();
            var node2 = new UnknownReference();

            var match = Match.Create(node1, node2, 0.8);

            var expected = "[" + node1 + ", " + node2 + ", 0,8]";

            Assert.AreEqual(expected, match.ToString());
        }

        [Test]
        public void GetOppositeNodeTest()
        {
            var node1 = new UnknownReference();
            var node2 = new UnknownReference();

            var match = Match.Create(node1, node2, 1.0);

            Assert.AreEqual(node1, match.GetOppositeNode(node2));
            Assert.AreEqual(node2, match.GetOppositeNode(node1));
        }

        [Test]
        public void ContainsNodeTest()
        {
            var node1 = new VariableReference {Identifier = "A"};
            var node2 = new VariableReference {Identifier = "B"};
            var node3 = new UnknownReference();

            var testMatch = Match.Create(node1, node2, 0.8);

            Assert.True(testMatch.ContainsNode(node1));
            Assert.True(testMatch.ContainsNode(node2));
            Assert.False(testMatch.ContainsNode(node3));
        }

        [Test]
        public void ContainsNodesTest()
        {
            var node1 = new VariableReference {Identifier = "A"};
            var node2 = new VariableReference {Identifier = "B"};
            var node3 = new UnknownReference();

            var testMatch = Match.Create(node1, node2, 0.8);

            Assert.True(testMatch.ContainsNodes(node1, node2));
            Assert.True(testMatch.ContainsNodes(node2, node1));
            Assert.False(testMatch.ContainsNodes(node1, node3));
        }
    }
}
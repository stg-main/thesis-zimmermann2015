﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
using System.Diagnostics;
using KaVE.Commons.Model.SSTs.Impl;
using KaVE.Commons.Model.SSTs.Impl.Declarations;
using KaVE.Commons.Model.SSTs.Impl.Expressions.Simple;
using KaVE.Commons.Model.SSTs.Impl.References;
using KaVE.Commons.Model.SSTs.Impl.Statements;
using NUnit.Framework;
using SSTDiff.Matching;
using SSTDiff.Util;

namespace SSTDiff.Tests.Matching
{
    public class MatchUtilTest
    {
        private MatchUtil _matchUtil = new MatchUtil(new Settings());

        [Test]
        public void MatchCriterion1SuccessOnShortStringValue()
        {
            var node1 = new VariableReference {Identifier = "foobar"};
            var node2 = new VariableReference {Identifier = "barfoo"};

            double similiarity;

            Assert.True(_matchUtil.MatchCriterion1(node1, node2, out similiarity));
        }

        [Test]
        public void MatchCriterion1SuccessOnLongStringValue()
        {
            var node1 = new VariableReference {Identifier = "verticalDrawAction"};
            var node2 = new VariableReference {Identifier = "drawVerticalAction"};

            double similiarity;

            Assert.True(_matchUtil.MatchCriterion1(node1, node2, out similiarity));
        }

        [Test]
        public void MatchCriterion1SuccessOnEmptyValues()
        {
            var node1 = new UnknownReference();
            var node2 = new UnknownReference();

            double similiarity;

            Assert.True(_matchUtil.MatchCriterion1(node1, node2, out similiarity));
        }

        [Test]
        public void MatchCriterion1UnsuccessfulOnDifferentTypes()
        {
            var node1 = new UnknownReference();
            var node2 = new UnknownStatement();

            double similiarity;

            Assert.False(_matchUtil.MatchCriterion1(node1, node2, out similiarity));
        }

        [Test]
        public void MatchCriterion1UnsuccessfulOnDifferentValues()
        {
            var node1 = new VariableReference {Identifier = "vertical"};
            var node2 = new VariableReference {Identifier = "horizontal"};

            double similiarity;

            Assert.False(_matchUtil.MatchCriterion1(node1, node2, out similiarity));
            Debug.Write(similiarity);
        }


        [Test]
        public void SmallSubtreeMatchCriterion2IntegrationTest()
        {
            var matchingAlgorithm = new MatchingAlgorithm();

            var methodDeclA = new MethodDeclaration
            {
                Body =
                {
                    new Assignment
                    {
                        Expression = new ConstantValueExpression {Value = "Foo"},
                        Reference = new VariableReference {Identifier = "VariableA"}
                    }
                }
            };

            var sstRootLeft = new SST
            {
                Methods =
                {
                    methodDeclA
                }
            };

            var methodDeclB = new MethodDeclaration
            {
                Body =
                {
                    new Assignment
                    {
                        Expression = new ConstantValueExpression {Value = "Bar"},
                        Reference = new VariableReference {Identifier = "VariableB"}
                    }
                }
            };
            var sstRootRight = new SST
            {
                Methods =
                {
                    methodDeclB
                }
            };
            double sim;
            Assert.True(_matchUtil.MatchCriterion2(methodDeclA, methodDeclB,
                matchingAlgorithm.MatchLeaves(sstRootLeft.GetPostOrderNodes(), sstRootRight.GetPostOrderNodes()), out sim));
        }

        [Test]
        public void LargeSubtreeMatchCriterion2IntegrationTest()
        {
            var matchingAlgorithm = new MatchingAlgorithm();

            var methodDeclA = new MethodDeclaration
            {
                Body =
                {
                    new Assignment
                    {
                        Expression = new ConstantValueExpression {Value = "Foo"},
                        Reference = new VariableReference {Identifier = "AAA"}
                    },
                    new Assignment
                    {
                        Expression = new ConstantValueExpression {Value = "Bar"},
                        Reference = new VariableReference {Identifier = "BBB"}
                    },
                    new Assignment
                    {
                        Expression = new ConstantValueExpression {Value = "Foo"},
                        Reference = new VariableReference {Identifier = "CCC"}
                    },
                    new Assignment
                    {
                        Expression = new ConstantValueExpression {Value = "Bar"},
                        Reference = new VariableReference {Identifier = "DDD"}
                    }
                }
            };

            var methodDeclB = new MethodDeclaration
            {
                Body =
                {
                    new Assignment
                    {
                        Expression = new ConstantValueExpression {Value = "Bar"},
                        Reference = new VariableReference {Identifier = "AAA"}
                    },
                    new Assignment
                    {
                        Expression = new ConstantValueExpression {Value = "Bar"},
                        Reference = new VariableReference {Identifier = "BBB"}
                    },
                    new Assignment
                    {
                        Expression = new ConstantValueExpression {Value = "Bar"},
                        Reference = new VariableReference {Identifier = "CCC"}
                    },
                    new Assignment
                    {
                        Expression = new ConstantValueExpression {Value = "Bar"},
                        Reference = new VariableReference {Identifier = "DDD"}
                    }
                }
            };
            var sstRootLeft = new SST
            {
                Methods =
                {
                    methodDeclA
                }
            };

            var sstRootRight = new SST
            {
                Methods =
                {
                    methodDeclB
                }
            };

            double sim;
            Assert.True(_matchUtil.MatchCriterion2(methodDeclA, methodDeclB,
                matchingAlgorithm.MatchLeaves(sstRootLeft.GetPostOrderNodes(), sstRootRight.GetPostOrderNodes()), out sim));
        }
    }
}
﻿/*
 * Copyright 2015 Markus Zimmermann
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;
using DiffTestBench;
using KaVE.Commons.Model.Names.CSharp;
using KaVE.Commons.Model.SSTs.Declarations;
using KaVE.Commons.Model.SSTs.Impl;
using KaVE.Commons.Model.SSTs.Impl.Declarations;
using KaVE.Commons.Model.SSTs.Impl.Expressions.Simple;
using KaVE.Commons.Model.SSTs.Impl.References;
using KaVE.Commons.Model.SSTs.Impl.Statements;
using KaVE.Commons.Model.SSTs.Visitor;
using KaVE.Commons.Utils.Collections;
using NUnit.Framework;
using SSTDiff.Matching;
using SSTDiff.Util;

namespace SSTDiff.Tests.Matching
{
    internal class MatchingAlgorithmTest
    {
        private readonly MatchingAlgorithm _uut = new MatchingAlgorithm(new Settings{ActivateMatchingWithParent = true});
        private readonly MatchUtil _matchUtil = new MatchUtil(new Settings());


        [Test]
        public void MarkNodeAsMatchedTest()
        {
            var node1 = new VariableReference {Identifier = "Test"};

            var otherNode = new MethodDeclaration();

            var nodeList = new List<ISSTNode>
            {
                node1,
                otherNode
            };

            _uut.MarkNodeAsMatched(nodeList, node1);

            var expectedNodeList = new List<ISSTNode>
            {
                otherNode
            };

            CollectionAssert.AreEqual(expectedNodeList, nodeList);
        }

        [Test]
        public void SortTempMatchingTest()
        {
            var node1 = new UnknownReference();
            var node2 = new UnknownReference();

            var match1 = Match.Create(node1, node2, 0.6);
            var match2 = Match.Create(node1, node2, 0.75);
            var match3 = Match.Create(node1, node2, 0.85);
            var match4 = Match.Create(node1, node2, 0.9);

            var unSortedList = new List<Match>
            {
                match1,
                match4,
                match2,
                match3
            };
            var expectedList = new List<Match>
            {
                match4,
                match3,
                match2,
                match1
            };

            CollectionAssert.AreEqual(expectedList, _uut.SortTempMatching(unSortedList));
        }

        [Test]
        public void MatchLeavesTest()
        {
            var methodName = TestNameFactory.GetAnonymousMethodName();

            var varRefA = new VariableReference {Identifier = "VariableA"};
            var fieldDeclA = new FieldDeclaration
            {
                Name = FieldName.Get("[System.Int32, mscore, 4.0.0.0] [MyClass, MyAssembly, 1.2.3.4]" + ".fieldA")
            };

            var varRefB = new VariableReference {Identifier = "VariableB"};
            var fieldDeclB = new FieldDeclaration
            {
                Name = FieldName.Get("[System.Int32, mscore, 4.0.0.0] [MyClass, MyAssembly, 1.2.3.4]" + ".fieldB")
            };

            var methodDeclarationA = new MethodDeclaration
            {
                Name = methodName,
                Body =
                {
                    new Assignment
                    {
                        Expression = new ReferenceExpression(),
                        Reference = varRefA
                    }
                }
            };

            var methodDeclarationB = new MethodDeclaration
            {
                Name = methodName,
                Body =
                {
                    new Assignment
                    {
                        Expression = new ReferenceExpression(),
                        Reference = varRefB
                    }
                }
            };

            var sstRootLeft = new SST
            {
                Fields = new KaVEHashSet<IFieldDeclaration>
                {
                    fieldDeclA
                },
                Methods = new KaVEHashSet<IMethodDeclaration>
                {
                    methodDeclarationA
                }
            };

            var sstRootRight = new SST
            {
                Fields =
                {
                    fieldDeclB
                },
                Methods =
                {
                    methodDeclarationB
                }
            };

            var actualMatching = _uut.MatchLeaves(sstRootLeft.GetPostOrderNodes(), sstRootRight.GetPostOrderNodes());

            double simFieldNames;
            _matchUtil.MatchCriterion1(fieldDeclA.FieldName, fieldDeclB.FieldName, out simFieldNames);

            double simVarRefs;
            _matchUtil.MatchCriterion1(varRefA, varRefB, out simVarRefs);

            var expectedMatching = new List<Match>
            {
                Match.Create(fieldDeclA.ValueType, fieldDeclB.ValueType, 1.0),
                Match.Create(fieldDeclA.FieldName, fieldDeclB.FieldName, simFieldNames),
                Match.Create(varRefA, varRefB, simVarRefs),
                Match.Create(new UnknownReference(), new UnknownReference(), 1.0),
                Match.Create(methodDeclarationA.ReturnType, methodDeclarationB.ReturnType, 1.0),
                Match.Create(methodDeclarationA.MethodName, methodDeclarationB.MethodName, 1.0)
            };
            
            CollectionAssert.AreEquivalent(expectedMatching, actualMatching);
        }

        [Test]
        public void CreateMatchingTestWithEqualSST()
        {
            var sst = SSTReader.ReadAndParseSST("Util\\Test.json");
            var matches = _uut.CreateMatching(sst, sst);
            var allNodes = sst.GetPostOrderNodes();
            Assert.AreEqual(allNodes.Count, matches.Count);
        }

        [Test]
        public void CreateMatchingTestWithDifferentSST()
        {
            var varRef = new VariableReference {Identifier = "foo"};

            var fieldDeclA = new FieldDeclaration
            {
                Name = FieldName.Get("[System.Int32, mscore, 4.0.0.0] [MyClass, MyAssembly, 1.2.3.4]" + ".Apple")
            };

            var fieldDeclB = new FieldDeclaration
            {
                Name = FieldName.Get("[System.Int32, mscore, 4.0.0.0] [MyClass, MyAssembly, 1.2.3.4]" + ".Orange")
            };

            var assignment = new Assignment
            {
                Expression = new ReferenceExpression(),
                Reference = varRef
            };

            var methodDeclarationA = new MethodDeclaration
            {
                Body =
                {
                    assignment
                }
            };

            var methodDeclarationB = new MethodDeclaration
            {
                Body =
                {
                    assignment,
                    new UnknownStatement()
                }
            };

            var sstRootLeft = new SST
            {
                Fields =
                {
                    fieldDeclA,
                    fieldDeclB
                },
                Methods =
                {
                    methodDeclarationA
                }
            };

            var sstRootRight = new SST
            {
                Fields =
                {
                    fieldDeclB
                },
                Methods =
                {
                    methodDeclarationB
                }
            };

            var actualMatching = _uut.CreateMatching(sstRootLeft, sstRootRight);

            var expectedMatching = new List<Match>
            {
                Match.Create(fieldDeclB.ValueType, fieldDeclB.ValueType, 1.0),
                Match.Create(fieldDeclB.FieldName, fieldDeclB.FieldName, 1.0),
                Match.Create(fieldDeclB, fieldDeclB),
                Match.Create(varRef, varRef, 1.0),
                Match.Create(new UnknownReference(), new UnknownReference(), 1.0),
                Match.Create(methodDeclarationA.ReturnType, methodDeclarationB.ReturnType, 1.0),
                Match.Create(methodDeclarationA.MethodName, methodDeclarationB.MethodName, 1.0),
                Match.Create(assignment, assignment, 1.0),
                Match.Create(new ReferenceExpression(), new ReferenceExpression(), 1.0),
                Match.Create(methodDeclarationA, methodDeclarationB, double.NaN),
                Match.Create(sstRootLeft, sstRootRight, double.NaN)
            };
            CollectionAssert.AreEquivalent(expectedMatching, actualMatching);
        }
    }
}
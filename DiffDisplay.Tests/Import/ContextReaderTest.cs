﻿using System.Diagnostics;
using DiffTestBench.Import;
using NUnit.Framework;

namespace DiffDisplay.Tests.Import
{
    public class ContextReaderTest
    {
        private ContextReader _sut;

        [SetUp]
        public void Setup()
        {
            _sut = new ContextReader();
        }

        [Test]
        public void FindZipFilesTest()
        {
            ContextReader.FindZipFiles(ContextReader.RootDir);
            var zipFiles = _sut.ZipFiles;
            foreach (var zipFile in zipFiles)
            {
                Assert.IsTrue(zipFile.EndsWith(".zip"));
                Debug.WriteLine(zipFile);
            }
        }

        [Test]
        public void FindsContexts()
        {
            _sut.ReadContextsFromAllZipFiles(ContextReader.FindZipFiles(ContextReader.RootDir));
            
            var contextDictionary = _sut.ContextDictionary;
            CollectionAssert.IsNotEmpty(contextDictionary);
        }
    }
}